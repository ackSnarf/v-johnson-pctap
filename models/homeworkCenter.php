<?php
/* This function deletes the selected homework center attendance record. */
function deleteHomeworkCenter($hcattendanceID){
  global $db;
  $query = "delete from hcattendance where hcattendanceID = :hcattendanceID";
	$statement = $db->prepare($query);
  $statement->bindValue(':hcattendanceID', $hcattendanceID);
  $statement->execute();
  $statement->closeCursor();
}

/* This function gets a list of the homework center attendance records for the tap record selected in an earlier view.
*/
function getHomeworkCenterList() {
  global $db;

  $query = 'select * from hcattendance
              join hcattendstatuses using (hcattendStatusID)
              where tapID = :tapID
              order by hcattendDate desc';
  $statement = $db->prepare($query);
  $statement->bindValue(':tapID', $_SESSION['tapID']);
  $statement->execute();
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}//end getHomeworkCenterList

/*This function returns the details for the homework center attendance record with the given ID. */
function getHomeworkCenterDetails($hcattendanceID){
  global $db;
  $query = "select * from hcattendance where hcattendanceID = :hcattendanceID";
	$statement = $db->prepare($query);
  $statement->bindValue(':hcattendanceID', $hcattendanceID);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

	return $results;
}//end getHomeworkCenterDetails

/*This function inserts a new homework center attendance record.*/
function insertHomeworkCenter(){
  global $db;
  extract($_REQUEST);

  $query = "insert into hcattendance
              (tapID, hcattendDate, hcattendStatusID, hcattendPoints, homeworkPoints)
              values (:tapID, :hcattendDate, :hcattendStatusID, :hcattendPoints, :homeworkPoints)";
  $statement = $db->prepare($query);
  $statement->bindValue(':tapID', $_SESSION['tapID']);
  $statement->bindValue(':hcattendDate', $hcattendDate);
  $statement->bindValue(':hcattendStatusID', $hcattendStatusID);
  $statement->bindValue(':hcattendPoints', $hcattendPoints = "" ? null : $hcattendPoints);
  $statement->bindValue(':homeworkPoints', $homeworkPoints = "" ? null : $homeworkPoints);
  $statement->execute();
  $statement->closeCursor();
}

/*This function updates the homework center attendance record based on the contents of the
REQUEST array. */
function updateHomeworkCenter(){
  global $db;
  extract($_REQUEST);

  $query = "update hcattendance
              set hcattendDate = :hcattendDate,
              hcattendStatusID = :hcattendStatusID,
              hcattendPoints = :hcattendPoints,
              homeworkPoints = :homeworkPoints
              where hcattendanceID = :hcattendanceID";
  $statement = $db->prepare($query);
  $statement->bindValue(':hcattendDate', $hcattendDate);
  $statement->bindValue(':hcattendStatusID', $hcattendStatusID);
  $statement->bindValue(':hcattendPoints', $hcattendPoints);
  $statement->bindValue(':homeworkPoints', $homeworkPoints);
  $statement->bindValue(':hcattendanceID', $hcattendanceID);
  $statement->execute();
  $statement->closeCursor();
}

function validateHomeworkCenterInputs(){
  define('MIN_HCATTEND_POINTS', 0);			          //Minimum attendance points
	define('MAX_HCATTEND_POINTS', 5);		            //Maximum attendance points
  define('MIN_HOMEWORK_POINTS', 0);               //Minimum homework points
  define('MAX_HOMEWORK_POINTS', 2);               //Maximum homework points

  $errors = [];
  extract($_REQUEST);

  if($javaScriptValidated == 'false'){
    //Validate attendance date (required, is a date, not in future, formatted ISO)
    if($hcattendDate == "")
      $errors['hcattendDate'] = "Attendance date is a required field.";
    else {
      $hcattendDate = str_replace('-','/', $hcattendDate);
      if(!isDate($hcattendDate))
        $errors['hcattendDate'] = "Attendance date must be a recognizable date.";
      else {
        $today = new DateTime();
        $hcattendDate = new DateTime($hcattendDate);
        if($hcattendDate > $today)
          $errors['hcattendDate'] = "Attendance date cannot be in the future.";
        else
          $_REQUEST['hcattendDate'] = $hcattendDate->format('Y-m-d');
        //end if
      }//end if
    }//end if

    //Validate attendance status (required)
		if($attendanceStatusID == "")
				$errors['attendanceStatusID'] = "Attendance status is a required field.";

    //Validate attendance points (numeric, in range)
    if ($hcattendPoints != ""){
      if(!is_numeric($hcattendPoints))
        $errors['hcattendPoints'] = "Attendance points must be a numeric value.";
      else if($hcattendPoints < MIN_HCATTEND_POINTS || $hcattendPoints > MAX_HCATTEND_POINTS)
        $errors['hcattendPoints'] = "Attendance points must be between " . MIN_HCATTEND_POINTS . " and " . MAX_HCATTEND_POINTS . ".";
    }//end if

    //Validate homework points (numeric, in range)
    if ($homeworkPoints != ""){
      if(!is_numeric($homeworkPoints))
        $errors['homeworkPoints'] = "Homework points must be a numeric value.";
      else if($homeworkPoints < MIN_HOMEWORK_POINTS || $homeworkPoints > MAX_HOMEWORK_POINTS)
        $errors['homeworkPoints'] = "Homework points must be between " . MIN_HOMEWORK_POINTS . " and " . MAX_HOMEWORK_POINTS . ".";
    }//end if
  }//end if JavaScriptValidated

  return $errors;
}
?>
