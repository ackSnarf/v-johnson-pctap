<?php

/* This function gets the grade levels from the db.  */
function getGradeLevels(){
   global $db;

    $myQuery = 'SELECT * from gradelevels order by gradeLevelID';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}

/* This function inserts a new grade level into the dropdown list. */
function insertGradeLevel($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into gradelevels (gradeLevel) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the grade level items for the loolUp list. */
function updateGradeLevels($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE gradelevels set gradeLevel=:value where gradeLevelID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
