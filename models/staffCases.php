<?php
require('staff.php');


switch($action) {
    case "staffList":
        $staff = getCurrentStaffList();
        $allStaff = getAllStaffList();
        include('views/staffList.php');
        break;
    case "staffAdd":
        $errors = "";
        $details = "";
    		include ("views/staffDetails.php");
    		break;
    case "staffDetails":
        $errors = "";
        $selectedStaff = null;
        $id = $selectedStaff;
        extract($_REQUEST);
        if ($id == null || $id < 0) {
            $details = staffDet ($_REQUEST['staffID']);
        } else {
            $details = staffDet ($id);
        }
        include ("views/staffDetails.php");
        break;
    case "staffNew":
    case 'staffUpdate':
			//Figure out which button was clicked
      switch(true) {
          case isset($_REQUEST['btnSave']):
                $errors = validateStaffInputs();
               if(count($errors)==0) {
                      if($action=='staffUpdate')
                         updateStaffMember();
                      else
                          saveNewStaff();
                      header('Location: ?action=staffList');
                  } else {
                      $details = $_REQUEST;
                      include('views/staffDetails.php');  //Show form again with error marker
                    }//end if
                  break;
           case isset($_REQUEST['btnCancel']):
              //Display the list
              header('Location: ?action=staffList');
              break;
        }//end Switch
}
?>
