<?php
	//Note call is to models folder, so path for connect

	include("../connectTap.php");
	$query = "Select count(*) from grades
              	where subjectID = :subjectID and tapID = :tapID and
								dateGradeChecked = :dateGradeChecked";
	$statement = $db->prepare($query);
	$statement->bindValue(':subjectID', $_REQUEST['subjectID']);
	$statement->bindValue(':tapID', $_SESSION['tapID']);
	$statement->bindValue(':dateGradeChecked', $_REQUEST['dateGradeChecked']);
	$statement->execute();
	$results = $statement->fetch();
	$statement->closeCursor();

	//Return results asynchronously
  echo $results[0];
?>
