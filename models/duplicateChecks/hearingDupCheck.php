<?php
	//Note call is to models folder, so path for connect

	include("../connectTap.php");
	$query = "Select count(*) from hearings
              	where courtOrderID = :courtOrderID and
								hearingDate = :hearingDate";
	$statement = $db->prepare($query);
	$statement->bindValue(':hearingDate', $_REQUEST['hearingDate']);
	$statement->bindValue(':courtOrderID', $_SESSION['courtOrderID']);
	$statement->execute();
	$results = $statement->fetch();
	$statement->closeCursor();

	//Return results asynchronously
  echo $results[0];
?>
