<?php
require_once('models/caseNotes.php');
require_once('models/clients.php');
require_once('models/contacts.php');
require_once('models/courtOrders.php');
require_once('models/dischargeReasons.php');
require_once('models/genders.php');
require_once('models/gradeLevels.php');
require_once('models/hearings.php');
require_once('models/noteTypes.php');
require_once('models/races.php');
require_once('models/schools.php');
require_once('models/staff.php');
require_once('models/targets.php');


switch($action){
  case "reportList":

      include("views/reportList.php");
  break;
  case "reportClients":
     echo "reportClients button";
//     include("views/clientList.php");
  break;
  case "caseNoteDetails": //JUST A TEMPLATE. 
      $errors = "";
      $targetTypes = getTargetTypes();
      $staff = getStaff();
      $noteTypes = getNoteTypes();
      $details = getCaseNoteDetails($_REQUEST['caseNoteID']);
      include("views/caseNoteDetails.php");
  break;

}

 ?>
