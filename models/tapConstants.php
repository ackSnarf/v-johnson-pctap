<?php

	date_default_timezone_set('America/Chicago');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //FIRST NAME CONSTANTS
	$CONTACTFIRSTNAME_REQ = '';
	$CONTACTFIRSTNAME_MAXLENGTH = 15;  //based on data dictionary
    //FIRST NAME VALIDATION STRING
	$CONTACTFIRSTNAME_VALIDATION = $CONTACTFIRSTNAME_REQ . ' maxlength="' . $CONTACTFIRSTNAME_MAXLENGTH . '"';

	//LAST NAME CONSTANTS
	$CONTACTLASTNAME_REQ = '';
	$CONTACTLASTNAME_MAXLENGTH = 15;   //based on data dictionary
	//LAST NAME VALIDATION STRING
	$CONTACTLASTNAME_VALIDATION = $CONTACTLASTNAME_REQ . ' maxlength="' . $CONTACTLASTNAME_MAXLENGTH . '"';

    //EMAIL CONSTANTS
    $CONTACTEMAIL_MAXLENGTH = 30;  //based on data dictionary
    //EMAIL VALIDATION STRING
    $CONTACTEMAIL_VALIDATION = ' maxlength="' . $CONTACTEMAIL_MAXLENGTH . '"';

    //STREET ADDRESS CONSTANTS
    $CONTACTSTREETADDRESS_MAXLENGTH = 40;  //based on data dictionary
    //STREET ADDRESS VALIDATION STRING
    $CONTACTSTREETADDRESS_VALIDATION = ' maxlength="' . $CONTACTSTREETADDRESS_MAXLENGTH . '"';

    //CITY CONSTANTS
    $CONTACTCITY_MAXLENGTH = 30;  //based on data dictionary
    //CITY VALIDATION STRING
    $CONTACTCITY_VALIDATION = ' maxlength="' . $CONTACTCITY_MAXLENGTH . '"';

    //STATE CONSTANTS
    $CONTACTSTATE_MAXLENGTH = 2;  //based on data dictionary
    //STATE VALIDATION STRING
    $CONTACTSTATE_VALIDATION = ' maxlength="' . $CONTACTSTATE_MAXLENGTH . '"';

    //ZIP CODE CONSTANTS
    $CONTACTZIPCODE_MIN =	0;    //based on data dictionary
	  $CONTACTZIPCODE_MAX =	99999;
	  $CONTACTZIPCODE_STEP =	1;
    //ZIP CODE VALIDATION STRING
    $CONTACTZIPCODE_VALIDATION = ' min="' . $CONTACTZIPCODE_MIN . '" max="' . $CONTACTZIPCODE_MAX . '" step="' . $CONTACTZIPCODE_STEP . '"';


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BOTTOM  OF CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS    CONTACTS
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////





    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    $PHONENUMBER_MIN =	0;    //based on data dictionary
  $PHONENUMBER_MAX =	99999999999999;
  $PHONENUMBER_STEP =	1;
    //PHONE NUMBER VALIDATION STRING
    $PHONENUMBER_VALIDATION = ' min="' . $PHONENUMBER_MIN . '" max="' . $PHONENUMBER_MAX . '" step="' . $PHONENUMBER_STEP . '"';

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BOTTOM OF PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE    PHONE
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////






    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //COURT ORDER     COURT ORDER   COURT ORDER       COURT ORDER    COURT ORDER    COURT ORDER    COURT ORDER
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //CASENUMBER
	$CASENUMBER_REQ = '';
	$CASENUMBER_MAXLENGTH = 6;  //based on data dictionary
    //CASE NUMBER VALIDATION STRING
	$CASENUMBER_VALIDATION = $CASENUMBER_REQ . ' maxlength="' . $CASENUMBER_MAXLENGTH . '"';

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BOTTOM OF COURT ORDER     COURT ORDER     COURT ORDER       COURT ORDER    COURT ORDER    COURT ORDER
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $GRADE_MIN = 1.0;
    $GRADE_MAX = 4.0;
    $GRADE_STEP = 0.01;

    $GRADE_VALIDATION = ' min=" ' . $GRADE_MIN . '" max="' . $GRADE_MAX . '" step="' . $GRADE_STEP . '"';


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BOTTOM OF GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES    GRADES
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//HOMEWORK CENTER    HOMEWORK CENTER    HOMEWORK CENTER    HOMEWORK CENTER    HOMEWORK CENTER
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$HCATTENDPOINTS_MIN = 0;
		$HCATTENDPOINTS_MAX = 5;
		$HCATTENDPOINTS_STEP = 1;

		$HCATTENDPOINTS_VALIDATION = ' min="' . $HCATTENDPOINTS_MIN . '" max="' . $HCATTENDPOINTS_MAX . '" step="' . $HCATTENDPOINTS_STEP . '"';

		$HOMEWORKPOINTS_MIN = 0;
		$HOMEWORKPOINTS_MAX = 2;
		$HOMEWORKPOINTS_STEP = 1;

		$HOMEWORKPOINTS_VALIDATION = ' min="' . $HOMEWORKPOINTS_MIN . '" max="' . $HOMEWORKPOINTS_MAX . '" step="' . $HOMEWORKPOINTS_STEP . '"';
?>
