<?php

/* This function generates a list of the genders for the client.  */
function getGenders(){
   global $db;

    $myQuery = 'SELECT * from genders order by gender';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}

/* This function inserts a new gender type into the dropdown list. */
function insertGender($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into genders (gender) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the gender items for the loolUp list. */
function updateGenders($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE genders set gender=:value where genderID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
