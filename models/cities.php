<?php

/* This function gets the cities for the list box of contact cities from the db.
 */
    function getContactCity(){
       global $db;
        
        $myQuery = 'select distinct contactCity from contacts';
		
        $statement = $db->prepare($myQuery);
        //$statement->bindValue(':filter',$_SESSION());
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        
        return $results;
    }
?>