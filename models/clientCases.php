<?php
require_once('models/clients.php');
require_once('models/courtOrders.php');
require_once('models/contacts.php');
require_once('models/contactTypes.php');
require_once('models/phones.php');
require_once('models/phoneTypes.php');
require_once('models/genders.php');
require_once('models/races.php');

  $new = false;   // this flag identifies a new contact entry
  $sideNav = false;

switch($action){
  case "clientList":

    unset ($_SESSION['tapID']);
    unset ($_SESSION['courtOrderID']);
    unset ($_SESSION['caseNumber']);
    unset ($_SESSION['contactID']);
    unset ($_SESSION['clientName']);
    unset ($_SESSION['kidtraxID']);
    unset ($_SESSION['dob']);
    unset ($_SESSION['clientID']);

  //  $_SESSION['clientSearch'] = fieldValue($_REQUEST, 'clientSearch', fieldValue($_SESSION, 'clientSearch'));

    $clients = getClientList();
    $allClients = getFullClientList();
    include("views/clientList.php");

    /********DEBUG CODE (DO NOT DELETE)********/
    /*
    echo "THIS IS CLIENT LIST";
    echo "REQUEST ARRAY (EMPTY ON FIRST LOAD, THEN HAS ACTION)";
    printArray($_REQUEST);
    echo "SESSION ARRAY (CLIENTSEARCH)";
    printArray($_SESSION);
    echo "DETAILS ARRAY(UNDEFINED because clients used)";
    printArray($details);
    echo "CLIENTS ARRAY(CONTACTID, CLIENTNAME)";
    printArray($clients);
    */
    /**********END DEBUG CODE*********/

    break;

  case "clientNew":
    $new = true;
    $sideNav = true;
    switch(true){
        case isset($_REQUEST['btnSave']):
            $errors = validateClientInputs();
            if(count($errors)==0) {

              //insertClientDetails() sets the clientID in the SESSION
              //inserts kidtraxID, dob, raceID, genderID
              $clientRow = insertClientDetails();

              //createContact() is in contacts.php
              //calls insertContact()
                //inserts fname, lname, address, city, state, zip, email, contactTypeID, emergencyContact
              //calls insertContactLink()
                //inserts clientID, contactID link
              //if phoneNumber not blank
                //calls insertPhoneNumber() which inserts pn, pntypeID
                //calls insertContactPhoneNumber() which inserts pnID, contactID link
              $conID = createContact($_SESSION['clientID']);

              $_SESSION['contactID'] = $conID;
              $_SESSION['clientName'] = $_REQUEST['contactFirstName'] . ' ' . $_REQUEST['contactLastName'];
              //$_SESSION['kidtraxID'] = $_REQUEST['kidtraxID'];
              //$_SESSION['dob'] = $_REQUEST['dob'];

              //echo "ROWCOUNTS: CLIENTROW: " . $clientRow;
              //header('Location:?action=clientEdit&contactID=' . $conID . '&editingClient=1');
              header('Location:?action=courtOrderNew');
            } else {
                //get all data back
                $details = $_REQUEST;
                $race = getRace();
                $genders = getGenders();
                $types = getPhoneType();
                include('views/clientDetails.php');  //Show form again with error markers
            }//end if
            break;

        case isset($_REQUEST['btnCancel']):
            header('Location:?action=clientList');
            break;

        default:
            $details = "";
            $errors = "";
            $phones = "";
            $race = getRace();
            $genders = getGenders();
            $types = getPhoneType();
            include("views/clientDetails.php");
            break;

    }//end case clientNew button switch
  break;
  case 'clientEdit':
    $new = false;
    $sideNav = false;
//    echo "request array";
//    printArray($_REQUEST);

    switch(true){
        case isset($_REQUEST['btnSave']):
            //echo "THIS IS CLIENT EDIT SAVE";

            //checks if fname and lname not blank
            $errors = validateClientInputs();
            if(count($errors)==0) {

                /********DEBUG CODE (DO NOT DELETE)********/
                /*
                echo "ERRORS IS 0, proceed to update functions";
                echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                    printArray($_SESSION);
                //echo "REQUEST ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)"; //This is what appears if no header
                echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                    printArray($_REQUEST);
                //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                    printArray($details);
                //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                    printArray($contactDetails);
                //echo "DEMOGRAPHICS ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                    printArray($demographics);
                //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                    printArray($phones);
                echo "ERRORS ARRAY (EMPTY)";
                    printArray($errors);
                //echo "RACE ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "RACE ARRAY (RACEID, RACE)";
                    printArray($race);
                //echo "GENDERS ARRAY (UNDEFINED)"; //This is what appears if no header
                echo "GENDERS ARRAY (GENDERID, GENDER)";
                    printArray($genders);
                //echo "TAPENROLLMENT ARRAY (UNDEFINED)"; //This is what appears if no header

                echo "TAPENROLLEMENT ARRAY (EMPTY??)";
                printArray($tapEnrollment);
                */
                /**********END DEBUG CODE*********/

                //updates fname, lname, address, city, state, zip, email, contactTypeID, emergencyContact
                $contactRow = updateContact();
                //updates kidtraxID, dob, genderID, raceID
                $clientRow = updateClientDemog();


                //echo "ROWCOUNTS: CONTACTROW: " . $contactRow . " CLIENTROW: " . $clientRow;
                header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
            } else {
                //get all data back
                $details = $_REQUEST;

                $race = getRace();
                $genders = getGenders();

                $phones = getContactPhoneList($_REQUEST['contactID']);

                $tapEnrollment = getClientTapEnrollmentDetails($_REQUEST['contactID']);

                /********DEBUG CODE (DO NOT DELETE)********/
                /*
                echo "ERRORS AFTER PHP VALIDATION, return to details page with errors and info on screen";
                echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                    printArray($_SESSION);
                echo "REQUEST ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)";
                    printArray($_REQUEST);
                echo "DETAILS ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)";
                    printArray($details);
                echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                    printArray($contactDetails);
                echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                    printArray($demographics);
                echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                    printArray($phones);
                echo "ERRORS ARRAY (all error messages)";
                    printArray($errors);
                echo "RACE ARRAY (RACEID, RACE)";
                    printArray($race);
                echo "GENDERS ARRAY (GENDERID, GENDER)";
                    printArray($genders);

                echo "TAPENROLLEMENT ARRAY (EMPTY??)";
                printArray($tapEnrollment);
                */
                /**********END DEBUG CODE*********/
                include('views/clientDetails.php');   //Show form again with error markers
            }//end if
            break;
        case isset($_REQUEST['btnCancel']):
            //echo "THIS IS CLIENT EDIT CANCEL";
            header('Location:?action=clientList');
            break;
        default:
          //  echo "THIS IS CLIENT EDIT DEFAULT";

            $errors = "";

                //MUST USE REQUEST HERE!!
                //(CONTACTID, FNAME, LNAME, KIDTRAX, DOB, CLIENTID, REFDATE, DISDATE, CASE#)
            /****CLIENTDETAILS WANTS TAPID COURTID ETC, DOESNT WORK FOR OUR SITUATION OF JUST SAVING A CLIENT WITHOUT A TAP RECORD YET.... MAKE NEW FUNCTION FOR MY PURPOSES AND LEAVE OLD?? *******/
            //$clientDetails = getClientDetails($_REQUEST['contactID']);


                //(CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)
            $contactDetails = getContactDetails();

                //MUST USE REQUEST/CONTACTDETAILS HERE!!
                //(CLIENTID, KIDTRAX, DOB, RACEID, GENDERID)
            $demographics = getClientDemogJ($_REQUEST['contactID']);

                //MUST USE REQUEST/CONTACTDETAILS HERE!!
                //(PHONENUMBER, PNTYPE, PNID, CONTACTID)
            $phones = getContactPhoneList($_REQUEST['contactID']);

                //MUST USE REQUEST/CONTACTDETAILS HERE!!
                //(CONTACTID, FNAME, LNAME, KIDTRAX, DOB, CLIENTID, REFDATE, DISDATE, CASE#, COURTORDERID, TAPID)
            $tapEnrollment = getClientTapEnrollmentDetails($_REQUEST['contactID']);

            $details = array_merge($contactDetails, $demographics);

                //get list of races
                //(RACEID, RACE)
            $race = getRace();
                //get list of genders
                //(GENDERID, GENDER)
            $genders = getGenders();

                //MUST USE DETAILS/CONTACTDETAILS HERE!!
            $_SESSION['clientID'] = $details['clientID'];
                //MUST USE REQUEST/DETAILS HERE!!
            $_SESSION['contactID'] = $details['contactID'];
                //MUST USE DETAILS/CONTACTDETAILS HERE!!
            $_SESSION['clientName'] = $details['contactFirstName'] . ' ' . $details['contactLastName'];
                //MUST USE DETAILS HERE??
            //$_SESSION['kidtraxID']= $details['kidtraxID'];
                //MUST USE DETAILS HERE??
            //$_SESSION['dob']= $details['dob'];

            /********DEBUG CODE (DO NOT DELETE)********/
            /*
            echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                printArray($_SESSION);
            echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                printArray($_REQUEST);
            echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                printArray($details);
            echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                printArray($contactDetails);
            echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                printArray($demographics);
            echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                printArray($phones);
            echo "ERRORS ARRAY (EMPTY)";
                printArray($errors);
            echo "RACE ARRAY (RACEID, RACE)";
                printArray($race);
            echo "GENDERS ARRAY (GENDERID, GENDER)";
                printArray($genders);

            echo "TAPENROLLEMENT ARRAY (CONTACTID, FNAME, LNAME, KIDTRAX, DOB, CLIENTID, REFDATE, DISDATE, CASE#, COURTORDERID, TAPID)";
                printArray($tapEnrollment);
            */
            /**********END DEBUG CODE*********/
            include('views/clientDetails.php');
            break;
        }//end case clientEdit button switch
    break;

    default: echo 'Action ' . $action . ' is not recognized';
    break;

  }//end client action switch
 ?>
