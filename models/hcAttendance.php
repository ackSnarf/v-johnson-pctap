<?php

    function alternDays(){
        global $db;

        $myQuery =  'select hcattendanceID, hcattendDate, hcattendPoints,hcAttendStatusID, hcattendStatus
                  from hcattendstatuses
                  join hcattendance using (hcAttendStatusID)
                  left join (select tapID, caseNoteDate, note
	                from hcattendance join casenotes using (tapID)
                  where noteTypeID = 6 and caseNoteDate = hcattendDate) hcCaseNotes using (tapID)
	                where note is null or caseNoteDate = hcattendDate
                    order by hcattendDate desc';

        $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }

    function hcAttendanceMon() {
        global $db;

        $myQuery = 'select distinct concat(contactFirstName, " ",contactLastName) as client, tapID
                   from contacts join clientContacts using (contactID)
                   join taps using (clientID)
                   join courtOrders using (tapID)
                   join (select courtOrderID, MAX(hearingDate), hcMonday
		               from hearings group by courtOrderID) mostRecentHearings using (courtOrderID)
   	                  where dischargeDate is null and contactTypeID = 5 and hcMonday = 1';
        $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

		return $results;
    }

    function hcAttendanceTues() {
        global $db;

        $myQuery = 'select distinct concat(contactFirstName, " ",contactLastName) as client, tapID
                   from contacts join clientContacts using (contactID)
                   join taps using (clientID)
                   join courtOrders using (tapID)
                   join (select courtOrderID, MAX(hearingDate), hcTuesday
		               from hearings group by courtOrderID) mostRecentHearings using (courtOrderID)
   	                  where dischargeDate is null and contactTypeID = 5 and hcTuesday = 1';
        $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

    return $results;
    }

    function hcAttendanceWed() {
        global $db;

        $myQuery = 'select distinct concat(contactFirstName, " ",contactLastName) as client, tapID
                   from contacts join clientContacts using (contactID)
                   join taps using (clientID)
                   join courtOrders using (tapID)
                   join (select courtOrderID, MAX(hearingDate), hcWednesday
		               from hearings group by courtOrderID) mostRecentHearings using (courtOrderID)
   	                  where dischargeDate is null and contactTypeID = 5 and hcWednesday = 1';
	      $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

		return $results;
    }

   function hcAttendanceThurs() {
        global $db;

        $myQuery = 'select distinct concat(contactFirstName, " ",contactLastName) as client, tapID
                   from contacts join clientContacts using (contactID)
                   join taps using (clientID)
                   join courtOrders using (tapID)
                   join (select courtOrderID, MAX(hearingDate), hcThursday
		               from hearings group by courtOrderID) mostRecentHearings using (courtOrderID)
   	                  where dischargeDate is null and contactTypeID = 5 and hcThursday = 1';
	      $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

		return $results;
    }

    function insertAttendance() {
      global $db;
      extract($_REQUEST);

      foreach ($client as $key=>$value) {
          $myQuery ='INSERT INTO hcattendance 
                            (tapID, 
                            hcattendStatusID, 
                            hcattendDate, 
                            hcattendPoints, 
                            homeworkPoints)
                    VALUES (:tapID, 
                            :hcattendStatusID, 
                            :hcattendDate, 
                            :hcattendPoints, 
                            :homeworkPoints)';

          $statement = $db->prepare($myQuery);
          $statement->bindValue(':tapID',$tapID[$key]);
          $statement->bindValue(':hcattendStatusID',$hcattendStatusID[$key]);
          $statement->bindValue(':hcattendDate',($hcattendDate==""?null:$hcattendDate));
          $statement->bindValue(':hcattendPoints',($hcattendPoints==""?0:$hcattendPoints[$key]));
          $statement->bindValue(':homeworkPoints',($homeworkPoints==""?0:$homeworkPoints[$key]));

          $statement->execute();
          $statement->closeCursor();
          /*
          echo $tapID[$key]."**".$hcattendStatusID[$key]."**".$hcattendDate."**".$hcattendPoints[$key]."**".$homeworkPoints[$key]."END";
          */
        }//end foreach
        return $statement->rowCount();
    }//end insertAttendance

    function validateHcAttendanceInputs() {
        $errors = array();
        if($_REQUEST['javascriptValidated']=='false') {
        //extract($_REQUEST);
        
        }//end if

        //Consistency Check here
        return $errors;
    }//end validateHcAttendanceInputs
    
/*NOT USING YET*/
/*
    function homeworkDetails($hcattendanceID){
      global $db;

      $myQuery =  'select hcattendanceID,hcattendDate, hcattendPoints, hcAttendStatusID,homeworkPoints
                  from hcattendstatuses
                  join hcattendance using (hcAttendStatusID)
                  left join (select tapID, caseNoteDate, note
	                from hcattendance join casenotes using (tapID)
                  where noteTypeID = 6 and caseNoteDate = hcattendDate) hcCaseNotes using (tapID)
	                where note is null or caseNoteDate = hcattendDate and
                  hcattendanceID = hcattendanceID
                    order by hcattendDate desc';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
    }

    function saveAttendance(){
      global $db;

      extract($_REQUEST);

      $myQuery = 'INSERT into hcattendance (tapID,hcattendDate,hcattendStatusID,hcattendPoints,homeworkPoints)
                            values (:tapID,:hcattendDate,:hcattendStatusID,:hcattendPoints,:homeworkPoints);
                              select hcattendDate into @attendDate from hcattendance;
                  INSERT into casenotes (tapID,caseNoteDate,noteTypeID,note)
                            values  (:tapid,@attendDate,6,:note)';

      $statement = $db->prepare($myQuery);
                                $statement->bindValue(':tapID',$tapID);
                                $statement->bindValue(':hcattendDate',$hcattendDate);
                                $statement->bindValue(':hcattendStatusID',$hcattendStatusID);
                                $statement->bindValue(':hcattendPoints',($hcattendPoints==""?null:$hcattendPoints));
                                $statement->bindValue(':homeworkPoints',($homeworkPoints==""?null:$homeworkPoints));
                                $statement->bindValue(':tapID',$tapID);
                                $statement->bindValue(':note',($note==""?null:$note));
                                $statement->execute();
                                $statement->closeCursor();

  return $statement->rowCount();
}
*/
?>
