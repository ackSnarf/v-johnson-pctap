<?php
/* This function deletes the selected hearing. */
function deleteHearing($id){
  global $db;
  $query = "delete from hearings where hearingID = :hearingID";
	$statement = $db->prepare($query);
  $statement->bindValue(':hearingID', $id);
  $statement->execute();
  $statement->closeCursor();
}

/*This function returns the details for the hearing with the given ID. */
function getHearingDetails($id){
  global $db;
  $query = "select * from hearings
            join courtOrders using (courtOrderID)
            join taps using (tapID)
            where hearingID = :hearingID";
	$statement = $db->prepare($query);
  $statement->bindValue(':hearingID', $id);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

	return $results;
}

/*This function gets a list of the hearing dates and hearing IDs. */
function getHearingList(){
  global $db;
  $query = "select * from
              hearings join courtOrders using (courtOrderID)
              where courtOrderID = :courtOrderID
              order by hearingDate desc";
  $statement = $db->prepare($query);
  $statement->bindValue(':courtOrderID', $_SESSION['courtOrderID']);
  $statement->execute();
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  $statement->closeCursor();
  return $results;
}

/*This function inserts a new hearing.*/
function insertHearing(){
  global $db;
  extract($_REQUEST);

  $query = "insert into hearings
              (courtOrderID, hearingDate, orderEndsID, yearOrderEnds, csw, csHoursOrdered, csHoursStayed,
               csHoursDueByReview, aodaScreen, jipsReferral, tapMonitor, wrap,
               tapAfterSchool, hcMonday, hcTuesday, hcWednesday, hcThursday,
               dmvSuspension, dmvSuspensionStayed, dnrSuspension, dnrSuspensionStayed, forfeiture, forfeitureAmount,
               forfeitureStayed, clientPresent, nextHearingDate)
               values (:courtOrderID, :hearingDate, :orderEndsID, :yearOrderEnds, :csw, :csHoursOrdered, :csHoursStayed,
                  :csHoursDueByReview, :aodaScreen, :jipsReferral, :tapMonitor, :wrap,
                  :tapAfterSchool, :hcMonday, :hcTuesday, :hcWednesday, :hcThursday,
                  :dmvSuspension, :dmvSuspensionStayed, :dnrSuspension, :dnrSuspensionStayed, :forfeiture, :forfeitureAmount,
                  :forfeitureStayed, :clientPresent, :nextHearingDate)";
  $statement = $db->prepare($query);
  $statement->bindValue(':courtOrderID', $_SESSION['courtOrderID']);
  $statement->bindValue(':hearingDate', $hearingDate);
  $statement->bindValue(':orderEndsID', $orderEndsID);
  $statement->bindValue(':yearOrderEnds', $yearOrderEnds);
  $statement->bindValue(':csw', isset($csw));
  $statement->bindValue(':csHoursOrdered', ($csHoursOrdered == "" ? null : $csHoursOrdered));
  $statement->bindValue(':csHoursStayed', ($csHoursStayed == "" ? null : $csHoursStayed));
  $statement->bindValue(':csHoursDueByReview', ($csHoursDueByReview == "" ? null : $csHoursDueByReview));
  $statement->bindValue(':aodaScreen', isset($aodaScreen));
  $statement->bindValue(':jipsReferral', isset($jipsReferral));
  $statement->bindValue(':tapMonitor', isset($tapMonitor));
  $statement->bindValue(':wrap', isset($wrap));
  $statement->bindValue(':tapAfterSchool', isset($tapAfterSchool));
  $statement->bindValue(':hcMonday', isset($hcMonday));
  $statement->bindValue(':hcTuesday', isset($hcTuesday));
  $statement->bindValue(':hcWednesday', isset($hcWednesday));
  $statement->bindValue(':hcThursday', isset($hcThursday));
  $statement->bindValue(':dmvSuspension', isset($dmvSuspension));
  $statement->bindValue(':dmvSuspensionStayed', isset($dmvSuspensionStayed));
  $statement->bindValue(':dnrSuspension', isset($dnrSuspension));
  $statement->bindValue(':dnrSuspensionStayed', isset($dnrSuspensionStayed));
  $statement->bindValue(':forfeiture', isset($forfeiture));
  $statement->bindValue(':forfeitureAmount', ($forfeitureAmount == "" ? null : $forfeitureAmount));
  $statement->bindValue(':forfeitureStayed', isset($forfeitureStayed));
  $statement->bindValue(':clientPresent', isset($clientPresent));
  $statement->bindValue(':nextHearingDate', ($nextHearingDate == "" ? null : $nextHearingDate));
  $statement->execute();
  $statement->closeCursor();
}

/*This function updates the hearing record based on the contents of the details fields (the REQUEST
array).*/
function updateHearing(){
  global $db;
  extract($_REQUEST);

  $query = "update hearings
              set hearingDate = :hearingDate,
              orderEndsID = :orderEndsID,
              yearOrderEnds = :yearOrderEnds,
              csw = :csw,
              csHoursOrdered = :csHoursOrdered,
              csHoursStayed = :csHoursStayed,
              csHoursDueByReview = :csHoursDueByReview,
              aodaScreen = :aodaScreen,
              jipsReferral = :jipsReferral,
              tapMonitor = :tapMonitor,
              wrap = :wrap,
              tapAfterSchool = :tapAfterSchool,
              hcMonday = :hcMonday,
              hcTuesday = :hcTuesday,
              hcWednesday = :hcWednesday,
              hcThursday = :hcThursday,
              dmvSuspension = :dmvSuspension,
              dmvSuspensionStayed = :dmvSuspensionStayed,
              dnrSuspension = :dnrSuspension,
              dnrSuspensionStayed = :dnrSuspensionStayed,
              forfeiture = :forfeiture,
              forfeitureAmount = :forfeitureAmount,
              forfeitureStayed = :forfeitureStayed,
              clientPresent = :clientPresent,
              nextHearingDate = :nextHearingDate
              where hearingID = :hearingID";
	$statement = $db->prepare($query);
  $statement->bindValue(':hearingDate', $hearingDate);
  $statement->bindValue(':orderEndsID', $orderEndsID);
  $statement->bindValue(':yearOrderEnds', $yearOrderEnds);
  $statement->bindValue(':csw', isset($csw));
  $statement->bindValue(':csHoursOrdered', ($csHoursOrdered == "" ? null : $csHoursOrdered));
  $statement->bindValue(':csHoursStayed', ($csHoursStayed == "" ? null : $csHoursStayed));
  $statement->bindValue(':csHoursDueByReview', ($csHoursDueByReview == "" ? null : $csHoursDueByReview));
  $statement->bindValue(':aodaScreen', isset($aodaScreen));
  $statement->bindValue(':jipsReferral', isset($jipsReferral));
  $statement->bindValue(':tapMonitor', isset($tapMonitor));
  $statement->bindValue(':wrap', isset($wrap));
  $statement->bindValue(':tapAfterSchool', isset($tapAfterSchool));
  $statement->bindValue(':hcMonday', isset($hcMonday));
  $statement->bindValue(':hcTuesday', isset($hcTuesday));
  $statement->bindValue(':hcWednesday', isset($hcWednesday));
  $statement->bindValue(':hcThursday', isset($hcThursday));
  $statement->bindValue(':dmvSuspension', isset($dmvSuspension));
  $statement->bindValue(':dmvSuspensionStayed', isset($dmvSuspensionStayed));
  $statement->bindValue(':dnrSuspension', isset($dnrSuspension));
  $statement->bindValue(':dnrSuspensionStayed', isset($dnrSuspensionStayed));
  $statement->bindValue(':forfeiture', isset($forfeiture));
  $statement->bindValue(':forfeitureAmount', ($forfeitureAmount == "" ? null : $forfeitureAmount));
  $statement->bindValue(':forfeitureStayed', isset($forfeitureStayed));
  $statement->bindValue(':clientPresent', isset($clientPresent));
  $statement->bindValue(':nextHearingDate', ($nextHearingDate == "" ? null : $nextHearingDate));
  $statement->bindValue(':hearingID', $hearingID);

  $statement->execute();
  $statement->closeCursor();
}

/*
This function validates the form data and populates an errors array with data fields and error messages.
*/
function validateHearingInputs(){
  define('MIN_CS_HOURS', 0);			          //Minimum community service hours
	define('MAX_CS_HOURS', 10);		            //Maximum community service hours
  define('MIN_FORFEITURE_AMOUNT', 0);			  //Minimum forfeiture amount
	define('MAX_FORFEITURE_AMOUNT', 500);		  //Maximum forfeiture amount
  define('MIN_YEAR_ORDER_ENDS', 2012);      //Minimum year order ends
  define('MAX_YEAR_ORDER_ENDS', date('Y', strtotime('+1 year'))); //Maximum year order ends (one year from current year)

  $errors = [];
  extract($_REQUEST);

	if($javaScriptValidated == 'false'){
    //Validate hearing date (required, is a date, not in future, formatted ISO)
    if($hearingDate == "")
      $errors['hearingDate'] = "Hearing date is a required field.";
    else {
      $hearingDate = str_replace('-','/', $hearingDate);
      if(!isDate($hearingDate))
        $errors['hearingDate'] = "Hearing date must be a recognizable date.";
      else {
        $today = new DateTime();
        $hearingDate = new DateTime($hearingDate);
        if($hearingDate > $today)
          $errors['hearingDate'] = "Hearing date cannot be in the future.";
        else
          $_REQUEST['hearingDate'] = $hearingDate->format('Y-m-d');
        //end if
      }//end if
    }//end if

    //Validate order ends (required)
		if($orderEndsID == "" || $orderEndsID == 10)
				$errors['orderEndsID'] = "Order ends is a required field.";

    //Validate year order ends (numeric, in range)
    if($yearOrderEnds != ""){
      if(!is_numeric($yearOrderEnds))
        $errors['yearOrderEnds'] = "Year order ends must be a numeric value";
      else if($yearOrderEnds < MIN_YEAR_ORDER_ENDS || $yearOrderEnds > MAX_YEAR_ORDER_ENDS)
        $errors['yearOrderEnds'] = "Year order ends must be between " . MIN_YEAR_ORDER_ENDS . " and " . MAX_YEAR_ORDER_ENDS . ".";
    }
    
    //Validate CS hours ordered (numeric, in range)
    if ($csHoursOrdered != ""){
      if(!is_numeric($csHoursOrdered))
        $errors['csHoursOrdered'] = "CS hours ordered must be a numeric value.";
      else if($csHoursOrdered < MIN_CS_HOURS || $csHoursOrdered > MAX_CS_HOURS)
        $errors['csHoursOrdered'] = "CS hours ordered must be between " . MIN_CS_HOURS . " and " . MAX_CS_HOURS . ".";
    }

    //Validate CS hours stayed (numeric, in range)
    if ($csHoursStayed != ""){
      if(!is_numeric($csHoursStayed))
        $errors['csHoursStayed'] = "CS hours stayed must be a numeric value.";
      else if($csHoursStayed < MIN_CS_HOURS || $csHoursStayed > MAX_CS_HOURS)
        $errors['csHoursStayed'] = "CS hours stayed must be between " . MIN_CS_HOURS . " and " . MAX_CS_HOURS . ".";
    }

    //Validate CS hours due by review (numeric, in range)
    if ($csHoursDueByReview != ""){
      if(!is_numeric($csHoursDueByReview))
        $errors['csHoursDueByReview'] = "CS hours due by review must be a numeric value.";
      else if($csHoursDueByReview < MIN_CS_HOURS || $csHoursDueByReview > MAX_CS_HOURS)
        $errors['csHoursDueByReview'] = "CS hours due by review must be between " . MIN_CS_HOURS . " and " . MAX_CS_HOURS . ".";
    }

    //Validate forfeiture amount (numeric, in range)
    if ($forfeitureAmount != ""){
      if(!is_numeric($forfeitureAmount))
        $errors['forfeitureAmount'] = "Forfeiture amount must be a numeric value.";
      else if($forfeitureAmount < MIN_FORFEITURE_AMOUNT || $forfeitureAmount > MAX_FORFEITURE_AMOUNT)
        $errors['forfeitureAmount'] = "Forfeiture amount must be between $" . MIN_FORFEITURE_AMOUNT . " and $" . MIN_FORFEITURE_AMOUNT . ".";
    }

    //Validate hearing date (is a date, not in future, formatted ISO)
    if($nextHearingDate != ""){
      $nextHearingDate = str_replace('-','/', $nextHearingDate);
      if(!isDate($nextHearingDate))
        $errors['nextHearingDate'] = "Next hearing date must be a recognizable date.";
      else {
        $nextHearingDate = new DateTime($nextHearingDate);
        $_REQUEST['nextHearingDate'] = $nextHearingDate->format('Y-m-d');
      }//end if
    }//end if
  }//end if javaScriptValidated

  //Consistency checks
  if($hearingDate != "" && $nextHearingDate != "" && $hearingDate >= $nextHearingDate)
    $errors['nextHearingDate'] = "Next hearing date must be after hearing date.";

  if($csHoursStayed + $csHoursDueByReview != $csHoursOrdered)
    $errors['csHoursOrdered'] = "CS hours ordered must equal CS hours stayed plus CS hours due by review.";

  if(isset($csw) && ($csHoursOrdered == "" || $csHoursOrdered == 0))
    $errors['csHoursOrdered'] = "If community service is checked, CS hours ordered must be greater than 0.";

  if(!isset($csw) && $csHoursOrdered != "" && $csHoursOrdered != 0)
    $errors['csw'] = "Community service must be checked if CS hours ordered is greater than 0.";

  if(!isset($tapAfterSchool) && (isset($hcMonday) || isset($hcTuesday) || isset($hcWednesday) || isset($hcThursday)))
    $errors['tapAfterSchool'] = "TAP afterschool must be checked if a day of the week is checked.";

  if(!isset($dmvSuspension) && isset($dmvSuspensionStayed))
    $errors['dmvSuspension'] = "DMV suspension must be checked if DMV suspension stayed is checked.";

  if(!isset($dnrSuspension) && isset($dnrSuspensionStayed))
    $errors['dnrSuspension'] = "DNR suspension must be checked if DNR suspension stayed is checked.";

  if(!isset($forfeiture) && (isset($forfeitureStayed) || $forfeitureAmount != "" && $forfeitureAmount != 0))
    $errors['forfeiture'] = "Forfeiture must be checked if forfeiture stayed is checked or forfeiture amount is greater than 0.";

  if(isset($forfeiture) && ($forfeitureAmount == "" || $forfeitureAmount == 0))
    $errors['forfeitureAmount'] = "If forfeiture is checked, forfeiture amount must be greater than 0.";

  return $errors;
}
?>
