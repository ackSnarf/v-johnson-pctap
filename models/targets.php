<?php

/* This funtion retrieves the list of target types for case notes.  */
function getTargetTypes(){
    global $db;

    $query = 'SELECT targetTypeID, targetType
              from targets order by targetType';

    $statement = $db->prepare($query);
    $statement->execute();
    $results = $statement->fetchALL(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}

/* This function inserts a target type to the dropdown list. */
function insertTargetType($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into targets (targetType) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the target type to the loolUp list. */
function updateTargetTypes($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE targets set targetType=:value where targetTypeID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}


/* This function validates target inputs. */
function validateTargetTypeInputs( ) {
  $errors = array();

  return $errors;
}//end validateTargetInputs
?>
