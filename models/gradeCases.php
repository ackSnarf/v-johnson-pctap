<?php
require('models/grades.php');
require('models/subjects.php');

switch($action){
  case 'gradeList':
    $_SESSION['tapID'] = fieldValue($_REQUEST,'tapID', fieldValue($_SESSION,'tapID'));
    $grades = getGradeList();
    include('views/gradeList.php');
    break;
  case 'gradeDetails':
    $errors="";
    $subjects = getSubjectList();
    $details = getGradeDetails($_REQUEST['gradeID']);
    include ('views/gradeDetails.php');
    break;
  case 'gradeNew':
    $errors="";
    $subjects = getSubjectList();
    $details = "";
    include ('views/gradeDetails.php');
    break;
  case "gradeSaveNew":
  case "gradeUpdate":
    switch(true) {
      case isset($_REQUEST['btnSave']):
        $errors = validateGradeInputs();
        if(count($errors)==0) {
          if($action === "gradeUpdate"){
            updateGrade();
          } else {
            //$_SESSION['tapID'] = fieldValue($_REQUEST,'tapID', fieldValue($_SESSION,'tapID'));
            insertGrade();
          }
          header('Location: ?action=gradeList');
        } else {
            $subjects = getSubjectList();
            $details = $_REQUEST;
            include('views/gradeDetails.php');  //Show form again with error markers
        }//end if
        break;
      case isset($_REQUEST['btnCancel']):
        //Display the list
        header('Location: ?action=gradeList');
        break;
      case isset($_REQUEST['btnDelete']):
        $deleted = deleteGrade($_REQUEST['gradeID']);
        header('Location: ?action=gradeList');
        break;
    }//end switch
    break;
  }//end switch
?>
