<?php


// get list of staff for dropdown boxes
function getStaff(){
  global $db;

  $query = 'SELECT staffID, staffFirstName from staff
            where staffEndDate is null
            order by staffFirstName';

  $statement = $db->prepare($query);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

return $results;
}



/* This function gets a list of the employees and their IDs.
 */
 function staffDet ($staffID) {
        global $db;

        $myQuery = 'select * from staff
                    where staffID =:staffID';
		$statement = $db->prepare($myQuery);
        $statement->bindValue(':staffID',$staffID);
        $statement->execute();
        $results = $statement->fetch(PDO::FETCH_ASSOC);
        $statement->closeCursor();

		return $results;
    }

/* This function gets a list of the past staff and their IDs.
 */
	function getCurrentStaffList() {
        global $db;

        $myQuery = 'select staffID, concat(staffFirstName, " ", staffLastName) as staffName from staff
	                   where staffEndDate is null
                     order by staffLastName;' ;
		    $statement = $db->prepare($myQuery);

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

		return $results;
    }

/* This function gets a current list of the staff and their IDs.
 */
	function getAllStaffList() {
    global $db;

    $myQuery = 'select staffID,  concat(staffFirstName, " ",staffLastName) as staffName from staff
                  order by StaffLastName';
		$statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

		return $results;
  }

function updateStaffMember() {
        global $db;

        extract($_REQUEST);

        $myQuery = "update staff
                        SET
                        staffFirstName=:staffFirstName,
                        staffLastName=:staffLastName,
                        staffStartDate=:staffStartDate,
                        staffEndDate=:staffEndDate,
                        staffStreetAddress=:staffStreetAddress,
                        staffCity=:staffCity,
                        staffState=:staffState,
                        staffZipCode=:staffZipCode,
                        staffDob=:staffDob,
                        staffEmail=:staffEmail,
                        staffPhoneNumber=:staffPhoneNumber,
                        staffNote=:staffNote
                        where staffID =:staffID";
		$statement = $db->prepare($myQuery);
        $statement->bindValue(':staffID',$staffID);
        $statement->bindValue(':staffFirstName',$staffFirstName);
        $statement->bindValue(':staffLastName',$staffLastName);
        $statement->bindValue(':staffStartDate',$staffStartDate);
        $statement->bindValue(':staffEndDate',($staffEndDate==""?null:$staffEndDate));
        $statement->bindValue(':staffStreetAddress',($staffStreetAddress==""?null:$staffStreetAddress));
        $statement->bindValue(':staffCity',($staffCity==""?null:$staffCity));
        $statement->bindValue(':staffState',($staffState==""?null:$staffState));
        $statement->bindValue(':staffZipCode',($staffZipCode==""?null:$staffZipCode));
        $statement->bindValue(':staffDob',($staffDob==""?null:$staffDob));
        $statement->bindValue(':staffEmail',($staffEmail==""?null:$staffEmail));
        $statement->bindValue(':staffPhoneNumber',($staffPhoneNumber==""?null:formatPhone($staffPhoneNumber,"G")));
        $statement->bindValue(':staffNote',($staffNote==""?null:$staffNote));
        $statement->execute();
        $statement->closeCursor();

		return $statement->rowCount();
}

function saveNewStaff() {
        global $db;

        extract($_REQUEST);

        $myQuery = "INSERT  staff
                        set
                        staffFirstName=:staffFirstName,
                        staffLastName=:staffLastName,
                        staffStartDate=:staffStartDate,
                        staffEndDate=:staffEndDate,
                        staffStreetAddress=:staffStreetAddress,
                        staffCity=:staffCity,
                        staffState=:staffState,
                        staffZipCode=:staffZipCode,
                        staffDob=:staffDob,
                        staffEmail=:staffEmail,
                        staffPhoneNumber=:staffPhoneNumber,
                        staffNote=:staffNote";
		$statement = $db->prepare($myQuery);
        //$statement->bindValue(':staffID',$staffID);
        $statement->bindValue(':staffFirstName',$staffFirstName);
        $statement->bindValue(':staffLastName',$staffLastName);
        $statement->bindValue(':staffStartDate',$staffStartDate);
        $statement->bindValue(':staffEndDate',($staffEndDate==""?null:$staffEndDate));
        $statement->bindValue(':staffStreetAddress',($staffStreetAddress==""?null:$staffStreetAddress));
        $statement->bindValue(':staffCity',($staffCity==""?null:$staffCity));
        $statement->bindValue(':staffState',($staffState==""?null:$staffState));
        $statement->bindValue(':staffZipCode',($staffZipCode==""?null:$staffZipCode));
        $statement->bindValue(':staffDob',($staffDob==""?null:$staffDob));
        $statement->bindValue(':staffEmail',($staffEmail==""?null:$staffEmail));
        $statement->bindValue(':staffPhoneNumber',($staffPhoneNumber==""?null:formatPhone($staffPhoneNumber,"G")));
        $statement->bindValue(':staffNote',($staffNote==""?null:$staffNote));
        $statement->execute();
        $statement->closeCursor();

		return $statement->rowCount();
}

/* This function validates employee inputs.
	*/
	function validateStaffInputs( ) {
		$MINSALARY =  15000;			//Minimum employee salary
		$MAXSALARY = 120000;			//Maximum employee salary

		$errors = array();
		if($_REQUEST['javascriptValidated']=='false') {
			extract($_REQUEST);

			if ($staffFirstName=="")
				$errors['staffFirstName'] = "You must enter the employee&apos;s first name.";
			//end if

			if ($staffLastName=="")
			    $errors['staffLastName'] = "You must enter the employee&apos;s last name.";
			//end if

			//if (!isset($gender))
		//		$errors['gender'] = "Please designate this personapos;s gender.";
			//end if

		//	if ($plantId=="Missing")
		//		$errors['plantId'] = "You must select a plant from the list.";
			//endif

			if ($staffPhoneNumber!="") {
				$staffPhoneNumber = formatPhone($staffPhoneNumber,"G");  //unformat
				if (strlen($staffPhoneNumber)!=10)  //after unformatting
					$errors['$staffPhoneNumber'] = "Phone numbers must contain 7 or 10 digits.";
			}//end if

		//	if ($hireDate!="")
		//		if (!isDate($hireDate))
				//	$errors['hireDate'] = "That is not a recognizable date format.";
		//		else {
			//		$hireDate = new DateTime($hireDate);
			//		$today = new DateTime();
			//		if ($hireDate > $today)
			//			$errors['hireDate'] = "Hire date cannot be in the future.";
			//	}//endif
			//endif

			//if ($salary!="") {
			//	$salary = unformatValue($salary);
			//	if(!is_numeric($salary))
			//		$errors['salary'] = "This is not a valid number.";
			//	else if($salary<$MINSALARY || $salary>$MAXSALARY)
			//		$errors['salary'] = "Salary must be between $" . number_format($MINSALARY) . " and $" . number_format($MAXSALARY);
			//}//end if
		//}//end if

		//Consistency Check here

		return $errors;
	   }//end if
    }//end validateEmployeeInputs

?>
