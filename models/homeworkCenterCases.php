<?php
require('models/homeworkCenter.php');
require('models/hcattendStatuses.php');

switch($action){
  case 'homeworkCenterList':
    $_SESSION['courtOrderID'] = fieldValue($_REQUEST,'courtOrderID', fieldValue($_SESSION,'courtOrderID'));
    $hcattendances = getHomeworkCenterList();
    include('views/homeworkCenterList.php');
    break;
  case 'homeworkCenterDetails':
    $errors = "";
    $hcattendStatuses = getHcattendStatuses();
    $details = getHomeworkCenterDetails($_REQUEST['hcattendanceID']);
    include ('views/homeworkCenterDetails.php');
    break;
  case 'homeworkCenterNew':
    $errors = "";
    $hcattendStatuses = getHcattendStatuses();
    $details = "";
    include ('views/homeworkCenterDetails.php');
    break;
  case "homeworkCenterSaveNew":
  case "homeworkCenterUpdate":
  switch(true) {
    case isset($_REQUEST['btnSave']):
      $errors = validateHomeworkCenterInputs();
      if(count($errors)==0) {
          if($action==="homeworkCenterUpdate"){
            updateHomeworkCenter();
          }
          else {
            insertHomeworkCenter();
          }
          header('Location: ?action=homeworkCenterList');
      } else {
          $hcattendStatuses = getHcattendStatuses();
          $details = $_REQUEST;
          include('views/homeworkCenterDetails.php');  //Show form again with error markers
      }//end if
      break;
    case isset($_REQUEST['btnCancel']):
      //Display the list
      header('Location: ?action=homeworkCenterList');
      break;
    case isset($_REQUEST['btnDelete']):
      $deleted = deleteHomeworkCenter($_REQUEST['hcattendanceID']);
      header('Location: ?action=homeworkCenterList');
      break;
  }//end switch
  break;
}//end switch
?>
