<?php

/* This function gets the phone number types for the list box of phone number types from the db.
 */
    function getPhoneType(){
       global $db;

        $myQuery = 'select * from phoneNumberTypes
                    order by phoneNumberType';

        $statement = $db->prepare($myQuery);
        //$statement->bindValue(':filter',$_SESSION());
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }

?>
