<?php
require_once('models/caseNotes.php');
require_once('models/clients.php');
require_once('models/contacts.php');
require_once('models/courtOrders.php');
require_once('models/dischargeReasons.php');
require_once('models/genders.php');
require_once('models/gradeLevels.php');
require_once('models/hearings.php');
require_once('models/noteTypes.php');
require_once('models/races.php');
require_once('models/schools.php');
require_once('models/staff.php');
require_once('models/targets.php');


switch($action){
  case "caseNoteList":
      $_SESSION['tapID'] = fieldValue($_REQUEST,'tapID', fieldValue($_SESSION,'tapID'));
      $caseNotes = getCaseNoteList($_SESSION['tapID']);
        include("views/caseNoteList.php");
  break;
  case "caseNoteDetails":
      $errors = "";
      $targetTypes = getTargetTypes();
      $staff = getStaff();
      $noteTypes = getNoteTypes();
      $details = getCaseNoteDetails($_REQUEST['caseNoteID']);
      include("views/caseNoteDetails.php");
  break;
  case "caseNoteNew":
    $errors = "";
    $details = "";
    $targetTypes = getTargetTypes();
    $staff = getStaff();
    $noteTypes = getNoteTypes();
    include("views/caseNoteDetails.php");
  break;
  case "caseNoteUpdate":
  case "caseNoteSaveNew":
        //figure out which button was CLICKED
    switch(true){
        case isset($_REQUEST['btnSave']):
          $errors = validateCaseNoteInputs();
          if($action==='caseNoteUpdate'){
            updateCaseNotes();
          }else{
            insertCaseNotes($_SESSION['tapID']);
          }
          header('Location: ?action=caseNoteList');
        break;
        case isset($_REQUEST['btnCancel']):
            $caseNotes = getCaseNoteList($_SESSION['tapID']);
            include("views/caseNoteList.php");
        break;
        case isset($_REQUEST['btnDelete']):
          $deleted = deleteCaseNote($_REQUEST['caseNoteID']);
          header('Location: ?action=caseNoteList');
        break;
      }//end BUTTON switch

    break;
  }//endSwitch


 ?>
