<?php
require('models/hearings.php');
require('models/orderEnds.php');
require('models/caseNotes.php');

switch($action){
  case 'hearingList':
    $_SESSION['courtOrderID'] = fieldValue($_REQUEST,'courtOrderID', fieldValue($_SESSION,'courtOrderID'));
    $hearings = getHearingList();
    include('views/hearingList.php');
    break;
  case 'hearingDetails':
    $errors = "";
    $orderEnds = getOrderEndsList();
    $details = getHearingDetails($_REQUEST['hearingID']);
    include ('views/hearingDetails.php');
    break;
  case 'hearingNew':
    $errors = "";
    $orderEnds = getOrderEndsList();
    $details = "";
    include ('views/hearingDetails.php');
    break;
  case "hearingSaveNew":
  case "hearingUpdate":
  switch(true) {
    case isset($_REQUEST['btnSave']):
      $errors = validateHearingInputs();
      if(count($errors)==0) {
          if($action==="hearingUpdate"){
            updateHearing();
          }
          else {
            insertHearing();
          }
          header('Location: ?action=hearingList');
      } else {
          $orderEnds = getOrderEndsList();
          $details = $_REQUEST;
          include('views/hearingDetails.php');  //Show form again with error markers
      }//end if
      break;
    case isset($_REQUEST['btnCancel']):
      //Display the list
      header('Location: ?action=hearingList');
      break;
    case isset($_REQUEST['btnDelete']):
      $deleted = deleteHearing($_REQUEST['hearingID']);
      header('Location: ?action=hearingList');
      break;
  }//end switch
  break;
}//end switch
?>
