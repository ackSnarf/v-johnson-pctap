<?php
/* This function gets a list of the subjects. */
function getSubjectList() {
    global $db;

    $query = 'select subjectID, subjectName from subjects
                        order by subjectName';
    $statement = $db->prepare($query);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}//end getSubjectList



/* This function inserts a subject name into the dropdown list. */
function insertSubject($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into subjects (subjectName) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the subject name for the loolUp list. */
function updateSubjects($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE subjects set subjectName=:value where subjectID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
