<?php
    require_once('models/caseNotes.php');
    require_once('models/clients.php');
    require_once('models/contacts.php');
    require_once('models/contactTypes.php');
    require_once('models/genders.php');
    require_once('models/phones.php');
    require_once('models/phoneTypes.php');
    require_once('models/races.php');


    $new = false;   // this flag identifies a new contact entry

    switch($action){
        case 'contactList':
            $_SESSION['clientID'] = fieldValue($_REQUEST,'clientID', fieldValue($_SESSION,'clientID'));
            $details = getContactList($_SESSION['clientID']);
            include('views/contactList.php');

            /********DEBUG CODE (DO NOT DELETE)********/
            /*
            echo "THIS IS CONTACT LIST";
            echo "REQUEST ARRAY (ACTION, sometimes EDITINGCLIENT)";
            printArray($_REQUEST);
            echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
            printArray($_SESSION);
            echo "DETAILS ARRAY(CONTACTID, FNAME, LNAME, TITLE, EMERGENCYCONTACT)";
            printArray($details);
            8?
            /**********END DEBUG CODE*********/
            break;

        case 'contactNew':
            $new = true;
            switch(true){
                case isset($_REQUEST['btnSave']):
                    $errors = validateContactInputs();
                    if(count($errors)==0) {
                        //calls insertContact()
                            //inserts fname, lname, address, city, state, zip, email, contactTypeID, emergencyContact
                        //calls insertContactLink()
                            //inserts clientID, contactID link
                        //if phoneNumber not blank
                            //calls insertPhoneNumber() which inserts pn, pntypeID
                            //calls insertContactPhoneNumber() which inserts pnID, contactID link
                        $conID = createContact($_SESSION['clientID']);
                        header('Location: ?action=contactList');
                    } else {
                        //get all data back
                        $details = $_REQUEST;
                        $relationships = getContactTypesList();
                        $types = getPhoneType();
                        include('views/contactDetails.php');  //Show form again with error markers
                    }//end if
                    break;

                case isset($_REQUEST['btnCancel']):
                    header('Location: ?action=contactList');
                    break;

                default:
                    $details = "";
                    $errors = "";
                    $phones = "";
                    $relationships = getContactTypesList();
                    $types = getPhoneType();
                    include("views/contactDetails.php");
                    break;

            }//end case contactNew button switch
            break;

        case 'contactEdit':
            $new = false;
            switch(true){
                case isset($_REQUEST['btnDelete']):
                    //deletes pnID,pnTypeID,pn, and contactID/pnID link
                    deleteContactPhoneNumbers();
                    //deletes all details from contact table for contactID
                    deleteContact();
                    //deletes contactID/clientID link
                    deleteClientContactLink();
                    header('Location: ?action=contactList');
                    break;
                case isset($_REQUEST['btnSave']):
                    //echo "THIS IS CONTACT EDIT SAVE";
                    //checks if fname and lname not blank
                    $errors = validateContactInputs();
                    if(count($errors)==0) {

                        /********DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS IS 0, proceed to update functions";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        //echo "REQUEST ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)"; //This is what appears if no header
                        echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                            printArray($_REQUEST);
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($details);
                        //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                            printArray($contactDetails);
                        //echo "DEMOGRAPHICS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($demographics);
                        //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                            printArray($phones);
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        //echo "RACE ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "RACE ARRAY (RACEID, RACE)";
                            printArray($race);
                        //echo "GENDERS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "GENDERS ARRAY (GENDERID, GENDER)";
                            printArray($genders);
                        //echo "TAPENROLLMENT ARRAY (UNDEFINED)"; //This is what appears if no header

                        echo "TAPENROLLEMENT ARRAY (EMPTY??)";
                        printArray($tapEnrollment);
                        */
                        /**********END DEBUG CODE*********/

                        //updates fname, lname, address, city, state, zip, email, contactTypeID, emergencyContact
                        $contactRow = updateContact();

                        //echo "ROWCOUNTS: CONTACTROW: " . $contactRow;
                        header('Location: ?action=contactList');
                    } else {
                        //get all data back
                        $details = $_REQUEST;
                        $phones = getContactPhoneList($_REQUEST['contactID']);
                        $relationships = getContactTypesList();

                        /********DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS AFTER PHP VALIDATION, return to details page with errors and info on screen";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)";
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (ACTION, CONTACTTYPEID, CONTACTID, CLIENTID, COURTORDERID, JAVASCRIPTVALIDATED, ORGFNAME, ORGLNAME, FNAME, LNAME, KIDTRAXID, RACEID, GENDERID, DOB, ADDRESS, CITY, STATE, ZIP, EMAIL, btnSave)";
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                            printArray($demographics);
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                            printArray($phones);
                        echo "ERRORS ARRAY (all error messages)";
                            printArray($errors);
                        echo "RACE ARRAY (RACEID, RACE)";
                            printArray($race);
                        echo "GENDERS ARRAY (GENDERID, GENDER)";
                            printArray($genders);

                        echo "TAPENROLLEMENT ARRAY (EMPTY??)";
                        printArray($tapEnrollment);
                        */
                        /**********END DEBUG CODE*********/
                        include('views/contactDetails.php');  //Show form again with error markers
                    }//end if
                    break;
                case isset($_REQUEST['btnCancel']):
                    //echo "THIS IS CONTACT EDIT CANCEL";
                    header('Location: ?action=contactList&editingClient=0');
                    break;
                default:
                    //echo "THIS IS CONTACT EDIT DEFAULT";

                    $details = getContactDetails();
                    $errors = "";
                    $phones = getContactPhoneList($_REQUEST['contactID']);
                    $relationships = getContactTypesList();

                    /********DEBUG CODE (DO NOT DELETE)********/
                    /*
                    echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID(FOR CLIENT), CLIENTNAME)";
                        printArray($_SESSION);
                    echo "REQUEST ARRAY (ACTION, CONTACTID (FOR CONTACT), EDITINGCLIENT)";
                        printArray($_REQUEST);
                    echo "DETAILS ARRAY (CONTACTID(FOR CONTACT), FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                        printArray($details);
                    echo "ERRORS ARRAY (EMPTY)";
                        printArray($errors);
                    echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID(FOR CONTACT))";
                        printArray($phones);
                    echo "RELATIONSHIPS ARRAY (CONTACTTYPEID, CONTACTTITLE)";
                        printArray($relationships);
                    */
                    /**********END DEBUG CODE*********/
                    include('views/contactDetails.php');
                    break;

            }//end case contactEdit button switch
            break;

        default:
            echo 'Action ' . $action . ' is not recognized';
            break;

    }//end contact action switch
?>
