<?php
require_once('models/contactTypes.php');
require_once('models/dischargeReasons.php');
require_once('models/genders.php');
require_once('models/gradeLevels.php');
require_once('models/hcattendStatuses.php');
require_once('models/lookUp.php');
require_once('models/noteTypes.php');
require_once('models/phoneTypes.php');
require_once('models/races.php');
require_once('models/schools.php');
require_once('models/subjects.php');
require_once('models/targets.php');


    extract($_REQUEST);

    if(!isset($table)) $table="";

    switch($action){
        case 'lookUpSelect':
            switch ($table) {
                case 'contactTypes':     $nc=35; $items = getFullContactTypesList(); $dataId='contactTypeID';     $dataValue='contactTitle';        break;
                case 'dischargeReasons': $nc=50; $items = getDischargeReasons();     $dataId='dischargeReasonID'; $dataValue='dischargeReason';     break;
                case 'genders':          $nc=6;  $items = getGenders();              $dataId='genderID';          $dataValue='gender';              break;
                case 'gradeLevels':      $nc=10; $items = getGradeLevels();          $dataId='gradeLevelID';      $dataValue='gradeLevel';          break;
                case 'hcattendstatuses'; $nc=9;  $items = getHcattendStatuses();     $dataId='hcattendStatusID';  $dataValue='hcattendStatus';      break;
                case 'noteTypes':        $nc=20; $items = getNoteTypes();            $dataId='noteTypeID';        $dataValue='noteTypeDescription'; break;
                case 'races':            $nc=16; $items = getRace();                 $dataId='raceID';            $dataValue='race';                break;
                case 'schools':          $nc=20; $items = getSchools();              $dataId='schoolID';          $dataValue='schoolName';          break;
                case 'subjects':         $nc=15; $items = getSubjectList();          $dataId='subjectID';         $dataValue='subjectName';         break;
                case 'targets':          $nc=30; $items = getTargetTypes();          $dataId='targetTypeID';      $dataValue='targetType';          break;
                default:                 $nc=0;  $items = [];                        break;
            }
            include('views/lookUpList.php');
            break;

        case 'lookUpNew':
            switch ($table) {
                case 'contactTypes':     insertContactType($newDataValue);     break;
                case 'dischargeReasons': insertDischargeReason($newDataValue); break;
                case 'genders':          insertGender($newDataValue);          break;
                case 'gradeLevels':      insertGradeLevel($newDataValue);      break;
                case 'hcattendstatuses': insertHcattendStatus($newDataValue);  break;
                case 'noteTypes':        insertNoteType($newDataValue);        break;
                case 'races':            insertRace($newDataValue);            break;
                case 'schools':          insertSchool($newDataValue);          break;
                case 'subjects':         insertSubject($newDataValue);         break;
                case 'targets':          insertTargetType($newDataValue);      break;
            }
            header('Location: ?action=lookUpSelect&table=' . $table);
            break;

        case 'lookUpEdit':
            switch ($table) {
                case 'contactTypes':     updateContactTypes($dataId,$editDataValue);     break;
                case 'dischargeReasons': updateDischargeReasons($dataId,$editDataValue); break;
                case 'genders':          updateGenders($dataId,$editDataValue);          break;
                case 'gradeLevels':      updateGradeLevels($dataId,$editDataValue);      break;
                case 'hcattendstatuses': updateHcattendStatus($dataId,$editDataValue);   break;
                case 'noteTypes':        updateNoteTypes($dataId,$editDataValue);        break;
                case 'races':            updateRaces($dataId,$editDataValue);            break;
                case 'schools':          updateSchools($dataId,$editDataValue);          break;
                case 'subjects':         updateSubjects($dataId,$editDataValue);         break;
                case 'targets':          updateTargetTypes($dataId,$editDataValue);      break;
            }
            header('Location: ?action=lookUpSelect&table=' . $table);
            break;

        default: echo "Action $action is not recognized.";
            break;
    }
 ?>
