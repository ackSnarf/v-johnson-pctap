<?php

    /*
    This function creates a new contact in the database.
    */
    function createContact($clientID){
        //extract($_REQUEST);
        $conID = insertContact();
        insertClientContactLink($clientID,$conID);

        if ($_REQUEST['phoneNumber'] != "") {
            $pnID = insertPhoneNumber();
            insertContactPhoneNumber($pnID,$conID);
        }//end if
         return $conID;
    }//end createContact


    /*
    This function deletes the current selected contact.
    */
    function deleteContact(){
        global $db;

        $myQuery = 'DELETE FROM contacts
                    WHERE contactID = :contactID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$_REQUEST['contactID']);
        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }//end deleteContact


    /*
    This function deletes a linking contact, client link.
    */
    function deleteClientContactLink() {
        global $db;

        $myQuery = 'DELETE FROM clientContacts
                    WHERE contactID = :contactID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$_REQUEST['contactID']);
        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }//end deleteClientContactLink


    /*
    This function gets the details for the selected contact.
    */
    function getContactDetails() {
       global $db;


        $myQuery ='SELECT contactID,
                            contactFirstName,
                            contactLastName,
                            contactStreetAddress,
                            contactCity,
                            contactState,
                            contactZipCode,
                            contactEmail,
                            contactTypeID,
                            emergencyContact
                    FROM contacts
                    WHERE contactID=:contactID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$_REQUEST['contactID']);
        $statement->execute();
        $results = $statement->fetch(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }//end getContactDetails


    /*
    This function gets a list of the contacts, their relationship to the client,
    whether or not they are an emergency contact, and their IDs for the selected client.
    */
    function getContactList($clientID) {
        global $db;

        $myQuery ='SELECT contactID,
                            contactFirstName,
                            contactLastName,
                            contactTitle,
                            emergencyContact
					        FROM contacts
                  INNER JOIN contactTypes USING (contactTypeID)
					        INNER JOIN clientContacts using (contactID)
                  WHERE clientID = :clientID
                  AND contactTypeID != 5
					        ORDER BY emergencyContact desc, contactTitle, contactFirstName';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':clientID',$clientID);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }//end getContactList


    /*
    This function inserts a linking contact, client link.
    */
    function insertClientContactLink($clientID,$conID) {
        global $db;

        $myQuery = 'INSERT INTO clientContacts (clientID, contactID)
                    VALUES (:clientID, :contactID)';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':clientID',$clientID);
        $statement->bindValue(':contactID',$conID);
        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }//end insertClientContactLink


    /*
    This function inserts a contact.
    */
    function insertContact(){
        global $db;
        extract($_REQUEST);

        $myQuery ='INSERT INTO contacts
                                (contactFirstName,
                                contactLastName,
                                contactStreetAddress,
                                contactCity,
                                contactState,
                                contactZipCode,
                                contactEmail,
                                contactTypeID,
                                emergencyContact)
                        VALUES (:contactFirstName,
                                :contactLastName,
                                :contactStreetAddress,
                                :contactCity,
                                :contactState,
                                :contactZipCode,
                                :contactEmail,
                                :contactTypeID,
                                :emergencyContact)';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactFirstName',$contactFirstName);
        $statement->bindValue(':contactLastName',$contactLastName);
        $statement->bindValue(':contactStreetAddress',($contactStreetAddress==""?null:$contactStreetAddress));
        $statement->bindValue(':contactCity',($contactCity==""?null:$contactCity));
		    $statement->bindValue(':contactState',($contactState==""?null:$contactState));
		    $statement->bindValue(':contactZipCode',($contactZipCode==""?null:$contactZipCode));
		    $statement->bindValue(':contactEmail',($contactEmail==""?null:$contactEmail));
        $statement->bindValue(':contactTypeID',($contactTypeID));
        $statement->bindValue(':emergencyContact',isset($emergencyContact));
        $statement->execute();
        $statement->closeCursor();

        return $db->lastInsertId();

    }//end insertContact


    /*
    This function updates the details for the selected contact.
     */
    function updateContact() {
        global $db;
        extract($_REQUEST);

        $myQuery ='UPDATE contacts
                   SET
                        contactFirstName=:contactFirstName,
                        contactLastName=:contactLastName,
                        contactStreetAddress=:contactStreetAddress,
            						contactCity=:contactCity,
            						contactState=:contactState,
            						contactZipCode=:contactZipCode,
            						contactEmail=:contactEmail,
                        contactTypeID=:contactTypeID,
                        emergencyContact=:emergencyContact
                  WHERE contactID=:contactID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactFirstName',$contactFirstName);
        $statement->bindValue(':contactLastName',$contactLastName);
        $statement->bindValue(':contactStreetAddress',($contactStreetAddress==""?null:$contactStreetAddress));
        $statement->bindValue(':contactCity',($contactCity==""?null:$contactCity));
    		$statement->bindValue(':contactState',($contactState==""?null:$contactState));
    		$statement->bindValue(':contactZipCode',($contactZipCode==""?null:$contactZipCode));
    		$statement->bindValue(':contactEmail',($contactEmail==""?null:$contactEmail));
        $statement->bindValue(':contactTypeID',($contactTypeID));
        $statement->bindValue(':emergencyContact',isset($emergencyContact));
        $statement->bindValue(':contactID',$contactID);

        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }//end updateContact


    /*
    This function validates contact inputs if javascript is inactive.
	*/
	function validateContactInputs() {

		$errors = array();
		if($_REQUEST['javascriptValidated']=='false') {
			extract($_REQUEST);

			if ($contactFirstName=="")
				$errors['contactFirstName'] = "You must enter the contact&apos;s first name.";
			//end if

			if ($contactLastName=="")
				$errors['contactLastName'] = "You must enter the contact&apos;s last name.";
			//end if

		}//end if

		//Consistency Check here?

		return $errors;
	}//end validateContactInputs

?>
