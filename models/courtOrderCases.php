<?php
require_once('models/clients.php');
require_once('models/contacts.php');
require_once('models/courtOrders.php');
require_once('models/dischargeReasons.php');
require_once('models/genders.php');
require_once('models/gradeLevels.php');
require_once('models/phones.php');
require_once('models/races.php');
require_once('models/schools.php');

  $sideNav = false;   // this flag identifies a new court order

switch($action){
  case "courtOrderDetails":
      $errors="";
      $courtOrderDetails = getCourtOrderDetails($_REQUEST['courtOrderID']);
      //most of these should already be in the session array... shouldn't need to reset them....but i might be anyway?
      $_SESSION['contactID'] = fieldValue($courtOrderDetails, 'contactID', fieldValue($_SESSION, 'contactID'));
      $_SESSION['clientID'] = fieldValue($courtOrderDetails, 'clientID', fieldValue($_SESSION, 'clientID'));
      $_SESSION['tapID'] = fieldValue($courtOrderDetails,'tapID', fieldValue($_SESSION,'tapID'));
      $_SESSION['courtOrderID']= fieldValue($_REQUEST,'courtOrderID', fieldValue($_SESSION,'courtOrderID'));
      $_SESSION['caseNumber'] = fieldValue($courtOrderDetails,'caseNumber', fieldValue($_SESSION,'caseNumber'));
      $gradeLevels = getGradeLevels();
      $dischargeReasons = getDischargeReasons();
      $schools = getSchools();
      include("views/courtOrderDetails.php");
  break;
  case "courtOrderNew":
      $sideNav = true;
      $errors="";
      $_SESSION['clientID'] = fieldValue($_REQUEST,'clientID', fieldValue($_SESSION,'clientID'));
      $courtOrderDetails = "";
      $gradeLevels = getGradeLevels();
      $dischargeReasons = getDischargeReasons();
      $schools = getSchools();
      include("views/courtOrderDetails.php");
  break;
  case "courtOrderSaveNew":
  case "courtOrderUpdate":
    $sideNav = false;
        switch(true) {
          case isset($_REQUEST['btnSave']):
            $errors = validateCourtOrderInputs();
            if(count($errors)==0) {
              if($action === "courtOrderUpdate"){
                $_REQUEST['clientID'] = $_SESSION['clientID'];
                $_REQUEST['contactID'] = $_SESSION['contactID'];
                $_REQUEST['tapID'] = $_SESSION['tapID'];
                updateCourtOrder();
                $contactDetails = getContactDetails($_REQUEST['contactID']);
                $demographics = getClientDemogJ($_REQUEST['contactID']);
                $phones = getContactPhoneList($_REQUEST['contactID']);
                $details = array_merge($contactDetails, $demographics);
                $tapEnrollment = getCourtOrdersList();// calling different function
                $race = getRace();
                $genders = getGenders();
                $_SESSION['contactID'] = $_REQUEST['contactID'];
                $_SESSION['clientID'] = $_REQUEST['clientID'];
                $_SESSION['tapID'] = $_REQUEST['tapID'];
                $new = false;
                $errors="";
                $_REQUEST['action']= "clientEdit";
                include("views/clientDetails.php");
              } else {
                $_REQUEST['clientID']=$_SESSION['clientID'];
                insertCourtOrder();
                $_SESSION['tapID'] = $_REQUEST['tapID'];
                $_REQUEST['contactID']=$_SESSION['contactID'];
                $tapEnrollment = getClientTapEnrollmentDetails($_SESSION['contactID']);
                $contactDetails = getContactDetails($_REQUEST['contactID']);
                $demographics = getClientDemogJ($_REQUEST['contactID']);
                $phones = getContactPhoneList($_REQUEST['contactID']);
                $details = array_merge($contactDetails, $demographics);
                $race = getRace();
                $genders = getGenders();
                $_SESSION['contactID']=$_REQUEST['contactID'];
                $_SESSION['clientID']=$_REQUEST['clientID'];
                $new = false;
                $errors="";
                $_REQUEST['action']= "clientEdit";
                include("views/clientDetails.php");
              }
        //      header('Location: ?action=gradeList');  NOT USING THE HEADER ACTION LIKE KAREN DID
              } else {
                $gradeLevels = getGradeLevels();
                $dischargeReasons = getDischargeReasons();
                $schools = getSchools();
                $courtOrderDetails = $_REQUEST; //MIGHT WORK
                include('views/courtOrderDetails.php');  //SHOW FORM AGAIN WITH FUCKING ERROR MARKERS
              }//end if
              break;
              case isset($_REQUEST['btnCancel']):
                //GOES BACK TO CLIENT DETAILS WITHOUT A HITCH????
                $new = false;
                $errors="";
                $tapEnrollment = getClientTapEnrollmentDetails($_SESSION['contactID']);
                $_REQUEST['contactID']=$_SESSION['contactID'];
                $_REQUEST['clientID']=$_SESSION['clientID'];
                $contactDetails = getContactDetails($_REQUEST['contactID']);
                $demographics = getClientDemogJ($_REQUEST['contactID']);
                $phones = getContactPhoneList($_REQUEST['contactID']);
                $details = array_merge($contactDetails, $demographics);
                $race = getRace();
                $genders = getGenders();
                $_SESSION['contactID']=$_REQUEST['contactID'];
                $_SESSION['clientID']=$_REQUEST['clientID'];
                $_REQUEST['action']= "clientEdit";
                include("views/clientDetails.php");
                break;
              }//end BUTTON switch
              break;
            }//end switch
          ?>
