<?php
	require('models/users.php');
    switch($action) {
    	case 'userLogin':
  			$userName = (isset($_REQUEST['userName']))?$_REQUEST['userName']:"";
  			$password = (isset($_REQUEST['password']))?$_REQUEST['password']:"";

  			if (isAuthorizedUser($userName, $password)) {
  				$_SESSION['authorizedUser'] = true;
					$_SESSION['userName'] = $userName;
					$_SESSION['userTypeID'] = getUserTypeID($_SESSION['userName']);
  				header('Location: .?action=clientList');
  			}else{
  				if($userName=="")
  					$loginMessage = " Only those with the correct credentials may access this website.";
  				else
  					$loginMessage = "Invalid username or password.";
  				//end if
  				include('views/login.php');
  			}//end if
  			break;

      case 'userLogout':
        $_SESSION = array();
        session_destroy();  //Is this line needed?
        $loginMessage = "You have been logged out.";
        $userName="";
        $password="";
        include('views/login.php');
			  break;
	  }//end switch
?>
