<?php

/* This function gets the note types.  */
function getNoteTypes(){
    global $db;

    $query = 'SELECT noteTypeID, noteTypeDescription from noteTypes order by noteTypeDescription';

    $statement = $db->prepare($query);
    $statement->execute();
    $results = $statement->fetchALL(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}


/* This function updates the note type on the loolUp list. */
function updateNoteTypes($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE noteTypes set noteTypeDescription=:value where noteTypeID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

  return ($statement->rowCount() == 1);
}


/* This function inserts a new note type into the dropdown list. */
function insertNoteType($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into noteTypes (noteTypeDescription) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}
?>
