<?php

// Retrieves just demographic information for the client used on clientDetails page
function  getClientDemog($id){
  global $db;


  $query = 'SELECT clientID, contactFirstName, contactLastName, kidtraxID, dob, race, raceID, gender, genderID
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join races using(raceID)
            join genders using(genderID)
            where contactTypeID=5 and clientID=:clientID';


  $statement = $db->prepare($query);
  $statement->bindValue(':clientID', $id);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;

}

// Retrieves just demographic information for the client used on clientDetails page **JES'S VERSION**
function  getClientDemogJ($id){
  global $db;


  $query = 'SELECT clientID, kidtraxID, dob, raceID, genderID
            from clients
            join clientcontacts using (clientID)
            join contacts using (contactID)
            where contactTypeID=5 and contactID=:contactID';


  $statement = $db->prepare($query);
  $statement->bindValue(':contactID', $id);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}//end getClientDemogJ

// This function updates the client demographic data in the client table
function updateClientDemog(){
  global $db;
  extract($_REQUEST);

  $myQuery ='UPDATE clients
             SET
                  kidtraxID=:kidtraxID,
                  dob=:dob,
                  raceID=:raceID,
                  genderID=:genderID
              WHERE clientID=:clientID';
  $statement = $db->prepare($myQuery);
  $statement->bindValue(':kidtraxID',($kidtraxID==""?null:$kidtraxID));
  $statement->bindValue(':dob',($dob==""?null:$dob));
  $statement->bindValue(':raceID',$raceID);
  $statement->bindValue(':genderID',$genderID);
  $statement->bindValue(':clientID',$clientID);

  $statement->execute();
  $statement->closeCursor();

  return $statement->rowCount();
}


// Fills the client table when a new client is created
function insertClientDetails(){
  global $db;
  extract($_REQUEST);

  $myQuery ='INSERT INTO clients (raceID,  genderID,  kidtraxID, dob)
             VALUES (:raceID, :genderID, :kidtraxID, :dob)';

      $statement = $db->prepare($myQuery);
      $statement->bindValue(':raceID',$raceID);
      $statement->bindValue(':genderID',$genderID);
      $statement->bindValue(':kidtraxID',($kidtraxID==""?null:$kidtraxID));
      $statement->bindValue(':dob',($dob==""?null:$dob));

      $statement->execute();
      $statement->closeCursor();

      $clientID = $db->lastInsertId();
      $_SESSION['clientID'] = $clientID;

      return $statement->rowCount();
}

// Retrieves the list of CURRENT client names for clientList
function getClientList(){
  global $db;

  $query = 'SELECT distinct contactID, contactTypeID, concat(contactFirstName, " ",contactLastName) as clientName from contacts
             join contacttypes using (contactTypeID)
             join clientcontacts using (contactID)
             join clients using (clientID)
             join taps using (clientID)
             join courtOrders using (tapID)
             where contactTypeID = 5 and dischargeDate is null
             order by contactLastName';

  $statement = $db->prepare($query);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}

// Retrieves full list of clients for search box on clientList page
function getFullClientList(){
  global $db;

        $myQuery = 'SELECT contactID, concat(contactFirstName, " ",contactLastName) as clientName from contacts
	                where contactTypeID = 5
                  order by contactLastName';

        $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }


// Retrieves details related to the client needed for selecting caseNumber
function getClientDetails($id){
  global $db;

  $query = 'SELECT contactID, contactFirstName, contactLastName, kidtraxID, dob, clientID, referralDate, dischargeDate, caseNumber
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join taps using (clientID)
            join courtorders using (tapID)
            where contactID=:contactID
            order by referralDate desc';

  $statement = $db->prepare($query);
  $statement->bindValue(':contactID', $id);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}

/*This function is called upon return to the clientDetails page with an updated enroll roster.
it's used in the second part of the IF in clientDetails.php
*/
function getClientTapEnrollDetails() {
  global $db;
  extract($_SESSION);

  $query = 'SELECT contactID, contactFirstName, contactLastName, kidtraxID, dob, clientID, referralDate, dischargeDate, caseNumber, courtOrderID, tapID
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join taps using (clientID)
            join courtorders using (tapID)
            where contactID=:contactID
            order by referralDate desc';

  $statement = $db->prepare($query);
  $statement->bindValue(':contactID', $contactID);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}

// THIS IS a template if we want to use it
/*
funtion getFullClientList(){
'SELECT distinct contactID, contactTypeID, concat(contactFirstName, " ",contactLastName) as clientName, caseNumber, dischargeDate, kidtraxID from contacts
             join contacttypes using (contactTypeID)
             join clientcontacts using (contactID)
             join clients using (clientID)
             join taps using (clientID)
             join courtOrders using (tapID)
             where contactTypeID = 5
             order by contactLastName, dischargeDate desc';


}
*/

// This function retrieves the full list of tapEnrollments for a given client
function getClientTapEnrollmentDetails($id){
  global $db;

  $query = 'SELECT contactID, contactFirstName, contactLastName, kidtraxID, dob, clientID, referralDate, dischargeDate, caseNumber, courtOrderID, tapID
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join taps using (clientID)
            join courtorders using (tapID)
            where contactID=:contactID
            order by referralDate desc';

  $statement = $db->prepare($query);
  $statement->bindValue(':contactID', $id);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}


    /*
    This function validates client inputs if javascript is inactive.
	*/
	function validateClientInputs() {

		$errors = array();
		if($_REQUEST['javascriptValidated']=='false') {
			extract($_REQUEST);

			if ($contactFirstName=="")
				$errors['contactFirstName'] = "You must enter the contact&apos;s first name.";
			//end if

			if ($contactLastName=="")
				$errors['contactLastName'] = "You must enter the contact&apos;s last name.";
			//end if

		}//end if

		//Consistency Check here?

		return $errors;
	}//end validateClientInputs

?>
