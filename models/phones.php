<?php

    /*
    This function gets one phone number and its details for the contactID and phoneNumberID sent in.
    */
    function getContactPhoneDetails() {
        global $db;

        $myQuery = 'SELECT  phoneNumber,
                            phoneNumberID,
                            phoneNumberType,
                            phoneNumberTypeID,
                            contactID
                    FROM phoneNumbers
                    INNER JOIN contactPhoneNumbers using (phoneNumberID)
                    INNER JOIN phoneNumberTypes using(phoneNumberTypeID)
                    WHERE contactID = :contactID
                    AND phoneNumberID = :phoneNumberID';
        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$_REQUEST['contactID']);
        $statement->bindValue(':phoneNumberID',$_REQUEST['phoneNumberID']);
        $statement->execute();
        $results = $statement->fetch(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }//end getContactPhoneDetails


    /*
    This function gets the multiple phone numbers for the contactID sent in.
    */
    function getContactPhoneList($id) {
        global $db;

        $myQuery = 'SELECT  phoneNumber,
                            phoneNumberType,
                            phoneNumberID,
                            contactID
                    FROM phoneNumbers
                    INNER JOIN contactPhoneNumbers using (phoneNumberID)
                        INNER JOIN phoneNumberTypes using (phoneNumberTypeID)
                    WHERE contactID = :contactID';
        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$id);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        return $results;
    }//end getContactPhoneList


    /*
    This function deletes A SINGLE phone number for the current phoneNumberID in the phoneNumbers table.
    This function ALSO deletes the link between the contact and the phone number in the contactPhoneNumbers table.
    (CAN ALTER THIS WHEN/IF WE REMOVE LINKING TABLE)
     */
     function deleteContactPhoneDetails(){
        global $db;

        $myQuery = 'DELETE p, c
                    FROM phoneNumbers p
                    INNER JOIN contactPhoneNumbers c using (phoneNumberID)
                    WHERE phoneNumberID = :phoneNumberID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':phoneNumberID',$_REQUEST['phoneNumberID']);
        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
     }//end deleteContactPhoneDetails


    /*
    This function deletes ALL phone numbers for the current selected contact.
    This function ALSO deletes the links between the contact and the phone numbers in the contactPhoneNumbers table.
    (CAN ALTER THIS WHEN/IF WE REMOVE LINKING TABLE)
     */
     function deleteContactPhoneNumbers(){
        global $db;

        $myQuery = 'DELETE p, c
                    FROM phoneNumbers p
                    INNER JOIN contactPhoneNumbers c using (phoneNumberID)
                    WHERE contactID = :contactID';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':contactID',$_REQUEST['contactID']);
        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
     }//end deleteContactPhoneNumbers


    /*
    This function inserts a linking contact, phone link.
    */
    function insertContactPhoneNumber($pnID,$conID) {
        global $db;

        $myQuery = 'INSERT INTO contactPhoneNumbers
                                (phoneNumberID,
                                contactID)
                    VALUES      (:phoneNumberID,
                                :contactID)';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':phoneNumberID',$pnID);
        $statement->bindValue(':contactID',$conID);
        $statement->execute();
        $statement->closeCursor();

        return $db->lastInsertId();
    }//end insertContactPhoneNumber


    /*
    This function inserts the detail data for the phone with the given ID.
     */
    function insertPhoneNumber() {
        global $db;
        extract($_REQUEST);

        $myQuery = 'INSERT INTO phoneNumbers
                                (phoneNumber,
                                phoneNumberTypeID)
                        VALUES (:phoneNumber,
                                :phoneNumberTypeID)';

        $statement = $db->prepare($myQuery);
        $statement->bindValue(':phoneNumber',formatPhone($phoneNumber,"G"));
        $statement->bindValue(':phoneNumberTypeID',$phoneNumberTypeID);
        $statement->execute();
        $statement->closeCursor();

        return $db->lastInsertId();
    }//end insertPhoneNumber


    /*
    This function updates the detail data for the phone number with the given ID.
     */
    function updateContactPhoneDetails() {
        global $db;
        extract($_REQUEST);

        $myQuery = 'UPDATE phoneNumbers
                    SET
                        phoneNumber = :phoneNumber,
                        phoneNumberTypeID = :phoneNumberTypeID
                    WHERE phoneNumberID = :phoneNumberID';
        $statement = $db->prepare($myQuery);
        $statement->bindValue(':phoneNumber',formatPhone($phoneNumber,"G"));
        $statement->bindValue(':phoneNumberTypeID',$phoneNumberTypeID);
        $statement->bindValue(':phoneNumberID',$phoneNumberID);

        $statement->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }//end updateContactPhoneDetails



    /*
    This function validates phone inputs if javascript is inactive.
	*/
	function validatePhoneInputs( ) {

		$errors = array();
		if($_REQUEST['javascriptValidated']=='false') {
			extract($_REQUEST);

            if ($phoneNumber!="") {
				$phone = formatPhone($phoneNumber,"G");  //unformat
				if (strlen($phone) != 10 && strlen($phone)!=14)  //after unformatting
					$errors['phoneNumber'] = "Phone numbers must contain 10 or 14 digits.";
			}//end if

		}//end if


		//Consistency Check here?

		return $errors;
	}//end validatePhoneInputs



//THIS FUNCTION NEEDS MORE TESTING AFTER DATABASE ALTERED TO ALLOW 14 DIGITS
    /*
       This function formats a phone number as (###) ###-#### for 10 digits and (###) ###-####-#### for 14 digits.
       The phone number must be unformatted.
    */
    function displayPhoneNumber($number){
        $length = strlen($number);

        if ($length == 14) {
           return "(".substr($number, 0, 3). ") ".substr($number, 3, 3). "-".substr($number, 6, 4). "-".substr($number, 10);
        } else {
           return "(".substr($number, 0, 3). ") ".substr($number, 3, 3). "-".substr($number, 6);
        }//end if

     }//end displayPhoneNumber


?>
