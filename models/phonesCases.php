<?php
    require_once('models/phones.php');
    require_once('models/phoneTypes.php');

    switch($action){
        case 'phoneEdit' :
            switch(true){
                case isset($_REQUEST['btnSave']):
                    //echo "THIS IS PHONE EDIT SAVE";

                    $errors = validatePhoneInputs();
                    if(count($errors)==0) {

                        /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS IS 0, proceed to update functions";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        //echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)"; //This is what appears if no header
                        echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                            printArray($_REQUEST);
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, KIDTRAX, DOB, CLIENTID, REFDATE, DISDATE, CASE#, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, RACE, RACEID, GENDER, GENDERID)";
                            printArray($details);
                        //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)"; printArray($contactDetails);
                        //echo "DEMOGRAPHICS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($demographics);
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                        //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($phones);
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        */
                        /**********END CLIENT PHONE DEBUG CODE*********/

                        /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS IS 0, proceed to update functions";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID (FOR CLIENT), CLIENTNAME)";
                            printArray($_SESSION);
                        //echo "REQUEST ARRAY (ACTION, CONTACTID (FOR CONTACT), PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)"; //This is what appears if no header
                        echo "REQUEST ARRAY (ACTION, CONTACTID (FOR CONTACT), EDITINGCLIENT)";
                            printArray($_REQUEST);
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, KIDTRAX, DOB, CLIENTID, REFDATE, DISDATE, CASE#, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, RACE, RACEID, GENDER, GENDERID)";
                            printArray($details);
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                        //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($phones);
                        echo "RELATIONSHIPS ARRAY (CONTACTTYPEID, CONTACTTITLE)";
                        //echo "RELATIONSHIPS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($relationships);
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        */
                        /**********END CONTACT PHONE DEBUG CODE*********/

                        if ($_REQUEST['phoneNumber'] != "") {
                            //updates pn, pntypeID
                            $phoneRow = updateContactPhoneDetails();
                            //echo "ROWCOUNTS: PHONEROW: " . $phoneRow;
                        }//end if
                        if ($_REQUEST['sender'] == "contact") {
                            header('Location:?action=contactEdit&contactID=' . $_REQUEST['contactID']);
                        } else {
                            header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
                        }//end if
                    } else {
                        //get the data back
                        $details = $_REQUEST;
                        $types = getPhoneType();

                        /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS AFTER PHP VALIDATION, return to details page with errors and info on screen";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                            printArray($demographics);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (PNTYPEID, PNTYPE)";
                            printArray($types);
                        echo "ERRORS ARRAY (all error messages)";
                            printArray($errors);
                        */
                        /**********END CLIENT PHONE DEBUG CODE*********/

                        /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS AFTER PHP VALIDATION, return to details page with errors and info on screen";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                            printArray($demographics);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (PNTYPEID, PNTYPE)";
                            printArray($types);
                        echo "ERRORS ARRAY (all error messages)";
                            printArray($errors);
                        */
                        /**********END CONTACT PHONE DEBUG CODE*********/

                        include("views/phoneEdit.php");  //Show form again with error markers
                    }//end if
                    break;

                case isset($_REQUEST['btnDelete']):
                    //echo "THIS IS PHONE EDIT DELETE";

                    //deletes pnID, pn, pnTypeID
                    //deletes pnID, contactID link
                    $deleteRow = deleteContactPhoneDetails();
                    if ($_REQUEST['sender'] == "contact") {
                        //echo "ROWCOUNTS: DELETEROW: " . $deleteRow;
                        header('Location:?action=contactEdit&contactID=' . $_REQUEST['contactID']);
                    } else {
                        //echo "ROWCOUNTS: DELETEROW: " . $deleteRow;
                        header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
                    }//end if
                    break;

                case isset($_REQUEST['btnCancel']):
                    //echo "THIS IS PHONE EDIT CANCEL";

                    //IF EDITING FOR CLIENT GO BACK TO CLIENTS PAGE, ELSE GO BACK TO CONTACTS PAGE
                    if ($_REQUEST['sender'] == "contact") {
                        
                        /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "THIS IS PHONE EDIT CANCEL: CLIENTS";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                        //echo "SESSION ARRAY (EMPTY)"; //This is what appears if no header
                        printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                        //echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnCancel)"; //This is what appears if no header
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID,  KIDTRAXID, DOB, RACEID, GENDERID)";
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                        //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                        //echo "DEMOGRAHPICS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($demographics);
                        //echo "ERRORS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (UNDEFINED)";
                            printArray($types);
                        */
                        /**********END CONTACT PHONE DEBUG CODE*********/

                       header('Location:?action=contactEdit&contactID=' . $_REQUEST['contactID']);
                    } else {

                        /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "THIS IS PHONE EDIT CANCEL: CLIENTS";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                        printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                        //echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnCancel)"; //This is what appears if no header
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID,  KIDTRAXID, DOB, RACEID, GENDERID)";
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)";
                        //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                        //echo "DEMOGRAHPICS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($demographics);
                        //echo "ERRORS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (UNDEFINED)";
                            printArray($types);
                        */
                        /**********END CLIENT PHONE DEBUG CODE*********/
                        header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
                    }//end if
                    break;

                default:
                    //echo "THIS IS PHONE EDIT DEFAULT";

                    $errors = "";

                    //(PHONENUMBER, PHONENUMBERID, PHONENUMBERTYPE, PHONENUMBERTYPEID, CONTACTID)
                    $details = getContactPhoneDetails();

                    //(PHONENUMBERTYPEID, PHONENUMBERTYPE)
                    $types = getPhoneType();

                    /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                    /*
                    echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                        printArray($_SESSION);
                    echo "REQUEST ARRAY (ACTION, PHONENUMBERID, CONTACTID, EDITINGCLIENT)";
                        printArray($_REQUEST);
                    echo "DETAILS ARRAY (PHONENUMBER, PHONENUMBERID, PHONENUMBERTYPE, PHONENUMBERTYPEID, CONTACTID)";
                        printArray($details);
                    echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                        printArray($contactDetails);
                    echo "ERRORS ARRAY (EMPTY)";
                        printArray($errors);
                    echo "PHONES ARRAY (UNDEFINED)";
                        printArray($phones);
                    echo "TYPES ARRAY (PHONENUMBERTYPEID, PHONENUMBERTYPE)";
                        printArray($types);
                    */
                    /**********END CLIENT PHONE DEBUG CODE*********/

                    /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                    /*
                    echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                        printArray($_SESSION);
                    echo "REQUEST ARRAY (ACTION, PHONENUMBERID, CONTACTID, EDITINGCLIENT)";
                        printArray($_REQUEST);
                    echo "DETAILS ARRAY (PHONENUMBER, PHONENUMBERID, PHONENUMBERTYPE, PHONENUMBERTYPEID, CONTACTID)";
                        printArray($details);
                    echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                        printArray($contactDetails);
                    echo "ERRORS ARRAY (EMPTY)";
                        printArray($errors);
                    echo "PHONES ARRAY (UNDEFINED)";
                        printArray($phones);
                    echo "TYPES ARRAY (PHONENUMBERTYPEID, PHONENUMBERTYPE)";
                        printArray($types);
                    */
                    /**********END CONTACT PHONE DEBUG CODE*********/
                    include("views/phoneEdit.php");
                    break;

                }//end case phoneEdit button switch
            break;

        case "phoneNew" :
            switch(true){
                case isset($_REQUEST['btnSave']):
                    //echo "THIS IS PHONE NEW SAVE";
                    $errors = validatePhoneInputs();
                    if(count($errors)==0) {

                        /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS IS 0, proceed to update functions";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        //echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)"; //This is what appears if no header
                        echo "REQUEST ARRAY (ACTION, CONTACTID, EDITINGCLIENT)";
                            printArray($_REQUEST);
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($details);
                        //echo "CONTACTDETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "CONTACTDETAILS ARRAY (CONTACTID, FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT)"; printArray($contactDetails);
                        //echo "DEMOGRAPHICS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DEMOGRAPHICS ARRAY (CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($demographics);
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                        //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($phones);
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        */
                        /**********END CLIENT PHONE DEBUG CODE*********/

                        /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "ERRORS IS 0, proceed to update functions";
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID(FOR CLIENT), CLIENTNAME)";
                            printArray($_SESSION);
                        //echo "REQUEST ARRAY (ACTION, CONTACTID (FOR CONTACT), PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)"; //This is what appears if no header
                        echo "REQUEST ARRAY (ACTION, CONTACTID (FOR CONTACT), EDITINGCLIENT)";
                            printArray($_REQUEST);
                        //echo "DETAILS ARRAY (UNDEFINED)"; //This is what appears if no header
                        echo "DETAILS ARRAY (CONTACTID (FOR CONTACT), FNAME, LNAME, ADDRESS, CITY, STATE, ZIP, EMAIL, CONTACTTYPEID, EMERGENCYCONTACT, CLIENTID, KIDTRAXID, DOB, RACEID, GENDERID)";
                            printArray($details);
                        echo "PHONES ARRAY (PHONENUMBER, PNTYPE, PNID, CONTACTID)";
                        //echo "PHONES ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($phones);
                        echo "RELATIONSHIPS ARRAY (CONTACTTYPEID, CONTACTTITLE)";
                        //echo "RELATIONSHIPS ARRAY (UNDEFINED)"; //This is what appears if no header
                            printArray($relationships);
                        echo "ERRORS ARRAY (EMPTY)";
                            printArray($errors);
                        */
                        /**********END CONTACT PHONE DEBUG CODE*********/
                        if ($_REQUEST['phoneNumber'] != "") {
                            $pnID = insertPhoneNumber();
                            insertContactPhoneNumber($pnID,$_REQUEST['contactID']);
                        }//end if

                        if ($_REQUEST['sender'] == "contact") {
                            header('Location:?action=contactEdit&contactID=' . $_REQUEST['contactID']);
                        } else {
                            header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
                        }//end if

                    } else {
                         //echo "ERRORS AFTER PHP VALIDATION, return to details page with errors and info on screen";
                        $details = $_REQUEST;
                        $types = getPhoneType();

                        /********CLIENT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                            printArray($demographics);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (PNTYPEID, PNTYPE)";
                            printArray($types);
                        echo "ERRORS ARRAY (all error messages)";
                            printArray($errors);
                        */
                        /**********END CLIENT PHONE DEBUG CODE*********/

                        /********CONTACT PHONE DEBUG CODE (DO NOT DELETE)********/
                        /*
                        echo "SESSION ARRAY (CLIENTSEARCH, CLIENTID, CONTACTID, CLIENTNAME)";
                            printArray($_SESSION);
                        echo "REQUEST ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($_REQUEST);
                        echo "DETAILS ARRAY (ACTION, CONTACTID, PHONENUMBERID, JAVASCRIPTVALIDATED, EDITINGCLIENT, PHONENUMBER, PHONENUMBERTYPEID, btnSave)";
                            printArray($details);
                        echo "CONTACTDETAILS ARRAY (UNDEFINED)";
                            printArray($contactDetails);
                        echo "DEMOGRAPHICS ARRAY (UNDEFINED)";
                            printArray($demographics);
                        echo "PHONES ARRAY (UNDEFINED)";
                            printArray($phones);
                        echo "TYPES ARRAY (PNTYPEID, PNTYPE)";
                            printArray($types);
                        echo "ERRORS ARRAY (all error messages)";
                            printArray($errors);
                        */
                        /**********END CONTACT PHONE DEBUG CODE*********/
                        include("views/phoneEdit.php");  //Show form again with error markers
                    }//end if
                    break;

                case isset($_REQUEST['btnCancel']):
                    //IF ADDING FOR CLIENT GO BACK TO CLIENTS PAGE, ELSE GO BACK TO CONTACTS PAGE
                    if ($_REQUEST['sender'] == "contact") {
                        header('Location:?action=contactEdit&contactID=' . $_REQUEST['contactID']);
                    } else {
                        header('Location:?action=clientEdit&contactID=' . $_REQUEST['contactID']);
                    }//end if
                    break;

                default:
                    $errors = "";
                    $details = "";
                    $types = getPhoneType();
                    include("views/phoneEdit.php");
                    break;

                }//end case phoneNew button switch
            break;

        default:
            echo "Action $action is not recognized";
            break;

    }// end phone action switch
?>
