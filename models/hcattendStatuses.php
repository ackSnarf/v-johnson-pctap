<?php
/* This function gets a list of the homework center attendance statuses. */
    function getHcattendStatuses(){
      global $db;

      $query = 'SELECT * from hcattendstatuses';

      $statement = $db->prepare($query);
      $statement->execute();
      $results = $statement->fetchAll(PDO::FETCH_ASSOC);
      $statement->closeCursor();

      return $results;
    }


/* This function inserts a new homework center attendance
status type into the dropdown list. */
function insertHcattendStatus($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into hcattendstatuses (hcattendStatus) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates a value selected in the homework
center attendance loolUp list. */
function updateHcattendStatus($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE hcattendstatuses set hcattendStatus=:value where hcattendStatusID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
