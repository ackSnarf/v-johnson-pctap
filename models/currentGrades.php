<?php

/* This function gets the grade levels for the list box of current grades from the db.
 */
    function getGradeLevels(){
       global $db;
        
        $myQuery = 'select * from gradeLevels';
		
        $statement = $db->prepare($myQuery);
        //$statement->bindValue(':filter',$_SESSION['empSearch']);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        
        return $results;
    }

?>