<?php
/* This function deletes the selected grade. */
function deleteGrade($gradeID){
  global $db;
  $query = "delete from grades where gradeID = :gradeID";
	$statement = $db->prepare($query);
  $statement->bindValue(':gradeID', $gradeID);
  $statement->execute();
  $statement->closeCursor();
}

/* This function gets a list of the grades for the tap record selected in an earlier view.
*/
function getGradeList() {
  global $db;

  $query = 'select * from grades
              join subjects using (subjectID)
              where tapID = :tapID
              order by subjectName, dateGradeChecked desc';
  $statement = $db->prepare($query);
  $statement->bindValue(':tapID', $_SESSION['tapID']);
  $statement->execute();
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}//end getGradeList

/*This function returns the details for the grade with the given ID. */
function getGradeDetails($gradeID){
  global $db;
  $query = "select * from grades where gradeID = :gradeID";
	$statement = $db->prepare($query);
  $statement->bindValue(':gradeID', $gradeID);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

	return $results;
}

/*This function inserts a new Grade.*/
function insertGrade(){
  global $db;
  extract($_REQUEST);

  $query = "insert into grades
              (subjectID, tapID, dateGradeChecked, grade)
              values (:subjectID, :tapID, :dateGradeChecked, :grade)";
  $statement = $db->prepare($query);
  $statement->bindValue(':subjectID', $subjectID);
  $statement->bindValue(':tapID', $_SESSION['tapID']);
  $statement->bindValue(':dateGradeChecked', $dateGradeChecked);
  $statement->bindValue(':grade', $grade);
  $statement->execute();
  $statement->closeCursor();
}

/*This function updates the Grade record based on the contents of the
REQUEST array. */
function updateGrade(){
  global $db;
  extract($_REQUEST);

  $query = "update grades
              set subjectID = :subjectID,
              grade = :grade,
              dateGradeChecked = :dateGradeChecked
              where gradeID = :gradeID";
  $statement = $db->prepare($query);
  $statement->bindValue(':subjectID', $subjectID);
  $statement->bindValue(':grade', $grade);
  $statement->bindValue(':dateGradeChecked', $dateGradeChecked);
  $statement->bindValue(':gradeID', $gradeID);
  $statement->execute();
  $statement->closeCursor();
}

/*
This function validates the form data and populates an errors array with data fields and error messages.
*/
function validateGradeInputs(){
  define('MIN_GRADE', 0.0);			  //Minimum grade
	define('MAX_GRADE', 4.0);		    //Maximum grade

  $errors = [];
  extract($_REQUEST);

	if($javaScriptValidated == 'false'){

    //Validate subject (required)
		if($subjectID == "" || $subjectID == 10)
				$errors['subjectID'] = "Subject is a required field.";

    //Validate grade (required, numeric, in range)
    if ($grade == "")
      $errors['grade'] = "Grade is a required field.";
    else if(!is_numeric($grade))
      $errors['grade'] = "Grade must be a numeric value.";
    else if($grade < MIN_GRADE || $grade > MAX_GRADE)
      $errors['grade'] = "Grade must be between " . MIN_GRADE . " and " . MAX_GRADE . ".";
    else
      $_REQUEST['grade'] = round($grade, 2);

      //Validate date grade checked (required, is a date, not in future, formatted ISO)
  		if($dateGradeChecked == "")
  			$errors['dateGradeChecked'] = "Date grade checked is a required field.";
  		else {
  			$dateGradeChecked = str_replace('-','/', $dateGradeChecked);
  			if(!isDate($dateGradeChecked))
  				$errors['dateGradeChecked'] = "Date grade checked must be a recognizable date.";
  			else {
  				$today = new DateTime();
  				$dateGradeChecked = new DateTime($dateGradeChecked);
  				if($dateGradeChecked > $today)
  					$errors['dateGradeChecked'] = "Date grade checked cannot be in the future.";
  				else
  					$_REQUEST['dateGradeChecked'] = $dateGradeChecked->format('Y-m-d');
  				//end if
  			}//end if
  		}//end if
  }

  return $errors;
}
?>
