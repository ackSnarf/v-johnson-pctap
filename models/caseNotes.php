<?php

// functino for deleting selected case note just the caseNote
function deleteCaseNote($id) {
  global $db;

    $query = 'DELETE from caseNotes
            where caseNoteID= :caseNoteID';

  $statement = $db->prepare($query);
  $statement->bindValue(':caseNoteID', $id);
  $statement->execute();
  $statement->closeCursor();

  return $statement->rowCount();
}

// Retrieves full contents of one case note record
function getCaseNoteDetails($id){
  global $db;

  $query = 'SELECT * from caseNotes
            where caseNoteID= :caseNoteID';

  $statement = $db->prepare($query);
  $statement->bindValue(':caseNoteID', $id);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}


// This funtion obtains the case notes -eventually for the tapID and clientID designated.
function getCaseNoteList(){
  global $db;

  $query = 'SELECT caseNoteID, tapID, noteTypeDescription, targetType, success, caseNoteDate, caseNoteTime, staffFirstName, note
            from caseNotes
            join staff using (staffID)
            join targets using (targetTypeID)
            join noteTypes using (noteTypeID)
            where tapID=:tapID
            order by caseNoteDate desc, caseNoteTime desc';

  $statement = $db->prepare($query);
  $statement->bindValue('tapID', $_SESSION['tapID']);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}

// This function inserts a new caseNote record based on the contents of the request array
function insertCaseNotes(){
  global $db;

  extract($_REQUEST);

  $query = 'INSERT into caseNotes
          (tapID, staffID, noteTypeID, targetTypeID, caseNoteDate, caseNoteTime, note, success)
          values(
              :tapID,
              :staffID,
              :noteTypeID,
              :targetTypeID,
              :caseNoteDate,
              :caseNoteTime,
              :note,
              :success)';

  $statement = $db->prepare($query);
  $statement->bindValue(':tapID', $_SESSION['tapID']);
  $statement->bindValue(':staffID', ($staffID==""?10:$staffID));
  $statement->bindValue(':noteTypeID', ($noteTypeID==""?10:$noteTypeID));
  $statement->bindValue(':targetTypeID', ($targetTypeID==""?10:$targetTypeID));
  $statement->bindValue(':caseNoteDate', $caseNoteDate);
  $statement->bindValue(':caseNoteTime', $caseNoteTime);
  $statement->bindValue(':note', $note);
  $statement->bindValue(':success', isset($success));

  $statement->execute();
  $statement->closeCursor();

  return $statement->rowCount();
}


// This function updates the caseNote record based on the contents of the request array
function updateCaseNotes(){
  global $db;

  extract($_REQUEST);

  $query = 'UPDATE caseNotes set
                staffID=:staffID,
                noteTypeID=:noteTypeID,
                targetTypeID=:targetTypeID,
                caseNoteDate=:caseNoteDate,
                caseNoteTime=:caseNoteTime,
                note=:note,
                success=:success
                where caseNoteID= :caseNoteID';

  $statement = $db->prepare($query);
  $statement->bindValue(':staffID', ($staffID==""?10:$staffID));
  $statement->bindValue(':noteTypeID', ($noteTypeID==""?10:$noteTypeID));
  $statement->bindValue(':targetTypeID', ($targetTypeID==""?10:$targetTypeID));
  $statement->bindValue(':caseNoteDate', $caseNoteDate);
  $statement->bindValue(':caseNoteTime', $caseNoteTime);
  $statement->bindValue(':note', $note);
  $statement->bindValue(':success', isset($success));
  $statement->bindValue(':caseNoteID', $caseNoteID);

  $statement->execute();
  $statement->closeCursor();

  return $statement->rowCount();
}

// php validation.
function validateCaseNoteInputs() {

  $errors = array();
  if($_REQUEST['javascriptValidated']=='false') {
    extract($_REQUEST);

    if ($caseNoteDate=="")
  				$errors['caseNoteDate'] = "You must enter a date.";
  			//end if
    if ($caseNoteTime=="")
          $errors['caseNoteTime'] = "You must enter a time.";
        //end if
    if ($note=="")
          $errors['note'] = "You must enter a note, or press the cancel button instead.";
        //end if

    }//end chain of ifs
    return $errors;
}

?>
