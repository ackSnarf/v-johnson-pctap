<?php
require_once('models/hcAttendance.php');
require_once('models/hcattendStatuses.php');

//$day = date('l');
//$day = date('N');
$day = "Wednesday";


 switch($action) {
     case "hcAttendanceNew":
        //printArray($_REQUEST);
        $errors = "";
        $details = "";
        $status = getHcattendStatuses();
        switch($day) {
            case "Monday":
              $details = hcAttendanceMon();
              include("views/hcAttendance.php");
              break;
            case "Tuesday":
              $details = hcAttendanceTues();
              include("views/hcAttendance.php");
              break;
            case "Wednesday":
              $details = hcAttendanceWed();
              include("views/hcAttendance.php");
              break;
            case "Thursday":
              $details = hcAttendanceThurs();
              include("views/hcAttendance.php");
              break;
            default:
              echo "*********NO HOMEWORK CENTER TODAY*********";
              //$alternativeDays = alternDays();
              //include("views/homeworkCenterList.php");
              break;
        }//end day switch
        break;
    case "hcAttendanceSaveNew":
        switch(true) {
            case isset($_REQUEST['btnSave']):
                //printArray($_REQUEST);
                //$row = insertAttendance();
                //echo $row;
                $errors = validateHcAttendanceInputs();
                if(count($errors)==0) {
                    insertAttendance();
                    header('Location:?action=clientList');
                } else {
                    $details = $_REQUEST;
                    include('views/hcAttendance.php');  //Show form again with error markers
                }//end if
                break;
            case isset($_REQUEST['btnCancel']):
                //Display the list
                header('Location:?action=clientList');
                break;
        }//end button switch
    break;
}//end action switch

?>
