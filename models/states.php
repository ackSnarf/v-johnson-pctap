<?php

/* This function gets the states for the list box of contact states from the db.
 */
    function getContactState(){
       global $db;
        
        $myQuery = 'select distinct contactState from contacts';
		
        $statement = $db->prepare($myQuery);
        //$statement->bindValue(':filter',$_SESSION());
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        
        return $results;
    }
?>