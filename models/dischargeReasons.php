<?php

/* This function gets the discharge reasons from the db. */
function getDischargeReasons(){
   global $db;

    $myQuery = 'SELECT * from dischargeReasons order by dischargeReason';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}

/* This function inserts a new contact type into the dropdown list. */
function insertDischargeReason($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into dischargeReasons (dischargeReason) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the discharge reason item for the loolUp list. */
function updateDischargeReasons($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE dischargeReasons set dischargeReason=:value where dischargeReasonID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
