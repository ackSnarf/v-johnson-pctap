<?php

/* This function retrieves the list of races for the client. */
function getRace(){
   global $db;

    $myQuery = 'SELECT * from races order by race';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}

/* This function inserts a new race type into the dropdown list. */
function insertRace($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into races (race) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the race type on the loolUp list. */
function updateRaces($id, $value){
    global $db;

    extract($_REQUEST);


    $query = 'UPDATE races set race=:value where raceID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

  return ($statement->rowCount() == 1);
}
?>
