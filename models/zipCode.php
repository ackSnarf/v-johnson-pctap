<?php

/* This function gets the zip codes for the list box of zip codes from the db.
 */
    function getContactZip(){
       global $db;
        
        $myQuery = 'select distinct contactZipCode from contacts';
		
        $statement = $db->prepare($myQuery);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        
        return $results;
    }
?>