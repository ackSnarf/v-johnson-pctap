<?php

/* This function gets the schools from the db.  */
function getSchools(){
   global $db;

    $myQuery = 'SELECT * from schools order by schoolName';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}


/* This function inserts a new school name into the dropdown list. */
function insertSchool($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into schools (schoolName) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the school name for the loolUp list. */
function updateSchools($id, $value){
    global $db;

    extract($_REQUEST);
    $query = 'UPDATE schools set schoolName=:value where schoolID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
