<?php

//gets the tap enrollment or courtOrderDetails of this client for edititng purposes
function getCourtOrderDetails($id){
  global $db;


  $query = 'SELECT contactID, clientID, caseNumber, courtOrderID, tapID, gradeLevelID, referralDate, dischargeDate, dischargeReasonID, schoolIDa, schoolIDb
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join taps using (clientID)
            join courtorders using (tapID)
            where courtOrderID=:courtOrderID and contactID=:contactID';

  $statement = $db->prepare($query);
  $statement->bindValue(':courtOrderID', $id);
  $statement->bindValue(':contactID', $_SESSION['contactID']);
  $statement->execute();
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}

//updates a court order for a client
function updateCourtOrder(){
  global $db;

  extract($_REQUEST);

  $query = 'UPDATE taps set
                referralDate=:referralDate,
                gradeLevelID=:gradeLevelID,
                schoolIDa=:schoolIDa,
                schoolIDb=:schoolIDb,
                dischargeReasonID=:dischargeReasonID,
                dischargeDate=:dischargeDate
                where tapID=:tapID';

  $statement = $db->prepare($query);
  $statement->bindValue(':referralDate', $referralDate);
  $statement->bindValue(':gradeLevelID', ($gradeLevelID==""?10:$gradeLevelID));
  $statement->bindValue(':schoolIDa', ($schoolIDa==""?10:$schoolIDa));
  $statement->bindValue(':schoolIDb', ($schoolIDb==""?10:$schoolIDb));
  $statement->bindValue(':dischargeReasonID', ($dischargeReasonID==""?10:$dischargeReasonID));
  $statement->bindValue(':dischargeDate', ($dischargeDate==""?null:$dischargeDate));
  $statement->bindValue(':tapID', $tapID);

  $statement->execute();
  $statement->closeCursor();
  //call second function for if caseNumber changed.
  updateCourtCaseNumber();

  return $statement->rowCount();

}

//to update changes in caseNumber if they are made.
function updateCourtCaseNumber(){
  global $db;

  extract($_REQUEST);

  $query = 'update courtorders set
                caseNumber=:caseNumber
                where tapID=:tapID';

  $statement = $db->prepare($query);
  $statement->bindValue(':caseNumber', $caseNumber);
  $statement->bindValue(':tapID', $tapID);
  $statement->execute();
  $statement->closeCursor();

  return $statement->rowCount();
}




/*This function is called upon return to the clientDetails page with an updated enroll roster.
it's used in the second part of the IF in clientDetails.php
and has been renamed to getCourtOrdersList ...so it's off of clients.
*/
function getCourtOrdersList() {
  global $db;
  extract($_SESSION);

  $query = 'SELECT contactID, contactFirstName, contactLastName, kidtraxID, dob, clientID, referralDate, dischargeDate, caseNumber, courtOrderID, tapID
            from contacts
            join clientcontacts using (contactID)
            join clients using (clientID)
            join taps using (clientID)
            join courtorders using (tapID)
            where contactID=:contactID
            order by referralDate desc';

  $statement = $db->prepare($query);
  $statement->bindValue(':contactID', $contactID);
  $statement->execute();
  $results = $statement->fetchALL(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}



// This function inserts a new court order; daisy-chained to the insertNewCourtOrder function also.
function insertCourtOrder(){
    global $db;
    extract($_REQUEST);


    $query = 'INSERT into taps
            (clientID, dischargeReasonID, gradeLevelID, dischargeDate, schoolIDa, schoolIDb, referralDate)
            values(
                :clientID,
                :dischargeReasonID,
                :gradeLevelID,
                :dischargeDate,
                :schoolIDa,
                :schoolIDb,
                :referralDate)
                ';
    $statement = $db->prepare($query);

    $statement->bindValue(':clientID', $clientID);
    $statement->bindValue(':dischargeReasonID', ($dischargeReasonID==""?10:$dischargeReasonID));
    $statement->bindValue(':gradeLevelID', ($gradeLevelID==""?10:$gradeLevelID));
    $statement->bindValue(':dischargeDate', ($dischargeDate==""?null:$dischargeDate));
    $statement->bindValue(':schoolIDa', ($schoolIDa==""?10:$schoolIDa));
    $statement->bindValue(':schoolIDb', ($schoolIDb==""?10:$schoolIDb));
    $statement->bindValue(':referralDate', $referralDate);
    $statement->execute();
    $statement->closeCursor();

    $tapID = $db->lastInsertId();
    $_REQUEST['tapID'] = $tapID;//into request for function alsoInsert


    alsoInsert();
    return $statement-> rowCount();
  }

//inserts new court order with tapID correct
function alsoInsert(){
  global $db;
  extract($_REQUEST);
  $query =  'INSERT into courtorders
          (tapID, caseNumber)
          values(
              :tapID,
              :caseNumber)';

    $statement = $db->prepare($query);
    $statement->bindValue(':tapID', $tapID);
    $statement->bindValue(':caseNumber', $caseNumber);
    $statement->execute();
    $statement->closeCursor();

    $courtOrderID = $db->lastInsertId(); //get the newly generated courtOrderID too
    $_SESSION['courtOrderID'] = $courtOrderID; //just go straight to the session, don't need it passed thru request

    return $statement->rowCount();
  }

  /*
  This function validates the form data and populates an errors array with data fields and error messages.
  */
  function validateCourtOrderInputs(){

    $errors = [];
    extract($_REQUEST);

  	if($javascriptValidated == 'false'){

      //Validate subject (required)
  		if($caseNumber == "")
  				$errors['caseNumber'] = "Case Number is a required field.";

        //Validate  referral date (required, is a date, not in future, formatted ISO)
    	if($referralDate == "")
    		$errors['referralDate'] = "Referral Date is a required field.";
    		else {
    			$referralDate = str_replace('-','/', $referralDate);
    			if(!isDate($referralDate))
    				$errors['referralDate'] = "This entry is not a recognizable date.";
    			else {
    				$today = new DateTime();
    				$referralDate = new DateTime($referralDate);
    				if($referralDate > $today)
    					$errors['referralDate'] = "Referral Date cannot be in the future.";
    				else
    					$_REQUEST['referralDate'] = $referralDate->format('Y-m-d');
    				//end if
    			}//end if
    		}//end if
    }

    return $errors;
  }




 ?>
