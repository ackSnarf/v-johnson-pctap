<?php
	/* This function checks to see if the current user name and password are authorized.
	*/
	function isAuthorizedUser($userName, $password) {
		global $db;

		$query = "select * from users
						where userName=:userName";
		$statement = $db->prepare($query);
		$statement->bindValue(':userName', $userName);
		$statement->execute();

        if($statement->rowCount()==0) {
            $isAuth =  false;
        } else {
		    $results = $statement->fetch();
            $isAuth = password_verify($password,$results["passwordHash"]);
        }
		$statement->closeCursor();
		return $isAuth;
	}//end isAuthorizedUser

	function getUserTypeID($userName){
		global $db;

		$query = "select userTypeID from users
							where userName = :userName";
		$statement = $db->prepare($query);
		$statement->bindValue(':userName', $userName);
		$statement->execute();
    $result = $statement->fetch();
		$statement->closeCursor();

    return $result[0];
	}

	/* This function generates encrypted passwords for all the users in the database.
		The raw password (before encryption) is the same as the user name.
		FOR SAMPLE DATA CREATION ONLY.
	*/
	function createUserPasswords() {
		global $db;

		$query = "Select userName from users";
		$statement = $db->prepare($query);
		$statement->execute();

		$results = $statement->fetchAll();

		$statement->closeCursor();

		foreach ($results as $user) {
			$query = "Update users
							  Set password=:password
							  Where userName=:userName";
			$statement = $db->prepare($query);
			$statement->bindValue(':password',sha1($user['userName']));
			$statement->bindValue(':userName',$user['userName']);
			$statement->execute();
			$statement->closeCursor();
		}//end foreach
		echo "Done";
	}//end createUserPasswords


?>
