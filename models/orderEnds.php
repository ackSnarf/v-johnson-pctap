<?php
/* This function gets a list of the order ends descriptions.
	*/
function getOrderEndsList() {
  global $db;

  $query = 'select orderEndsID, orderEndsDescription from orderends
                      order by orderEndsDescription';
  $statement = $db->prepare($query);
  $statement->execute();
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  $statement->closeCursor();

  return $results;
}//end getOrderEndsList
?>
