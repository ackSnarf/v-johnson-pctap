<?php

/* This function gets the contact types for the list box of contact types from the db. */
function getContactTypesList(){
   global $db;

    $myQuery = 'SELECT * from contactTypes
                where contactTypeID != 5
			          order by contactTitle';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}//end getContactTypesList

/* This function retrieves the full lookUp list for contact types.  */
function getFullContactTypesList(){
   global $db;

    $myQuery = 'SELECT * from contactTypes order by contactTitle';

    $statement = $db->prepare($myQuery);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    return $results;
}//end getFullContactTypesList

/* This function inserts a new contact type into the dropdown list */
function insertContactType($value){
    global $db;

    extract($_REQUEST);

    $query = 'INSERT into contactTypes (contactTitle) values (:value)';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->execute();
    $statement->closeCursor();

    if ($statement->rowCount() == 1) return $db->lastInsertId();
    return 0;
}

/* This function updates the contact type for the loolUp list. */
function updateContactTypes($id, $value){
    global $db;

    extract($_REQUEST);

    $query = 'UPDATE contactTypes set contactTitle=:value where contactTypeID=:id';

    $statement = $db->prepare($query);
    $statement->bindValue(':value', $value);
    $statement->bindValue(':id', $id);

    $statement->execute();
    $statement->closeCursor();

    return ($statement->rowCount() == 1);
}
?>
