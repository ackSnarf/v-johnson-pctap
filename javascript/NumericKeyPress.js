//Version: 2015-02-18
//Source: http://stackoverflow.com/questions/995183

//This function checks keys intercepted by a keypress press event in a numeric input box, 
//accepting only digits. If the input includes a step with a decimal, one decimal point is allowed.
//If the input's minimum value is less than 0, and minus sign is allowed at the beginning of the input.
//
//To use this script in your page:
//  1. Link to your favorite jQuery library.
//	2. Link to this file in the <head> tag
//     <script src="NumericKeyPress.js"></script>
//                (set the path as appropriate)
//
//  3. In $(document).ready, tie each input:number to this function.
//        $(":input[type='number']").NumericKeyPress();

//Know issues:
// 1. Allows multiple minus signs at beginning of number.  This is because val() returns undefined
//    until the user enters at least one digit. As far as I can tell, there's no way to get the "text"
//    that's been entered in a number input and when you ask for val, it first converts to a number
//    (if possible).


jQuery.fn.NumericKeyPress = 
    function() {
        return this.each(function() {
            $(this).keydown(function (e) {
                // Allow: delete, backspace, tab, escape, enter and . (num keypad and regular)
                var otherChars = [46, 8, 9, 27, 13];
                val = $(this).val();
                var min = ($(this).attr("min")===undefined) ? -1 : $(this).attr("min");
                if(min<0 && val=="") otherChars = otherChars.concat([109, 189, 173]);  //Reg and numeric keypad minus, 173 for firefox 
                var step = ($(this).attr("step")===undefined)?"1":$(this).attr("step")
                if(step.indexOf(".")>-1 && val.indexOf(".")==-1) otherChars = otherChars.concat([110, 190]);  //Reg and numeric keypad decimal
                if ($.inArray(e.keyCode, otherChars) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // If not a number and stop the keypress
                //                  Regular keys                            Numeric Keypad
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });	
    };//end IntegerKeyPress

/*
            $(this).keydown(function (e) {
                // Allow: delete, backspace, tab, escape, enter and . (num keypad and regular)
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

*/