var el = function(id) {	return document.getElementById(id);	}//end el

function newData() {
    el('newDataValue').maxLength = el('dataNc').value; //SpELL

    //set the position of the New Data Item relative to the New Button
    var obj = el("addItem");
    var top = obj.offsetTop;
    // adjust position for each object Button is contained in
    while (obj.offsetParent) {
        top = top + obj.offsetParent.offsetTop;
        if (obj == document.getElementsByTagName('body')[0]) {
            break
        } else {
            obj = obj.offsetParent;
        }
    }
    // Get vertical scroll value
    if (window.pageYOffset !== undefined) { // All browsers, except IE9 and earlier
        var scrollTop = window.pageYOffset;
    } else { // IE9 and earlier
        var scrollTop = document.documentElement.scrollTop;
    }
    // Set position of New Data Item on form
    newItem = el("newItem");
    newItem.style.position = "absolute";
    newItem.style.top = top + "px";
    newItem.style.left = "100px";
    newItem.style.zIndex = "500";

    $('#newItem').removeClass("hidden");
    el('newDataValue').focus();
}

function saveNewItem() {
    value = el('newDataValue').value.trim();
    if (value == "") {
        cancelNewItem();
    } else {
        document.forms['dataNew'].submit();
    }
}

function cancelNewItem() {
    $('#newItem').addClass("hidden");
}


function editData(id, value) {
    el('dataId').value = id;
    el('origDataValue').value = value;
    el('editDataValue').value = value;
    el('editDataValue').maxLength = el('dataNc').value; //SPELL HERRE

    //set the position of the Edit Data Item relative to the Selected Button
    var obj = el("item" + id);
    var top = obj.offsetTop;
    // adjust position for each object Button is contained in
    while (obj.offsetParent) {
        top = top + obj.offsetParent.offsetTop;
        if (obj == document.getElementsByTagName('body')[0]) {
            break
        } else {
            obj = obj.offsetParent;
        }
    }
    // Get vertical scroll value
    if (window.pageYOffset !== undefined) { // All browsers, except IE9 and earlier
        var scrollTop = window.pageYOffset;
    } else { // IE9 and earlier
        var scrollTop = document.documentElement.scrollTop;
    }
    // Set position of Edit Data Item on form
    editItem = el("editItem");
    editItem.style.position = "absolute";
    editItem.style.top = (top-175) + "px";
    editItem.style.left = "100px";
    editItem.style.zIndex = "500";

    $('#editItem').removeClass("hidden");
    el('editDataValue').focus();
}

function saveEditItem() {
    origValue = el('origDataValue').value;
    value = el('editDataValue').value.trim();
    if (value == "" || origValue == value) {
        cancelNewItem();
    } else {
        document.forms['dataEdit'].submit();
    }
}

function cancelEditItem() {
    $('#editItem').addClass("hidden");
}
