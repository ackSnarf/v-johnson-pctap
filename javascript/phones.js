var el = function(id) {	return document.getElementById(id);	}//end el

window.onload = function () {

    //set focus to first input field
    el("phoneNumber").focus();

	jQuery('form').areYouSure();

	//Event Handlers
    el("phoneNumber").onblur = validateContactPhoneNumber; //(10 or 14 DIGITS)

    $(":input[type='number']").NumericKeyPress();

	el("btnCancel").onclick = verifyCancel;
	el("btnDelete").onclick = verifyDelete;
    el("btnReset").onclick = resetForm;
    el("btnSave").onclick = validateForm;

	el("javascriptValidated").value = "true";

}//end onload


/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit (quit when false is found)--
	all conditions are evaluated.
*/
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd


/*	This function clears all the form error markers
	and resets the form fields.
*/
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("phoneNumber").focus();

}//end resetForm


/* This function validates the phone number.
*/
function validateContactPhoneNumber() {
	var ptr = el("phoneNumber");        //Pointer to phone input field
	var err = el("errPhoneNumber");        //Pointer to error marker for phone
	var phone = ptr.value;                 //Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (phone != "") {                     //Phone is optional
		phone = formatPhone(phone, "G");   //Remove special characters

		if (phone.length!=10 && phone.length!=14){
			err.style.visibility = "visible";
			err.title = "Phone numbers must contain 10 or 14 digits.";
		} else {
            ptr.value = phone;
			//ptr.value = formatPhone(phone);	//We'll use dashes (MAY NOT BE ABLE TO USE THIS) (NEED 10 OR 14 DIGIT CAPABILITY)
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateContactPhoneNumber


/*	This function ensures all form fields are valid before submitting the form data.
*/
function validateForm() {
	if ( noShortCircuitAnd(validateContactPhoneNumber()) ) {

		return true;   //Go ahead and submit form
	} else {
		alert("Please correct the designated errors and submit again.");
		return false;   //Cancel the form submit
	}//end if
}//end validateForm


/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#phone').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel


/* This function verifies that the user really wants to delete the current phone number.
*/
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this phone number?\n" + "This action cannot be undone!");

	return yn;			//Confirm returns True (for yes) or No
}//end verifyDelete
