var el = function(id) {	return document.getElementById(id);	}//end $


$(function() {

	jQuery('form').areYouSure();
	el("caseNumber").onblur = validateCaseNumber;
	el("referralDate").onblur = validateReferralDate;
	el("dischargeDate").onblur = validateDischargeDate;
	el("btnSave").onclick = validateForm;
	el("btnReset").onclick = resetForm;
	el("btnCancel").onclick = verifyCancel;

	el("referralDate").focus();
}); //end document ready


/* This function ensures a case number has been added but leaves the
structure of the value completely open. database accepts 6 characters */
function validateCaseNumber(){
	var ptr = el("caseNumber");
	var err = el("errCaseNumber");

    err.style.visibility = "hidden";
    if (ptr.value ==""){
        err.style.visibility = "visible";
        err.title = "Enter a case number.";
    }//end if
    return err.style.visibility =="hidden";
}//end validateCaseNumber


/*	This function ensures the date entered by the user is a valid date.
	If the date is valid, it is formatted in ISO-standard format. */
function validateDischargeDate(){
	//var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");

	var ptr = el("dischargeDate");	 //Pointer to the date input
	var err = el("errDischargeDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if(ptr.value!="") {
			var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");

			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if

        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateDischargeDate

/*	This function ensures the date entered by the user is a valid date.
If the date is valid, it is formatted in ISO-standard format and compared
to today to disallow future dates.
*/
function validateReferralDate(){
    //var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");

	var ptr = el("referralDate");	 //Pointer to the date input
	var err = el("errReferralDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "Enter the Referral Date.";
	} else {
        var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");

			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if

        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateReferralDate

/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for
 		el("caseNumber").focus();

}//end resetForm

/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit
	(quit when false is found)-- all conditions are evaluated. */
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];

	return result;
}//noShortCircuitAnd

/*	This function ensures all form fields are valid before
	allowing submition of the data. */
function validateForm() {
	var allOK = noShortCircuitAnd(
                    validateCaseNumber(),
                    validateReferralDate(),
                    validateDischargeDate());

	if (!allOK){
		alert("Please correct entries with red alerts, check the date again.")
	}else{
		return allOK; //if true submits form.
	}
		return false;   //Cancel the form submit
}//end validateForm


/* This function verifies that the user really wants to leave the form
   without saving their changes. */
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#courtOrderDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel
