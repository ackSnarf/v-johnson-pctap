var el = function(id) {	return document.getElementById(id);	}//end el

window.onload = function () {
    //Set focus to first input field
    el("hcattendDate").focus();
    
    //RESET BUTTON NOT CHECKING IF DIRTY
    
    jQuery('form').areYouSure();

    //Event Handlers
    el("hcattendDate").onblur = validateHcAttendDate;
    
    $(":input[type='number']").NumericKeyPress();
    
    el("btnSave").onclick = validateForm;
	el("btnReset").onclick = resetForm;
	el("btnCancel").onclick = verifyCancel;
}//end onload


/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit
	(quit when false is found)-- all conditions are evaluated. */
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];

	return result;
}//noShortCircuitAnd


/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for
    
    el("hcattendDate").focus();
}//end resetForm


/*	This function ensures all form fields are valid before
	allowing submition of the data. */
function validateForm() {
    if ( noShortCircuitAnd(
          validateHcAttendDate()
       	) ) {
        alert("Your changes have been saved!");
        return true;   //Go ahead and submit form
        
    } else {
        alert("Please correct the designated errors and submit again.");
        return false;   //Cancel the form submit
    }//end if

}//end validateForm


/* This function verifies that the user really wants to leave the form
   without saving their changes. */
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#hcAttendance').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel


function validateHcAttendDate(){
    //var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");
    
	var ptr = el("hcattendDate");	 //Pointer to the date input
	var err = el("errhcattendDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "Enter the Date.";
	} else {
        var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");
			
			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if
            
        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateHcAttendDate
