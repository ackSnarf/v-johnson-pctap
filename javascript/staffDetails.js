var el = function(id) {	return document.getElementById(id);	}//end el

window.onload = function () {

  jQuery('form').areYouSure();

    el("staffFirstName").focus();
    el("staffFirstName").onblur = validateStaffFirstName;
    el("staffLastName").onblur = validateStaffLastName;
    el("staffDob").onblur = validateStaffDob;
    el("staffPhoneNumber").onblur = validateStaffPhoneNumber;
    el("staffEmail").onblur = validateStaffEmail;
    el("staffState").onblur = validateStaffState;
    el("staffZipCode").onblur = validateStaffZipCode;
    el("staffStartDate").onblur = validateStaffStartDate;
    el("staffEndDate").onblur = validateStaffEndDate;

    el("btnCancel").onclick = verifyCancel;
    el("btnReset").onclick = resetForm;
    el("btnSave").onclick = validateForm;


    $(":input[type='number']").NumericKeyPress();

    el("javascriptValidated").value = "true";

}//end onload


/* This function checks to see if the current staff member first name, last name combination is already in the database.
 */
function isDuplicateStaffName() {
    var isDup = false;
    $.ajax({
       async : false,
       method: 'get',
       url   : 'models/duplicateChecks/asyncDupStaffNameCheck.php',
       data  : {
               'staffFirstName': $("#staffFirstName").val(),
               'staffLastName' : $("#staffLastName").val()
               },
       success : function(data) {
            console.log("Success processed. Data is " + data);
            isDup = (data > 0);
       }
    });
    console.log("Function ending");
    return isDup;
}//end isDuplicateStaffName


/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit (quit when false is found)--
	all conditions are evaluated.
*/
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd


/*	This function clears all the form error markers
	and resets the form fields.
*/
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("staffFirstName").focus();

}//end resetForm


/*	This function ensures all form fields are valid before submitting the form data.
*/
function validateForm() {
    if (noShortCircuitAnd(validateStaffFirstName(),validateStaffLastName(),validateStaffDob(),
          validateStaffStartDate(),validateStaffEndDate(),validateStaffState(),validateStaffZipCode(),
          validateStaffPhoneNumber(),validateStaffEmail()) ) {
    return true;   //Go ahead and submit form
    } else {
        alert("Please correct the designated errors and submit again.");
        return false;   //Cancel the form submit
    }//end if

}//end validateForm


/*	This function ensures the date entered by the user is a valid date.
	If the date is valid, it is formatted in ISO-standard format.
*/
function validateStaffDob(){
	//var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");

	var ptr = el("staffDob");	 //Pointer to the date input
	var err = el("errStaffDob"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if(ptr.value!="") {
			var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");

			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if

        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateStaffDob


/* This function validates the staff member's email address.
*/
function validateStaffEmail() {
	var patternForEmail = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+$/;   //Regular express for email format
    var ptr = el("staffEmail");     //Pointer to email input field
	var err = el("errStaffEmail");	//Pointer to error marker for email

    err.style.visibility = "hidden";
	if (ptr.value != "") { 				//Email is optional
		if (!ptr.value.match(patternForEmail)) {
				err.style.visibility = "visible";
				err.title = "This is not a valid email";
		}else{
			ptr.value = ptr.value;
			err.style.visibility = "hidden";
		}//end if
	}//end if

    return err.style.visibility=="hidden";
}//end validate staff email


/*	This function validates the staff member's end date.
*/
function validateStaffEndDate(){
	//var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");

	var ptr = el("staffEndDate");	 //Pointer to the date input
	var err = el("errStaffEndDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if(ptr.value!="") {
			var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");

			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if

        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateStaffEndDate


/*	This function ensures the user has entered a first name.
 *	If a first name is entered it is appropriately capitalized.
 */
function validateStaffFirstName(){
	var ptr = el("staffFirstName"); 	//Pointer to staff first name input field
	var err = el("errStaffFirstName");	//Pointer to error marker for staff first name

	err.style.visibility = "hidden";
	el("errStaffLastName").style.visibility = "hidden";
	//Dup check
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the staff member's first name.";

	} else if( ($('#staffFirstName').val() + $('#staffLastName').val()) !=
               ($('#orgStaffFirstName').val() + $('#orgStaffLastName').val()) &&
               isDuplicateStaffName()){
        var err = el('errStaffLastName');
        err.style.visibility = "visible";
        err.title = $("#staffFirstName").val() + " " + $("#staffLastName").val() + " already exists in the database";

	}//end if
	return err.style.visibility=="hidden";
}//end validateStaffFirstName


/*	This function ensures the user has entered a last name.
 *	If a last name is entered it is appropriately capitalized.
 */
function validateStaffLastName(){
	var ptr = el("staffLastName");      //Pointer to staff last name input field
	var err = el("errStaffLastName");	//Pointer to error marker for staff last name

	err.style.visibility = "hidden";
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the staff member's last name.";

	} else if( ($('#staffFirstName').val() + $('#staffLastName').val()) !=
               ($('#orgStaffFirstName').val() + $('#orgStaffLastName').val()) &&
               isDuplicateStaffName()){
        var err = el('errStaffLastName');
        err.style.visibility = "visible";
        err.title = $("#staffFirstName").val() + " " + $("#staffLastName").val() + " already exists in the database";

	}//end if
	return err.style.visibility=="hidden";
}//end validateStaffLastName


/*
This function validates the phone number.
*/
function validateStaffPhoneNumber() {
	var ptr = el("staffPhoneNumber");      //Pointer to phone input field
	var err = el("errStaffPhoneNumber");   //Pointer to error marker for phone
	var phone = ptr.value;                 //Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (phone != "") {                     //Phone is optional
		phone = formatPhone(phone, "G");   //Remove special characters

		if (phone.length!=10){
			err.style.visibility = "visible";
			err.title = "Phone numbers must contain 10 digits.";
		} else {
            //ptr.value = phone;
			ptr.value = formatPhone(phone, "("); //(XXX)XXX-XXXX
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateStaffPhoneNumber


/*	This function validates the member's start date not blank.
*/
function validateStaffStartDate(){
	//var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
    var today = new Date();
    today = new Date(today).format("isoDate");

	var ptr = el("staffStartDate");	   //Pointer to the date input
	var err = el("errStaffStartDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
    if(ptr.value=="") {
        err.style.visibility = "visible";
        err.title = "You must enter a start date.";
    } else {
        var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");

			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
                ptr.value = enteredDate;
            }//end if
        } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
    }//end if

	return err.style.visibility =="hidden";
}//end validateStaffStartDate


/* This function validates the state.
*/
function validateStaffState() {
	var ptr = el("staffState"); 		//Pointer to state input field
	var err = el("errStaffState");		//Pointer to error marker for state
	var state = ptr.value;				//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (state != "") { //State is optional
		if (state.length!=2){
			err.style.visibility = "visible";
			err.title = "State abbreviation must be two letters.";
		} else {
			ptr.value = state.toUpperCase();//Make both letters uppercase
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateStaffState


/* This function validates the zip code.
*/
function validateStaffZipCode() {
	var ptr = el("staffZipCode"); 		//Pointer to zip code input field
	var err = el("errStaffZipCode");    //Pointer to error marker for zip code
	var zipCode = ptr.value;			//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (zipCode != "") { //Zip code is optional
		if (zipCode.length!=5){
			err.style.visibility = "visible";
			err.title = "Zip code must be 5 digits.";
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateStaffZipCode


/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#frmDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel
