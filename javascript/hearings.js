var el = function(id) {	return document.getElementById(id);	}

const MIN_CS_HOURS = 0.0;              //Minimum CS hours ordered, stayed, or due by review (inclusive)
const MAX_CS_HOURS= 10.0;              //Minimum CS hours ordered, stayed, or due by review (inclusive)
const MIN_FORFEITURE_AMOUNT = 0.0;     //Minimum forfeiture amount (inclusive)
const MAX_FORFEITURE_AMOUNT = 500.0;   //Maximum forfeiture amount (inclusive)
const MIN_YEAR = 2012;                 //Minimum year that order ends
const MAX_YEAR = new Date(new Date().setFullYear(new Date().getFullYear() + 1)); //Maximum year that order ends (one year from current year)

/* This function initializes the form. */
$(function(){
  el("frmDetails").reset(); //Clear previous entries in FF

  setInputMinsMaxes();
  toggleCswFields();
  toggleTapAfterSchoolFields();
  toggleForfeitureFields();

  jQuery('form').areYouSure();

  el("hearingDate").focus();

  el("csHoursOrdered").onkeypress = NumberKeyPress;
  el("csHoursStayed").onkeypress = NumberKeyPress;
  el("csHoursDueByReview").onkeypress = NumberKeyPress;
  el("forfeitureAmount").onkeypress = NumberKeyPress;

  el("hearingDate").onblur = validateHearingDate;
  el("csHoursOrdered").onblur = validateCsHoursOrdered;
  el("csHoursStayed").onblur = validateCsHoursStayed;
  el("csHoursDueByReview").onblur = validateCsHoursDueByReview;
  el("forfeitureAmount").onblur = validateForfeitureAmount;
  el("nextHearingDate").onblur = validateNextHearingDate;

  el("csw").onclick = toggleCswFields;
  el("tapAfterSchool").onclick = toggleTapAfterSchoolFields;
  el("forfeiture").onclick = toggleForfeitureFields;

  el("btnSave").onclick = validateForm;
  el("btnReset").onclick = resetForm;
  el("btnDelete").onclick = verifyDelete;
  el("btnCancel").onclick = verifyCancel;

  el("javaScriptValidated").value = "true";
});//end document ready

/*This function sets the minimum and maximum numeric and date values.*/
function setInputMinsMaxes(){
  el("csHoursOrdered").min = MIN_CS_HOURS;
  el("csHoursOrdered").max = MAX_CS_HOURS;
  el("csHoursStayed").min = MIN_CS_HOURS;
  el("csHoursStayed").max = MAX_CS_HOURS;
  el("csHoursDueByReview").min = MIN_CS_HOURS;
  el("csHoursDueByReview").max = MAX_CS_HOURS;

  el("forfeitureAmount").min = MIN_FORFEITURE_AMOUNT;
  el("forfeitureAmount").max = MAX_FORFEITURE_AMOUNT;

  var today = new Date();
  el("hearingDate").max = today.format("isoDate");
}//end setInputMinsMaxes

/*This function ensures that the user enters a valid date */
function validateHearingDate(){
	var ptr = el("hearingDate");     //Pointer to date input field
	var err = el("errHearingDate");	 //Pointer to date field's error marker

	err.style.visibility = "hidden";
	if (ptr.value == ""){
		err.style.visibility = "visible";
		err.title = "You must enter a hearing date.";
	} else {
		var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dashes and dots with slashes
		if(isDate(enteredDate)){
			enteredDate = new Date(enteredDate).format("isoDate");
			ptr.value = enteredDate;
			if (enteredDate > ptr.max){
				err.style.visibility = "visible";
				err.title = "You must enter a date on or before " + ptr.max + ".";
			} else if ($('#hearingDate').val() != $('#origHearingDate').val() &&
          isDuplicateHearing() ){
        err.style.visibility = "visible";
        err.title = "A hearing for this client already exists on this date.";
      }
		} else {
			err.style.visibility = "visible";
			err.title = "You must enter a recognizable date.";
		}//end if
	}//end if
	return err.style.visibility == "hidden";
}//end validateHearingDate

/* This function ensures that CS Hours Ordered is in the correct range. */
function validateCsHoursOrdered() {
	var ptr = el("csHoursOrdered"); 				 //Pointer to input field
	var err = el("errCsHoursOrdered");			 //Pointer to error marker
	var input = ptr.value;						       //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseInt(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Hours ordered must be between " + FormatNumber(ptr.min, "N0") + " and " +
			   FormatNumber(ptr.max, "N0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateCsHoursOrdered

/* This function ensures that CS Hours Stayed is in the correct range. */
function validateCsHoursStayed() {
	var ptr = el("csHoursStayed"); 				 //Pointer to input field
	var err = el("errCsHoursStayed");			//Pointer to error marker
	var input = ptr.value;						    //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseInt(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Hours stayed must be between " + FormatNumber(ptr.min, "N0") + " and " +
			   FormatNumber(ptr.max, "N0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateCsHoursStayed

/* This function ensures that CS Hours Due by Review is in the correct range. */
function validateCsHoursDueByReview() {
	var ptr = el("csHoursDueByReview");      //Pointer to input field
	var err = el("errCsHoursDueByReview");   //Pointer to error marker
	var input = ptr.value;						       //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseInt(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Hours due by review must be between " + FormatNumber(ptr.min, "N0") + " and " +
			   FormatNumber(ptr.max, "N0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateCsHoursDueByReview

/* This function ensures that the forfeiture amount is in the correct range. */
function validateForfeitureAmount() {
	var ptr = el("forfeitureAmount");      //Pointer to input field
	var err = el("errForfeitureAmount");   //Pointer to error marker
	var input = ptr.value;						     //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseFloat(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Forfeiture amount must be between " + FormatNumber(ptr.min, "C0") + " and " +
			   FormatNumber(ptr.max, "C0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateForfeitureAmount

/*This function ensures that the user enters a valid date */
function validateNextHearingDate(){
	var ptr = el("nextHearingDate");     //Pointer to date input field
	var err = el("errNextHearingDate");		 //Pointer to date field's error marker

	err.style.visibility = "hidden";
	if (ptr.value != ""){
		var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dashes and dots with slashes
		if(isDate(enteredDate)){
			enteredDate = new Date(enteredDate).format("isoDate");
			ptr.value = enteredDate;
		} else {
			err.style.visibility = "visible";
			err.title = "You must enter a recognizable date.";
		}//end if
	}//end if
	return err.style.visibility == "hidden";
}//end validateNextHearingDate

/* This function ensures that all the form entries are complete and correct.
 */
function validateForm() {
	var allOK = noShortCircuitAnd(validateHearingDate(),									
									validateCsHoursOrdered(),
                  validateCsHoursStayed(),
                  validateCsHoursDueByReview(),
                  validateForfeitureAmount(),
                  validateNextHearingDate());
	if (!allOK){
		alert("You must correct the designated errors before submitting.")
	}
	return allOK;
}//end validateForm

/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("hearingDate").focus();
}//end resetForm

/* This function accepts any number of parameters. The function iterates through all the parameters, which
evaluate as true or false, without short-circuiting before completion.
 */
function noShortCircuitAnd() {
	var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd

/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

  if(jQuery('#frmDetails').hasClass('dirty'))
    yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel

/* This function verifies that the user really wants to delete the current employee.
*/
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this record?\n" +
	           "Hearings should normally be archived, not deleted.");

	return yn;			//Confirm returns True (for yes) or No

}//end verifyDelete

/* This function checks that a hearing record does not exist for the same court order on the same date */
function isDuplicateHearing(){
  var isDup = false;
  $.ajax({
    async : false,
    method: 'get',
    url: 'models/duplicateChecks/hearingDupCheck.php',
    data: {
          'hearingDate' : $("#hearingDate").val(),
          'courtOrderID' : $("#courtOrderID").val()
          },
    success: function(data){
      console.log("Success processed. Data is " + data);
      isDup = (data > 0);
    }
  });
  console.log("Function ending");
  return isDup;
}

function toggleCswFields(){
  if ($("#csw").is(':checked')){
     $("#cswFields").show();
  } else {
      $("#cswFields").hide();
      $("#csHoursOrdered").val("");
      $("#csHoursStayed").val("");
      $("#csHoursDueByReview").val("");
  }
}

function toggleTapAfterSchoolFields(){
  if ($("#tapAfterSchool").is(':checked'))
      $("#tapAfterSchoolFields").show();
  else {
      $("#tapAfterSchoolFields").hide();
      $("#hcMonday").prop("checked", false);
      $("#hcTuesday").prop("checked", false);
      $("#hcWednesday").prop("checked", false);
      $("#hcThursday").prop("checked", false);
  }
}

function toggleForfeitureFields(){
  if ($("#forfeiture").is(':checked'))
      $("#forfeitureFields").show();
  else {
    $("#forfeitureFields").hide();
    $("#forfeitureAmount").val("");
    $("#forfeitureStayed").prop("checked", false);
  }
}
