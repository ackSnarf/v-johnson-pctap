/* This function sets the contactID to the contactID of the client
  selected in "Search All Clients" textbox. */
function selectedClient() {
      var val = document.getElementById('txtClients').value;
      var text = $('#clients').find('option[value="' + val + '"]').attr('id');
      if (text == null || text == ""){
        document.getElementById("btnSearch").value = "clientNew";
      } else {
        document.getElementById('contactID').value = text;
      }//end if
}//end selectedClient


/* This function sets the staffID to the staffID of the staff member
  selected in "Search All Staff" textbox. */
function selectedStaff() {
      var val = document.getElementById('txtStaff').value;
      var text = $('#allStaff').find('option[value="' + val + '"]').attr('id');
      document.getElementById('staffID').value = text;
}//end selectedStaff