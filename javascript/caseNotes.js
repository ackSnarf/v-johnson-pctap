var el = function(id) {	return document.getElementById(id);	}//end $


$(function() {

	jQuery('form').areYouSure();
	el("caseNoteTime").onblur = validateTime;
	el("txtNote").onblur = validateNote;

	el("btnSave").onclick = validateForm;
	el("caseNoteTime").onclick = timeNow;
	el("dtpCaseNoteDate").onclick = dayNow;
	el("btnReset").onclick = resetForm;
	el("btnCancel").onclick = verifyCancel;
	el("btnDelete").onclick = verifyDelete;
}); //end document ready


/* Generates the date on a click event. */
function dayNow(i){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if (dd<10){
		dd='0'+dd
	}
	if (mm<10){
		mm='0'+mm
	}
return 	i.value = yyyy+'-'+mm+'-'+dd;
}

/* Generates the time on a click event. */
function timeNow(t){
	var err = el("errCaseNoteTime"); //Pointer to the date error marker
	err.style.visibility =="hidden";

	var d = new Date();
	 	h = (d.getHours()<10?'0':'') + d.getHours();
		m = (d.getMinutes()<10?'0':'') + d.getMinutes();

return	t.value = h + ":" + m;
}

/* This function validates the time entered for a case note. */
function validateTime(){
	var ptr = el("caseNoteTime");	//Pointer to the date input
	var err = el("errCaseNoteTime"); //Pointer to the date error marker

	err.style.visibility = "hidden";
	if (ptr.value ==""){
		err.style.visibility = "visible";
		err.title = "Enter a time.";

	}else{
			return err.style.visibility =="hidden";
	}
}


/* Ensures the note field is not blank */
function validateNote(){
	var ptr = el("txtNote");	//Pointer to the date input
	var err = el("errNote"); //Pointer to the date error marker

	err.style.visibility = "hidden";
	if (ptr.value ==""){
		err.style.visibility = "visible";
		err.title = "Enter a note.";
	}else{
			return err.style.visibility =="hidden";
	}
}


/*	This function ensures the date entered by the user is a valid date.
	If the date is valid, it is formatted in ISO-standard format. */
function validateDate(){
	var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
	var ptr = el("dtpCaseNoteDate");	//Pointer to the date input
	var err = el("errCaseNoteDate"); //Pointer to the date error marker

	err.style.visibility = "hidden";
	if (ptr.value ==""){
		err.style.visibility = "visible";
		err.title = "Enter a date.";
	}else{
		if(isDate(ptr.value)){
			var enteredDate = ptr.value;
			if(!enteredDate.match(pattern)){
				enteredDate = new Date(enteredDate).format("yyyy-mm-dd");
				ptr.value = enteredDate; //reformat user entry
			}//end if
		}else{
			err.style.visibility = "visible";
			err.title = "That's not a recognizable date.";
		}//end if
	}//else OK (date is option)
	return err.style.visibility =="hidden";
}//end date.


/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("dtpCaseNoteDate").focus();
}//end resetForm

/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit
	(quit when false is found)-- all conditions are evaluated.*/
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];

	return result;
}//noShortCircuitAnd

/*	This function ensures all form fields are valid before
	allowing submition of the data. */
function validateForm() {
	var allOK = noShortCircuitAnd(validateDate(), validateTime(), validateNote());
	if (!allOK){
		alert("A Date, Time, and Note are required here.\n\nClick cancel if you wish to return without creating a note.")
	}else{
		return allOK; //if true submits form.
	}
		return false;   //Cancel the form submit
}//end validateForm

/* This function verifies that the user really wants to leave the form
   without saving their changes. */
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#caseNoteDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel

/* This function verifies that the user really wants to delete the note. */
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this note?\n");

	return yn;			//Confirm returns True (for yes) or No
}//end verifyDelete
