var el = function(id) {	return document.getElementById(id);	}//end el

window.onload = function () {
    //RESET BUTTON NOT CHECKING IF DIRTY
	 	jQuery('form').areYouSure();
		//Event Handlers
    //CHANGE NOT IN FUTURE TO SOMETHING MORE RELEVANT? REMOVE?
    //USE REGEX EXPRESSION OR GOOD ENOUGH AS-IS?
    el("dob").onblur = validateDob; //(IS A DATE, NOT IN FUTURE)
    //CHANGE TO MATCH ACTUAL VALIDATION REQUIREMENTS...
    //(JUST GOING WITH 4 DIGITS UNTIL WE KNOW EXACT PATTERN)
    el("kidtraxID").onblur = validateKidtraxID; //(4 DIGITS)
    el("contactFirstName").onblur = validateContactFirstName;  //REQUIRED
  	el("contactLastName").onblur = validateContactLastName;    //REQUIRED
  	el("contactState").onblur = validateContactState;          //(UPPERCASE, 2 LETTERS) **STILL NEEDS TO NOT ACCEPT DIGITS
  	el("contactZipCode").onblur = validateContactZipCode;      //(5 DIGITS)
  	el("contactEmail").onblur = validateContactEmail;          //(PATTERN)
    $(":input[type='number']").NumericKeyPress();
    el("btnReset").onclick = resetForm;
    el("btnSave").onclick = validateForm;
		
		el("javascriptValidated").value = "true";

    //set focus to first input field
    el("contactFirstName").focus();
}//end onload

/* This function checks to see if the current contact first name, last name
 * combination is already in the database.
 */
function isDuplicateContactName() {
    var isDup = false;
    $.ajax({
       async : false,
       method: 'get',
       url   : 'models/duplicateChecks/contactNameDupCheck.php',
       data  : {
               'contactFirstName': $("#contactFirstName").val(),
               'contactLastName' : $("#contactLastName").val()
               },
       success : function(data) {
            console.log("Success processed. Data is " + data);
            isDup = (data > 0);
       }
    });
    console.log("Function ending");
    return isDup;
}//end isDuplicateContactName

/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit (quit when false is found)--
	all conditions are evaluated.
*/
function noShortCircuitAnd() {
  var result=true;
	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous
	return result;
}//noShortCircuitAnd

/*	This function clears all the form error markers
	and resets the form fields.
*/
function resetForm() {
	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for
	el("contactFirstName").focus();
}//end resetForm

/* This function validates the contact's email address.
*/
function validateContactEmail() {
	var patternForEmail = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+$/;   //Regular express for email format
  var ptr = el("contactEmail");       //Pointer to email input field
	var err = el("errContactEmail");	//Pointer to error marker for email

  err.style.visibility = "hidden";
	if (ptr.value != "") { 				//Email is optional
		if (!ptr.value.match(patternForEmail)) {
			err.style.visibility = "visible";
			err.title = "This is not a valid email";
		}else{
			ptr.value = ptr.value;
			err.style.visibility = "hidden";
		}//end if
	}//end if
  return err.style.visibility=="hidden";
}//end validate contact email

/*	This function ensures the user has entered a first name.
 *	If a first name is entered it is appropriately capitalized.
 */
function validateContactFirstName(){
	var ptr = el("contactFirstName"); 	    //Pointer to contact first name input field
	var err = el("errContactFirstName");	//Pointer to error marker for contact first name

	err.style.visibility = "hidden";
	el("errContactLastName").style.visibility = "hidden";
	//Dup check
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the contact's first name.";
	} else if( ($('#contactFirstName').val() + $('#contactLastName').val()) !=
               ($('#orgContactFirstName').val() + $('#orgContactLastName').val()) &&
               isDuplicateContactName()){
        var err = el('errContactLastName');
        err.style.visibility = "visible";
        err.title = $("#contactFirstName").val() + " " + $("#contactLastName").val() + " already exists in the database";
	}//end if
	return err.style.visibility=="hidden";
}//end validateContactFirstName

/*	This function ensures the user has entered a last name.
 *	If a last name is entered it is appropriately capitalized.
 */
function validateContactLastName(){
	var ptr = el("contactLastName");        //Pointer to contact last name input field
	var err = el("errContactLastName");		//Pointer to error marker for contact last name

	err.style.visibility = "hidden";
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the contact's last name.";
	} else if( ($('#contactFirstName').val() + $('#contactLastName').val()) !=
               ($('#orgContactFirstName').val() + $('#orgContactLastName').val()) &&
               isDuplicateContactName()){
      var err = el('errContactLastName');
      err.style.visibility = "visible";
      err.title = $("#contactFirstName").val() + " " + $("#contactLastName").val() + " already exists in the database";
	}//end if
	return err.style.visibility=="hidden";
}//end validateContactLastName

/* This function validates the state.
*/
function validateContactState() {
	var ptr = el("contactState"); 		    //Pointer to state input field
	var err = el("errContactState");		//Pointer to error marker for state
	var state = ptr.value;					//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (state != "") { //State is optional
		if (state.length!=2){
			err.style.visibility = "visible";
			err.title = "State abbreviation must be two letters.";
		} else {
			ptr.value = state.toUpperCase();//Make both letters uppercase
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateContactState

/* This function validates the zip code.
*/
function validateContactZipCode() {
	var ptr = el("contactZipCode"); 		//Pointer to zip code input field
	var err = el("errContactZipCode");		//Pointer to error marker for zip code
	var zipCode = ptr.value;				//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (zipCode != "") { //Zip code is optional
		if (zipCode.length!=5){
			err.style.visibility = "visible";
			err.title = "Zip code must be 5 digits.";
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateContactZipCode

/*	This function ensures the date entered by the user is a valid date.
	If the date is valid, it is formatted in ISO-standard format.
*/
function validateDob(){
	//var pattern = /^\d{4}-\d{2}-\d{2}$/; //Regex for date
  var today = new Date();
  today = new Date(today).format("isoDate");
	var ptr = el("dob");	//Pointer to the date input
	var err = el("errDob"); //Pointer to the date error marker

	err.style.visibility = "hidden";
  if(ptr.value!="") {
		var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dash and dot with slash
		if(isDate(enteredDate)) {
			//Format for ISO
			enteredDate = new Date(enteredDate).format("isoDate");
			//Now we can compare dates
			if(enteredDate > today) {
				err.style.visibility = "visible";
				err.title = "Date cannot be in the future.";
			} else {
	      ptr.value = enteredDate;
	    }//end if
	  } else {
			err.style.visibility = "visible";
			err.title = "Not a recognizable date.";
		}//end if
  }//end if
	return err.style.visibility =="hidden";
}//end validateDob

/*	This function ensures all form fields are valid before submitting the form data.
*/
function validateForm() {
	if ( noShortCircuitAnd(
		validateContactFirstName(),
		validateContactLastName(),
    validateKidtraxID(),
    validateDob(),
		validateContactState(),
		validateContactZipCode(),
    validateContactEmail()
    ) ) {
			alert("Your changes have been saved!");
      //set focus to first input field
      el("contactFirstName").focus();
      return true;   //Go ahead and submit form
  } else {
		alert("Please correct the designated errors and submit again.");
    return false;   //Cancel the form submit
  }//end if
}//end validateForm

/* This function validates the kidtraxID.
*/
function validateKidtraxID() {
	var ptr = el("kidtraxID"); 		//Pointer to kidtrax ID input field
	var err = el("errKidtraxID");	//Pointer to error marker for kidtrax ID
	var kidtrax = ptr.value;		//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (kidtrax != "") { //KidtraxID is optional
		if (kidtrax.length!=4){
			err.style.visibility = "visible";
			err.title = "Kidtrax ID must be 4 digits.";
		}//end if
	}//endif
	return err.style.visibility == "hidden";
}//end validateKidtraxID
