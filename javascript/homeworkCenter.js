var el = function(id) {	return document.getElementById(id);	}

const MIN_HCATTEND_POINTS = 0;    //Minimum attendance points (inclusive)
const MAX_HCATTEND_POINTS = 5;    //Maximum attendance points (inclusive)
const MIN_HOMEWORK_POINTS = 0;    //Minimum homework points (inclusive)
const MAX_HOMEWORK_POINTS = 2;    //Maximum homework points (inclusive)

/* This function initializes the form. */
$(function(){
  el("frmDetails").reset(); //Clear previous entries in FF

  setInputMinsMaxes();

  jQuery('form').areYouSure();

  el("hcattendDate").focus();

  el("hcattendPoints").onkeypress = NumberKeyPress;
  el("homeworkPoints").onkeypress = NumberKeyPress;

  el("hcattendDate").onblur = validateHcattendDate;
  el("hcattendStatusID").onblur = validateHcattendStatusID;
  el("hcattendPoints").onblur = validateHcattendPoints;
  el("homeworkPoints").onblur = validateHomeworkPoints;

  el("btnSave").onclick = validateForm;
  el("btnReset").onclick = resetForm;
  el("btnDelete").onclick = verifyDelete;
  el("btnCancel").onclick = verifyCancel;

  el("javaScriptValidated").value = "true";
});//end document ready

/*This function sets the minimum and maximum numeric and date values.*/
function setInputMinsMaxes(){
  el("hcattendPoints").min = MIN_HCATTEND_POINTS;
  el("hcattendPoints").max = MAX_HCATTEND_POINTS;
  el("homeworkPoints").min = MIN_HOMEWORK_POINTS;
  el("homeworkPoints").max = MAX_HOMEWORK_POINTS;

  var today = new Date();
  el("hcattendDate").max = today.format("isoDate");
}//end setInputMinsMaxes

/*This function ensures that the user enters a valid date
*/
function validateHcattendDate(){
	var ptr = el("hcattendDate");    	  //Pointer to date input field
	var err = el("errHcattendDate");		//Pointer to date field's error marker

	err.style.visibility = "hidden";
	if (ptr.value == ""){
		err.style.visibility = "visible";
		err.title = "You must enter a date.";
	} else {
		var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dashes and dots with slashes
		if(isDate(enteredDate)){
			enteredDate = new Date(enteredDate).format("isoDate");
			ptr.value = enteredDate;
			if (enteredDate > ptr.max){
				err.style.visibility = "visible";
				err.title = "You must enter a date on or before " + ptr.max + ".";
			} else if ( $('#hcattendDate').val() != $('#origHcattendDate').val()
          && isDuplicateAttendance() ){
        err.style.visibility = "visible";
        err.title = "A homework center attendance record for this client already exists on this date.";
      }
		} else {
			err.style.visibility = "visible";
			err.title = "You must enter a recognizable date.";
		}//end if
	}//end if
	return err.style.visibility == "hidden";
}//end validateHcattendDate

/* This function validates the attendance status ID. */
function validateHcattendStatusID() {
	var ptr = el("hcattendStatusID"); 		 //Pointer to attendance status ID input field
	var err = el("errHcattendStatusID");   //Pointer to error marker for attendance status ID

	err.style.visibility = "hidden";
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must select a status from the list.";
	} //end if
  return err.style.visibility == "hidden";
}//end validateHcattendStatusID

/* This function ensures that attendance points are in the correct range. */
function validateHcattendPoints() {
	var ptr = el("hcattendPoints");          //Pointer to input field
	var err = el("errHcattendPoints");       //Pointer to error marker
	var input = ptr.value;						       //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseInt(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Attendance points must be between " + FormatNumber(ptr.min, "N0") + " and " +
			   FormatNumber(ptr.max, "N0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateHcattendPoints

/* This function ensures that homework points are in the correct range. */
function validateHomeworkPoints() {
	var ptr = el("homeworkPoints");          //Pointer to input field
	var err = el("errHomeworkPoints");       //Pointer to error marker
	var input = ptr.value;						       //Contents of the text box

	err.style.visibility = "hidden";
	if(input != "") {
		input = parseInt(input);
		if(input<ptr.min || input>ptr.max) {
			err.style.visibility = "visible";
			err.title = "Homework points must be between " + FormatNumber(ptr.min, "N0") + " and " +
			   FormatNumber(ptr.max, "N0") + ".";
		} //end if
	}//end if

	return err.style.visibility == "hidden";
}//end validateHomeworkPoints

/* This function ensures that all the form entries are complete and correct.
 */
function validateForm() {
	var allOK = noShortCircuitAnd(validateHcattendDate(),
									validateHcattendStatusID(),
									validateHcattendPoints(),
                  validateHomeworkPoints());
	if (!allOK){
		alert("You must correct the designated errors before submitting.")
	}
	return allOK;
}//end validateForm

/* This function accepts any number of parameters. The function iterates through all the parameters, which
evaluate as true or false, without short-circuiting before completion.
 */
function noShortCircuitAnd() {
	var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd

/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("hcattendDate").focus();
}//end resetForm

/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#frmDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel

/* This function verifies that the user really wants to delete the current employee.
*/
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this record?\n" +
	           "Attendance records should normally be archived, not deleted.");

	return yn;			//Confirm returns True (for yes) or No

}//end verifyDelete

function isDuplicateAttendance(){
  var isDup = false;
  $.ajax({
    async : false,
    method: 'get',
    url: 'models/duplicateChecks/homeworkCenterDupCheck.php',
    data: {
          'tapID': $("#tapID").val(),
          'hcattendDate' : $("#hcattendDate").val()
          },
    success: function(data){
      console.log("Success processed. Data is " + data);
      isDup = (data > 0);
    }
  });
  console.log("Function ending");
  return isDup;
}
