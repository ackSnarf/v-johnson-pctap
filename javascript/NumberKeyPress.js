//Version: 2014-11-22

//This JavaScript intercepts keystrokes in a textbox or
//text area, and ignores all characters EXCEPT 0-9 and period.
//A decimal point is only allowed if one hasn't already been entered.
//Commas are NOT accepted.  To allow commas, use DecimalKeyPress.js.

//This function is designed to be used with input type="number".

//To use this script in your page:
//	1. Include a reference to this file in the <head> tag
//     <script type="text/javascript" src="NumberKeyPress.js"></script>
//                                     (set the path as appropriate)
//
//  2. Add an onkeypress event handler for the input of your choice. 

//Know issues:
//   In FF arrow keys become disabled
//		Arrow keys return keycode 37-40 which are the same as % ' & (   (weird)


function NumberKeyPress(e)
{

    e = e || window.event; //FF uses parameter, this is for IE

    var key = e.keyCode || e.which;

    var el = e.srcElement? e.srcElement : e.target;  //Element that caused this event
    
   if(el.type == "number") {
    
        var allowDecimal = el.step.indexOf(".")>=0; //See if field step value includes decimal
        switch (key) {
            case 8: //backspace (for FF)  and Tab (for FF)
            case 9: return true; break;

            case 46: //Decimal point
                     return allowDecimal && el.value.indexOf(".")==-1;   //Return whether decimal already there.
                     break;

            case 48: //digits
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56: 
            case 57: return true;
                     break;

            default: return false;  //All other keys are not accepted/ignored
        }//end switch
   } else {
       alert("NumberKeyPress should only be used for <input type='number'>\n\n" +
             "Consider IntegerKeyPress, DecimalKeyPress or CurrencyKeyPress.");
       return true;
   }//end if

}//end NumberKeyPress
