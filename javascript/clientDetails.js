var $ = function(id) {	return document.getElementById(id);	}//end $

window.onload = function () {


jQuery('frm').areYouSure();

//Event Handlers
    $("contactFirstName").onblur = validateFirstName;
	$("contactLastName").onblur = validateLastName;
	$("caseNum1").onblur = validateCaseNumber;
    $("referralDate").onblur = validateReferralDate;

    $("btnSave").onclick = validateForm;
	$("btnCancel").onclick = verifyCancel;
	
}

/*	This function validates the clients first name.
*/

function validateFirstName(){
	var ptr = $("contactFirstName"); 			//Pointer to item name input field
	var err = $("errContactFirstName");			//Pointer to error marker for item name

	err.style.visibility = "hidden";
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the clients first name.";
	} else if( ($('#contactFirstName').val() + $('#contactLastName').val()) !=
               ($('#orgFirstName').val() + $('#orgLastName').val()) &&
               isDuplicateName()){
        var err = $('errStaffLastName');
        err.style.visibility = "visible";
        err.title = $("#staffirstName").val() + " " + $("#lastName").val() + " already exists in the database";
    }
	return err.style.visibility=="hidden";
}//end validateFirstName

function validateLastName(){
	var ptr = $("contactLastName"); 			//Pointer to item name input field
	var err = $("errContactLastName");			//Pointer to error marker for item name

	err.style.visibility = "hidden";
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the clients last name.";
	} else {
		ptr.value = ptr.value.toTitleCase();
	}//end if
	return err.style.visibility=="hidden";
}//end validateLastName

function validateCaseNumber(){
    var ptr = $("caseNum1"); 			//Pointer to item name input field
    var err = $("errCaseNum1");			//Pointer to error marker for item name

    err.style.visibility = "hidden";
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the case number.";
    }else{
        ptr.value = ptr.value;
    }
    return err.style.visibility=="hidden";
}//end validateCaseNumber

function validateReferralDate(){
    var today = new Date;
    today = new Date(today);
    var ptr = $("referralDate");			//Pointer to the date input
    var err = $("errRefferalDate");			//Pointer to the date error marker

		err.style.visibility = "hidden";
		if (ptr.value == "") {
			err.style.visibility = "visible";
			err.title = "You must enter a referral date.";
        }else if (ptr.value > today) {
			err.style.visibility = "visible";
			err.title = "Date for meal can not be in the future.";
			} else
               err.style.visibility = "hidden";

      }//end validateReferralDate

function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd

/*	This function ensures all form fields are valid before submitting the form data.
*/
function validateForm() {
	if ( noShortCircuitAnd(
		validateStaffFirstName(),
		validateStaffLastName(),
        //validateStaffDob(),
		validateStaffAddress(),
		validateStaffCity() ,
		validateStaffState(),
		validateStaffZipCode()
        //validateStaffStartDate()
       	) ) {

		return true;   //Go ahead and submit form
	} else {
		alert("Please correct the designated errors and submit again.");
		return false;   //Cancel the form submit
	}//end if
}//end validateForm

/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#frmDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel
