var el = function(id) {	return document.getElementById(id);	}

const MIN_GRADE = 0.00;    //Minimum grade value (inclusive)
const MAX_GRADE = 4.00;    //Maximum grade value (inclusive)

/* This function initializes the form. */
$(function(){
  el("frmDetails").reset(); //Clear previous entries in FF

  setInputMinsMaxes();

  jQuery('form').areYouSure();

  el("subjectID").focus();

  el("grade").onkeypress = NumberKeyPress;

  el("subjectID").onblur = validateSubjectID;
  el("grade").onblur = validateGrade;
  el("dateGradeChecked").onblur = validateDateGradeChecked;

  el("btnSave").onclick = validateForm;
  el("btnReset").onclick = resetForm;
  el("btnDelete").onclick = verifyDelete;
  el("btnCancel").onclick = verifyCancel;

  el("javaScriptValidated").value = "true";
});//end document ready

/*This function sets the minimum and maximum numeric and date values.*/
function setInputMinsMaxes(){
  el("grade").min = MIN_GRADE;
  el("grade").max = MAX_GRADE;

  var today = new Date();
  el("dateGradeChecked").max = today.format("isoDate");
}//end setInputMinsMaxes

/*This function ensures that the user enters a valid date
*/
function validateDateGradeChecked(){
	var ptr = el("dateGradeChecked");    	//Pointer to date input field
	var err = el("errDateGradeChecked");		//Pointer to date field's error marker

	err.style.visibility = "hidden";
	if (ptr.value == ""){
		err.style.visibility = "visible";
		err.title = "You must enter a date.";
	} else {
		var enteredDate = ptr.value.replace(/[-\.]/g, "/"); //Replace dashes and dots with slashes
		if(isDate(enteredDate)){
			enteredDate = new Date(enteredDate).format("isoDate");
			ptr.value = enteredDate;
			if (enteredDate > ptr.max){
				err.style.visibility = "visible";
				err.title = "You must enter a date on or before " + ptr.max + ".";
			} else if ( ($('#dateGradeChecked').val() + $('#subjectID').val()) !=
          ($('#origDateGradeChecked').val() + $('#origSubjectID').val()) &&
          isDuplicateGrade() ){
        err.style.visibility = "visible";
        err.title = "A grade for this client already exists for this subject on this date.";
      }
		} else {
			err.style.visibility = "visible";
			err.title = "You must enter a recognizable date.";
		}//end if
	}//end if
	return err.style.visibility == "hidden";
}//end validateDateGradeChecked


/*	This function ensures that the user has entered a required valid grade.
	NumericKeyPress ensures that only digits and a decimal are entered. */
function validateGrade() {
	var ptr = el("grade"); 				//Pointer to grade input field
	var err = el("errGrade");			//Pointer to error marker for grade
	var input = ptr.value;				//Contents of the grade text box

	err.style.visibility = "hidden";
	if(input == "") {
    err.style.visibility = "visible";
		err.title = "You must enter a grade.";
  } else {
    input = parseFloat(input);
    if(input < ptr.min || input > ptr.max) {
  			err.style.visibility = "visible";
  			err.title = "Grade must be between " + FormatNumber(ptr.min, "N") + " and " +
  			   FormatNumber(ptr.max, "N") + ".";
  	}//end if
  }
  return err.style.visibility == "hidden";
}//end validateGrade

/* This function validates the subject ID number. */
function validateSubjectID() {
	var ptr = el("subjectID"); 		 //Pointer to subject ID input field
	var err = el("errSubjectID");  //Pointer to error marker for subject ID

	err.style.visibility = "hidden";
  el("errDateGradeChecked").style.visibility = "hidden" //Duplicate check
	if (ptr.value == "" || ptr.value == "10") {
		err.style.visibility = "visible";
		err.title = "You must select a subject from the list.";
	} else if ( ($('#dateGradeChecked').val() + $('#subjectID').val()) !=
      ($('#origDateGradeChecked').val() + $('#origSubjectID').val()) &&
      isDuplicateGrade() ){
    var err = el('errDateGradeChecked');
    err.style.visibility = "visible";
    err.title = "A grade for this client already exists for this subject on this date.";
  }//end if
  return err.style.visibility == "hidden";
}//end validateSubjectID

/* This function ensures that all the form entries are complete and correct.
 */
function validateForm() {
	var allOK = noShortCircuitAnd(
                  validateSubjectID(),
                  validateDateGradeChecked(),
									validateGrade());
	if (!allOK){
		alert("You must correct the designated errors before submitting.")
	}
	return allOK;
}//end validateForm

/* This function accepts any number of parameters. The function iterates through all the parameters, which
evaluate as true or false, without short-circuiting before completion.
 */
function noShortCircuitAnd() {
	var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd

/*	This function clears all the form error markers
	and resets the form fields. */
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("subjectID").focus();
}//end resetForm

/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#frmDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel

/* This function verifies that the user really wants to delete the current employee.
*/
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this record?\n" +
	           "Grades should normally be archived, not deleted.");

	return yn;			//Confirm returns True (for yes) or No

}//end verifyDelete

function isDuplicateGrade(){
  var isDup = false;
  $.ajax({
    async : false,
    method: 'get',
    url: 'models/duplicateChecks/gradeDupCheck.php',
    data: {
          'subjectID': $("#subjectID").val(),
          'dateGradeChecked' : $("#dateGradeChecked").val()
          },
    success: function(data){
      console.log("Success processed. Data is " + data);
      isDup = (data > 0);
    }
  });
  console.log("Function ending");
  return isDup;
}
