var el = function(id) {	return document.getElementById(id);	}//end el

window.onload = function () {

    //set focus to first input field
    el("txtContactFirstName").focus();

	jQuery('form').areYouSure();

	//Event Handlers
    el("txtContactFirstName").onblur = validateContactFirstName;    //REQUIRED
	el("txtContactLastName").onblur = validateContactLastName;      //REQUIRED
	el("txtContactState").onblur = validateContactState; //(UPPERCASE, 2 LETTERS) **STILL NEEDS TO NOT ACCEPT DIGITS
	el("numContactZipCode").onblur = validateContactZipCode; //(5 DIGITS)
	el("txtContactEmail").onblur = validateContactEmail; //(PATTERN)

    $(":input[type='number']").NumericKeyPress();

	el("btnCancel").onclick = verifyCancel;
	el("btnDelete").onclick = verifyDelete;
    el("btnReset").onclick = resetForm;
    el("btnSave").onclick = validateForm;

	el("javascriptValidated").value = "true";

}//end onload


/* This function checks to see if the current contact first name, last name
 * combination is already in the database.
 */
function isDuplicateContactName() {
    var isDup = false;
    $.ajax({
       async : false,
       method: 'get',
       url   : 'models/duplicateChecks/contactNameDupCheck.php',
       data  : {
               'contactFirstName': $("#txtContactFirstName").val(),
               'contactLastName' : $("#txtContactLastName").val()
               },
       success : function(data) {
            console.log("Success processed. Data is " + data);
            isDup = (data > 0);
       }
    });
    console.log("Function ending");
    return isDup;
}//end isDuplicateContactName


/*	This function accepts any number of parameters. Most are designated as conditions
	but they can also be flags. All parameters should evaluate to true/false. The
	function returns True if all the conditions are true, but does not short-circuit (quit when false is found)--
	all conditions are evaluated.
*/
function noShortCircuitAnd() {
  var result=true;

	for (var i=0; i<arguments.length; i++)
		result = result && arguments[i];    //go through each argument and AND it with the previous

	return result;
}//noShortCircuitAnd


/*	This function clears all the form error markers
	and resets the form fields.
*/
function resetForm() {

	var images = document.getElementsByTagName("img");
	for (var i=0; i<images.length; i++) {
		if(images[i].id.indexOf("err")==0)
			images[i].style.visibility = "hidden";
	}//end for

	el("txtContactFirstName").focus();

}//end resetForm


/* This function validates the contact's email address.
*/
function validateContactEmail() {
	var patternForEmail = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+$/;   //Regular express for email format
    var ptr = el("txtContactEmail"); 	//Pointer to email input field
	var err = el("errContactEmail");	//Pointer to error marker for email

    err.style.visibility = "hidden";
	if (ptr.value != "") { 				//Email is optional
		if (!ptr.value.match(patternForEmail)) {
				err.style.visibility = "visible";
				err.title = "This is not a valid email";
		}else{
			ptr.value = ptr.value;
			err.style.visibility = "hidden";
		}//end if
	}//end if

    return err.style.visibility=="hidden";
}//end validate contact email


/*	This function ensures the user has entered a first name.
 *	If a first name is entered it is appropriately capitalized.
 */
function validateContactFirstName(){
	var ptr = el("txtContactFirstName"); 	//Pointer to contact first name input field
	var err = el("errContactFirstName");	//Pointer to error marker for contact first name

	err.style.visibility = "hidden";
	el("errContactLastName").style.visibility = "hidden";
	//Dup check
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the contact's first name.";

	} else if( ($('#txtContactFirstName').val() + $('#txtContactLastName').val()) !=
               ($('#orgContactFirstName').val() + $('#orgContactLastName').val()) &&
               isDuplicateContactName()){
        var err = el('errContactLastName');
        err.style.visibility = "visible";
        err.title = $("#txtContactFirstName").val() + " " + $("#txtContactLastName").val() + " already exists in the database";

	}//end if
	return err.style.visibility=="hidden";
}//end validateContactFirstName


/*	This function ensures the user has entered a last name.
 *	If a last name is entered it is appropriately capitalized.
 */
function validateContactLastName(){
	var ptr = el("txtContactLastName"); 	//Pointer to contact last name input field
	var err = el("errContactLastName");		//Pointer to error marker for contact last name

	err.style.visibility = "hidden";
	ptr.value = ptr.value.toTitleCase();
	if (ptr.value == "") {
		err.style.visibility = "visible";
		err.title = "You must enter the contact's last name.";

	} else if( ($('#txtContactFirstName').val() + $('#txtContactLastName').val()) !=
               ($('#orgContactFirstName').val() + $('#orgContactLastName').val()) &&
               isDuplicateContactName()){
        var err = el('errContactLastName');
        err.style.visibility = "visible";
        err.title = $("#txtContactFirstName").val() + " " + $("#txtContactLastName").val() + " already exists in the database";

	}//end if
	return err.style.visibility=="hidden";
}//end validateContactLastName


/* This function validates the state.
*/
function validateContactState() {
	var ptr = el("txtContactState"); 		//Pointer to state input field
	var err = el("errContactState");		//Pointer to error marker for state
	var state = ptr.value;					//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (state != "") { //State is optional
		if (state.length!=2){
			err.style.visibility = "visible";
			err.title = "State abbreviation must be two letters.";
		} else {
			ptr.value = state.toUpperCase();//Make both letters uppercase
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateContactState


/* This function validates the zip code.
*/
function validateContactZipCode() {
	var ptr = el("numContactZipCode"); 		//Pointer to zip code input field
	var err = el("errContactZipCode");		//Pointer to error marker for zip code
	var zipCode = ptr.value;				//Transfer to variable to simplify code

	err.style.visibility = "hidden";
	if (zipCode != "") { //Zip code is optional
		if (zipCode.length!=5){
			err.style.visibility = "visible";
			err.title = "Zip code must be 5 digits.";
		}//end if
	}//endif

	return err.style.visibility == "hidden";
}//end validateContactZipCode


/*	This function ensures all form fields are valid before submitting the form data.
*/
function validateForm() {
    if ( noShortCircuitAnd(
		  validateContactFirstName(),
		  validateContactLastName(),
		  validateContactState(),
		  validateContactZipCode(),
          validateContactEmail()
       	) ) {

        return true;   //Go ahead and submit form
    } else {
        alert("Please correct the designated errors and submit again.");
        return false;   //Cancel the form submit
    }//end if

}//end validateForm


/* This function verifies that the user really wants to leave the form
   without saving their changes.
*/
function verifyCancel() {
	var yn;				//User's response to confirmation dialog

    if(jQuery('#contactDetails').hasClass('dirty'))
        yn=confirm("You have unsaved changes!\n\nAre you sure you want to leave this page?");
	return yn;			//Confirm returns True (for yes) or No
}//end verifyCancel


/* This function verifies that the user really wants to delete the current contact.
*/
function verifyDelete() {
	var yn;				//User's response to confirmation dialog

	yn=confirm("Are you sure you want to delete this record?\n" +
				"All phone number records for this contact will also be deleted!");

	return yn;			//Confirm returns True (for yes) or No
}//end verifyDelete
