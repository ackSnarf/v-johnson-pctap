# README #
This is my Capstone Project. It is a one-semester project that had three team members (myself and two others).
We found a client, designed the app, learned PHP in our concurrently running Web Data Management class, and generated this app. 
It's designed using the MVC Model.
My contributions on this project span the full-stack. 
I assisting with the design of the database and website. Creation of the 
mock-dataset, and parts of the back-end as well as the majority of the front-end design and GUI work and site 
navigation. 
 
Technologies used within include: PHP, Javascript, jQuery, HTML5, CSS3, Bootstrap, and W3.CSS with a mySQL database. 

### What is this repository for? ###

* Boys and Girls Club of Portage County Truancy Abatement Program
* 0.9
* This project is continually updated but the version here remains static. 

### How do I get set up? ###

This site was created using XAMMP and Apache web server on 
my local host. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact