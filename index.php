<?php
    require ('models/connectTap.php');
    require ('models/utilities.php');
    require ('models/tapConstants.php');

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "clientList";

if(!isset($_SESSION['authorizedUser'])) $action = 'userLogin';

    switch(true) {
        case strpos($action,"caseNote")===0:
            include("models/caseNoteCases.php");
            break;
        case strpos($action,"client")===0:
            include("models/clientCases.php");
            break;
        case strpos($action, 'contact')===0:
            include('models/contactsCases.php');
            break;
        case strpos($action,"court")===0:
            include("models/courtOrderCases.php");
            break;
        case strpos($action, "grade")===0:
            include("models/gradeCases.php");
            break;
        case strpos($action, "hcAttendance")===0:
            include("models/hcAttendanceCases.php");
            break;
        case strpos($action, "hearing")===0:
            include("models/hearingCases.php");
            break;
        case strpos($action, "homeworkCenter")===0:
            include("models/homeworkCenterCases.php");
            break;
        case strpos($action, "lookUp") === 0:
            include("models/lookUpCases.php");
            break;
        case strpos($action, 'phone')===0:
            include('models/phonesCases.php');
            break;
        case strpos($action,"report")===0:
            include("models/reportCases.php");
            break;
        case strpos($action,"staff")===0:
            include("models/staffCases.php");
            break;
        case strpos($action,"tap")===0:
            include("models/tapEnrollmentCases.php");
            break;
        case strpos($action, "user")===0:
            include("models/userCases.php");
            break;
        default:
            echo ("action $action is not recognized.");
            break;
     }//end switch
?>
