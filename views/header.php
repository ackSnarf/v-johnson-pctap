<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Karen Bezella-Bond, Valerie Johnson, Jes Treder">
	<meta name="description" content="Truancy Abatement Program">
	<meta name="keywords" content="truancy, abatement, Portage County, Boys and Girls Club">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Truancy Abatement Program </title>

		<link href="bootstrap/css/spacelabs.css" rel="stylesheet" type="text/css">
		<link href="bootstrap/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="bootstrap/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href="css/w3.css" rel="stylesheet" type="text/css">
    <link href="css/header.css" rel="stylesheet" type="text/css">
    <link href="css/footer.css" rel="stylesheet" type="text/css">
    <link href="css/main.css" rel="stylesheet" type="text/css">

    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script type="text/javascript" src="javascript/footer.js"></script>

</head>

<body>
    <header class="headerStyle">
        <ul>
            <li style="float:left">&nbsp;&nbsp;<img src="images/bgcLogo.png"></li>
            <li><a href="?action=userLogout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a></li>
						<li><a <?php echo ($_SESSION['userTypeID'] == 1) ? 'href="?action=staffList"' : 'class="hidden"' ?>><i class="fa fa-heart" aria-hidden="true"></i> Staff</a></li>
						<li><a <?php echo ($_SESSION['userTypeID'] == 1) ? 'href="?action=lookUpSelect"' : 'class="hidden"' ?>><i class="fa fa-cog" aria-hidden="true"></i> Look-Up Lists</a></li>
						<li><a <?php echo ($_SESSION['userTypeID'] == 1) ? 'href="?action=reportList"' : 'class="hidden"' ?>><i class="fa fa-cog" aria-hidden="true"></i> Reports</a></li>
            <li><a href="?action=hcAttendanceNew"><i class="fa fa-bars" aria-hidden="true"></i> Attendance</a></li>
            <li><a href="?action=clientList"><i class="fa fa-users" aria-hidden="true"></i> Clients</a></li>
        </ul>
    </header>
<main>
