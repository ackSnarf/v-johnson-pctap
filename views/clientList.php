<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">

<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
<img src="images/closedDoor.png">
</nav>

<div id="pageDiv" class="clearfix">  <!-- contents wrapped in a div-->

<!-- SEARCH ALL CLIENTS -->
	<section>
			<form method="post" action="">
				<div class="large-box">
					<h5>Search All Clients</h5>
				    <input class="w3-input w3-border w3-round-large" type="text" list="clients" id="txtClients"/>
						    <datalist id="clients">
						        <?php foreach ($allClients as $c): ?>
						          <option value="<?php echo $c['clientName']; ?>" id="<?php echo $c['contactID']; ?>" >
						        <?php endforeach; ?>;
						    </datalist>
				    <input type="hidden" name="contactID" id="contactID" value="null">
				 </div>


					<button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-blue"
					type="submit" onClick="selectedClient()" id="btnSearch" name="action" value="clientEdit">Go to Details</button>
					<br /><br />
				 <hr>
			</form>
	</section>

	<!--NEW CLIENT-->
	<section>
			<form  method="post" action="">
	        <input type="hidden" name="action" value="clientNew">
	        <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-blue"
					type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Client</button>
			</form>
		<hr>
	</section>

	<!--CURRENT CLIENT LIST -->
	<section>
			<table style="width:20%">
				<thead>
						<tr>
							<th style="background-color: white">Current Clients</th>
						</tr>
				</thead>
				 <tbody>
						<?php foreach($clients as $client): ?>
					    <tr>
								<td style="border: none"><a href="?action=clientEdit&contactID=<?php echo $client['contactID']; ?>&editingClient=1"><?php echo $client['clientName']; ?></a></td>
					    </tr>
						<?php endforeach; ?>
				</tbody>
		</table>
	</section>
</div>

<script type="text/javascript" src="javascript/search.js"></script>

<?php include("views/footer.php") ?>
