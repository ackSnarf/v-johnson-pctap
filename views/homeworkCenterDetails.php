<?php include('views/header.php'); ?>
<link href="css/form.css" rel="stylesheet" type="text/css">

<!--this is the side bar-->
<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
<img src="images/closedDoor.png">
</nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

      <section>
           <div class="large-box">
              <label>Client Name</label>
              <input  class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
          </div>

           <div class="large-box">
              <label for='caseNumber'>Court Case Number</label>
          		<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
          </div>
          <br /><br />
      	<hr>
      </section>

      <section>
            <form class="formStyle" id="frmDetails" method="post" action= ".">
              <input type='hidden' name='action' value='<?php echo (strpos($action, 'New') > 0) ? 'homeworkCenterSaveNew' : 'homeworkCenterUpdate'; ?>'>
              <input type='hidden' name='hcattendanceID' value='<?php echo fieldValue($details, 'hcattendanceID'); ?>'>
              <input type='hidden' name='tapID' value='<?php echo fieldValue($details, 'tapID'); ?>'>
              <input type='hidden' name='origHcattendDate' id='origHcattendDate' value='<?php echo fieldValue($details, 'hcattendDate') ?>'>
              <input type='hidden' name='javaScriptValidated' id='javaScriptValidated' value='false'>

              <br />
              <div class="date-box">
                <label for ='hcattendDate'>Date&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                <input class="w3-input w3-border w3-round-large" type='date' name='hcattendDate' id='hcattendDate' value='<?php echo fieldValue($details, 'hcattendDate', date("Y-m-d")); ?>'>
                <img src="images/error.png" id="errHcattendDate" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'hcattendDate'); ?>>
              </div>

              <div class="tenChar-box">
                  <?php $hcattendStatusID=fieldValue($details, 'hcattendStatusID');?>
                		<label for="hcattendStatusID">Status&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                  		<select class="w3-input w3-border w3-round-large" name="hcattendStatusID" id="hcattendStatusID" >
                  			<?php foreach ($hcattendStatuses as $h) : ?>
                  				<option value="<?php echo $h['hcattendStatusID']; ?>" <?php if($h['hcattendStatusID']==$hcattendStatusID) echo ' selected'; ?> >
                  					<?php echo $h['hcattendStatus']; ?>
                  				</option>
                  			<?php endforeach; ?>
                  		</select>
                		<img src="images/error.png" id="errHcattendStatusID" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'hcattendStatusID'); ?>>
                </div>

                <div class="sixChar-box">
                <div class="twoChar-box">
                  <label for='hcattendPoints'>Attendance&nbspPoints</label>
              		<input class="w3-input w3-border w3-round-large" type='number' name='hcattendPoints' id='hcattendPoints' value='<?php echo fieldValue($details, 'hcattendPoints'); ?>' <?php echo $HCATTENDPOINTS_VALIDATION ?>>
              		<img src="images/error.png" id="errHcattendPoints" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'hcattendPoints'); ?>>
              	</div>
              </div>

                <div class="twoChar-box">
                  <label for='homeworkPoints'>Homework&nbspPoints</label>
              		<input class="w3-input w3-border w3-round-large" type='number' name='homeworkPoints' id='homeworkPoints' value='<?php echo fieldValue($details, 'homeworkPoints'); ?>' <?php echo $HOMEWORKPOINTS_VALIDATION ?>>
              		<img src="images/error.png" id="errHomeworkPoints" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'homeworkPoints'); ?>>
              	</div>

                <section>
                  <label>&nbsp;</label>
                  <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type='submit' id='btnSave' name='btnSave'>  <img src='images/save.png'> Save</button>
                  <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
                  <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type='submit' id='btnCancel' name='btnCancel'><img src='images/list.png'> Homework Center List</button>
                  <button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red" type='submit' id='btnDelete' name='btnDelete'><img src='images/delete.png'> Delete</button>
                </section>
            </form>
      </section>
</div>

<script src="javascript/areyousure.js"></script>
<script src="javascript/NumberKeyPress.js"></script>
<script src="javascript/FormatNumber.js"></script>
<script src="javascript/isDate.js"></script>
<script src="javascript/DateFormat.js"></script>
<script src="javascript/homeworkCenter.js"></script>

<?php include('views/footer.php'); ?>
