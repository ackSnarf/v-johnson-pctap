<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">
<link href="css/form.css" rel="stylesheet" type="text/css">

    <nav>
        <ul>
            <li><a class="active" href="#noAction">Client</a></li>
            <li><a class="inactive" href="#noAction">Contact</a></li>
            <li><a class="inactive" href="#noAction">Case Notes</a></li>
            <li><a class="inactive" href="#noAction">Hearings</a></li>
            <li><a class="inactive" href="#noAction">Community Service</a></li>
            <li><a class="inactive" href="#noAction">Homework Center</a></li>
            <li><a class="inactive" href="#noAction">Grades</a></li>
        </ul>
    </nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

<section>

<form class="formStyle" id="clientDetails" method="post" action=".">

		<!-- HIDDEN FIELDS -->
        <input type="hidden" name="action" value="<?php echo ($new)?'clientNew':'clientEdit' ?>">
        <input type="hidden" name="contactTypeID" id="contactTypeID" value="5" >
        <input type="hidden" name="contactID" id="contactID" value="<?php echo fieldValue($_SESSION, 'contactID'); ?>" >
        <input type="hidden" name="clientID" id="clientID" value="<?php echo fieldValue($_SESSION, 'clientID'); ?>" >
        <!--<input type="hidden" name="courtOrderID" id="courtOrderID" value="<!?php echo fieldValue($details, 'courtOrderID'); ?>" >-->
        <input type="hidden" name="javascriptValidated" id="javascriptValidated" value="false">
        <input type="hidden" name="orgContactFirstName" id="orgContactFirstName" value="<?php echo fieldValue($details, 'contactFirstName'); ?>">
        <input type="hidden" name="orgContactLastName" id="orgContactLastName" value="<?php echo fieldValue($details, 'contactLastName'); ?>">

		<!--FIRST NAME-->
		<div class="large-box">
			<label for="contactFirstName">First Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
			<input class="w3-input w3-border w3-round-large" type="text" id="contactFirstName" name="contactFirstName" value="<?php echo fieldValue($details, 'contactFirstName'); ?>" <?php echo $CONTACTFIRSTNAME_VALIDATION ?> size="20">
			<img src="images/error.png" id="errContactFirstName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactFirstName'); ?> >
		</div>

		<!--LAST NAME-->
		<div class="large-box">
			<label for="contactLastName">Last Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
			<input class="w3-input w3-border w3-round-large" type="text" id="contactLastName" name="contactLastName" value="<?php echo fieldValue($details, 'contactLastName'); ?>" <?php echo $CONTACTLASTNAME_VALIDATION ?> size="20">
			<img src="images/error.png" id="errContactLastName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactLastName'); ?> >
		</div>

		<!--KIDTRAX ID-->
		<div class="sixChar-box">
			<label for='kidtraxID'>KidTrax ID </label>
			<input class="w3-input w3-border w3-round-large" type='number' name='kidtraxID' id='kidtraxID' value="<?php echo fieldValue($details, 'kidtraxID'); ?>">
      <img src="images/error.png" id="errKidtraxID" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'kidtraxID'); ?> >
		</div>

    <!--DOB-->
    <div class="date-box">
      <label for='dob'>Birthday</label>
      <input class="w3-input w3-border w3-round-large" type='date' name='dob' id='dob' value="<?php echo fieldValue($details, 'dob'); ?>">
      <img src="images/error.png" id="errDob" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'dob'); ?> >
    </div>

    <hr>

		<!--RACE-->
		<div class="tenChar-box">
			<?php $raceID=fieldValue($details, 'raceID');?>
				<label for="raceID">Race</label><br/>
				<select class="w3-input w3-border w3-round-large" name="raceID" id="raceID" >
					<?php foreach ($race as $r) : ?>
						<option value="<?php echo $r['raceID']; ?>" <?php if($r['raceID']==$raceID) echo 'selected'; ?> >
							<?php echo $r['race']; ?>
						</option>
					<?php endforeach; ?>
				</select>
		</div>

		<!--GENDER-->
		<div class="sixChar-box">
			<?php $genderID=fieldValue($details, 'genderID');?>
			 <label for="genderID">Gender</label><br/>
				<select class="w3-input w3-border w3-round-large" name="genderID" id="genderID" >
					<?php foreach ($genders as $g) : ?>
						<option value="<?php echo $g['genderID']; ?>" <?php if($g['genderID']==$genderID) echo ' selected'; ?> >
							<?php echo $g['gender']; ?>
						</option>
					<?php endforeach; ?>
				</select>
		</div>

        <hr>

		<!--STREET ADDRESS-->
		<div class="large-box">
			<label for="contactStreetAddress">Address</label>
			<input class="w3-input w3-border w3-round-large" type="text" id="contactStreetAddress" name="contactStreetAddress" value="<?php echo fieldValue($details, 'contactStreetAddress'); ?>" <?php echo $CONTACTSTREETADDRESS_VALIDATION ?> size="20">
			<img src="images/error.png" id="errContactStreetAddress" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactStreetAddress'); ?> >
		</div>

        <!--CITY-->
        <div class="large-box">
            <label for="contactCity">City</label><br />
            <input class="w3-input w3-border w3-round-large" type="text" id="contactCity" name="contactCity" value="<?php echo fieldValue($details, 'contactCity'); ?>" <?php echo $CONTACTCITY_VALIDATION ?> size="20">
            <img src="images/error.png" id="errContactCity" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactCity'); ?> >
        </div>

        <!--STATE-->
        <div class="twoChar-box">
            <label for="contactState">State</label>
            <input class="w3-input w3-border w3-round-large" type="text" id="contactState" name="contactState" value="<?php echo fieldValue($details, 'contactState'); ?>" <?php echo $CONTACTSTATE_VALIDATION ?> size="20">
            <img src="images/error.png" id="errContactState" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactState'); ?> >
        </div>

        <!--ZIP CODE-->
        <div class="sixChar-box">
            <label for="contactZipCode">Zip Code</label>
            <input class="w3-input w3-border w3-round-large" type="number" id="contactZipCode" name="contactZipCode" value="<?php echo fieldValue($details, 'contactZipCode'); ?>" <?php echo $CONTACTZIPCODE_VALIDATION ?> size="20">
            <img src="images/error.png" id="errContactZipCode" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactZipCode'); ?> >
        </div>



        <!--EMAIL-->
        <div class="large-box">
            <label for="contactEmail">Email</label>
            <input class="w3-input w3-border w3-round-large" type="text" id="contactEmail" name="contactEmail" value="<?php echo fieldValue($details, 'contactEmail'); ?>" <?php echo $CONTACTEMAIL_VALIDATION ?> size="20">
            <img src="images/error.png" id="errContactEmail" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactEmail'); ?> >
        </div>
        <hr>

<!-- New Client only -->
<?php if ($new) { ?>
    <?php include("views/phoneDetails.php"); ?>
    </form>
    </section>

<?php } else { ?>
<!-- Edit Client only -->
    <section>
        <!--EDIT CLIENT BUTTONS -->
        <label>&nbsp;</label>
        <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button>
        <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
    </section>
    </form>
    </section>

    <?php include("views/phoneList.php"); ?>

	<section>
		<form  method="post" action="">
				<br />
				<input type="hidden" name="action" value="courtOrderNew">
				<button  class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Case</button>
		</form>
	</section>

	<section>
        <table style="width:40%">
        <thead>
            <tr>
              <th>Case Number</th>
              <th>Referral Date</th>
              <th>Discharge Date</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($tapEnrollment as $te): ?>
            <tr>
      				<td><a href="?action=courtOrderDetails&courtOrderID=<?php echo $te['courtOrderID']; ?>"><?php echo $te['caseNumber']; ?></a></td>
      				<td><?php echo date_format(new DateTime($te['referralDate']), 'm/d/Y'); ?></td>
              <td><?php echo isset($te['dischargeDate'])? date_format(new DateTime($te['dischargeDate']), 'm/d/Y'):'' ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
	</section>
<?php } ?> <!--end edit client only-->

</div> <!-- end the formatting div -->

<!--JAVASCRIPT-->
<!-- New Contact only -->
<?php if ($new) { ?>
    <?php echo '<script type="text/javascript" src="javascript/clientsNew.js"></script><script type="text/javascript" src="javascript/formatPhone.js"></script>'; ?>
<?php } else { ?>
    <?php echo '<script type="text/javascript" src="javascript/clients.js"></script>'; ?>
 <?php } ?>
<!--end if -->
<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/DateFormat.js"></script>
<script type="text/javascript" src="javascript/isDate.js"></script>
<script type="text/javascript" src="javascript/numericKeyPress.js"></script>
<script type="text/javascript" src="javascript/toTitleCase.js"></script>

<?php include("views/footer.php") ?>
