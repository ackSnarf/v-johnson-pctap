<link href="css/login.css" rel="stylesheet" type="text/css">
<link href="css/w3.css" rel="stylesheet" type="text/css">

<link href="bootstrap/css/spacelabs.css" rel="stylesheet" type="text/css">
<link href="bootstrap/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

  <div id="parent">
		  <div id="wide">
	       <img src="images/openDoor.png">
       </div> <!--wide-->

       <div id="four">

        	<form class='formStyle' id='frmDetails' method='post' action='.'>
        		<input type='hidden' name='action' value='login'>

            <br /><br /><br /><br /><br /><br /><br /><br />
            <div class="logIn-box">
              <h3 for='txtUserName'>User Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></h3>
          		<input class="w3-input w3-border w3-round-large" type='text' name='userName' id='txtUserName' size='20' value="<?php echo $userName ?>">
            </div>
        		<br />

            <div class="logIn-box">
          		<h3 for='txtPassword'>Password&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></h3>
          		<input class="w3-input w3-border w3-round-large" type='password' name='password' id='txtPassword' size='20' value="<?php echo $password; ?>">
            </div>
        		<br><br><br>

        		<label>&nbsp;</label>
        		<button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-blue" type='submit' id='btnLogin' name='btnLogin'><img src='images/unlock.png'> Login</button>
        		<br>

        		<label>&nbsp;</label>
        		<label class='flowLabel'><?php echo $loginMessage ?></label>
        	</form>
        </div>
    </div>
