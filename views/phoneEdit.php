<?php include ("views/header.php"); ?>
    <link href="css/form.css" rel="stylesheet" type="text/css">

<!--this is the side bar-->
		<nav style ="border: 3px solid #f8f8ff; background-color = #0687bf">
		<img src="images/closedDoor.png">
		</nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

<section>
  	<div class="large-box">
    	<label>Client Name</label>
        <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'
    	  value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
    </div>
    <br /><br />
	<hr>
</section>

<section>
<form class="formStyle" id="phone" method="post" role="form" action=".">

    <!--HIDDEN FIELDS-->
    <!--THIS LINE CAUSES A PROBLEM WITH NAVIGATION AFTER RETURNING TO FORM BECAUSE OF PHP ERROR ON PHONENEW-->
    <!--THE PHONENUMBERID is set, but is blank upon returning, causing value to be set to phoneEdit when it should be phoneNew-->
    <!--NO ISSUES IF JAVASCRIPT ACTIVE-->
    <input type="hidden" name="action" id="action" value="<?php echo (isset($_REQUEST['phoneNumberID'])) ? "phoneEdit" : "phoneNew";?>">
    <input type="hidden" name="contactID" value="<?php echo $_REQUEST['contactID']; ?>">
    <input type="hidden" name="phoneNumberID" value="<?php echo (isset($_REQUEST['phoneNumberID'])) ? $_REQUEST['phoneNumberID'] : "";?>">
    <input type="hidden" name="javascriptValidated" id="javascriptValidated" value="false">
    <input type="hidden" name="sender" id="sender" value="<?php echo ($_REQUEST['sender']); ?>">

    <br />

    <?php include("views/phoneDetails.php");?>

</form>

</section>

</div> <!-- end the formatting div -->

<!--JAVASCRIPT-->
<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/formatPhone.js"></script>
<script type="text/javascript" src="javascript/numericKeyPress.js"></script>
<script type="text/javascript" src="javascript/phones.js"></script>
<?php include("views/footer.php") ?>
