<?php include ("views/header.php"); ?>

<link href="css/table.css" rel="stylesheet" type="text/css">


<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
	<img src="images/closedDoor.png">
</nav>

<div id="pageDiv" class="clearfix">
	<section>
		<div>
			<fieldset style="border:white;" >
		            <h3>Look-Up List Values</h3>
		            <div>
		                <div>
		                    <h5>Select a table from the list below</h5>
		                </div>
		                <div class="row-fluid">
		                    <input type="hidden" name="action" value="lookUpSelect">
		                    <input type="hidden" id="dataNc" name="dataNc" value="<?php echo $nc; ?>">
		                    <form id="lookUp" method="post" role="form" class="formStyle">
		                        <div class="large-box">
		                                    <select id="table" name="table" class="w3-input w3-border w3-round-large" onchange="document.forms['lookUp'].submit()" >
		                                        <option value=""<?php if($table=='') echo ' selected'; ?> disabled> </option>
																						<option value="hcattendstatuses"<?php if($table=='hcattendstatuses') echo ' selected'; ?>>Attendance Status</option>
		                                        <option value="contactTypes"<?php if($table=='contactTypes') echo ' selected'; ?>>Contact Types</option>
		                                        <option value="dischargeReasons"<?php if($table=='dischargeReasons') echo ' selected'; ?>>Discharge Reasons</option>
		                                        <option value="genders"<?php if($table=='genders') echo ' selected'; ?>>Genders</option>
		                                        <option value="gradeLevels"<?php if($table=='gradeLevels') echo ' selected'; ?>>Grade Levels</option>
		                                        <option value="noteTypes"<?php if($table=='noteTypes') echo ' selected'; ?>>Note Types</option>
		                                        <option value="races"<?php if($table=='races') echo ' selected'; ?>>Races</option>
		                                        <option value="schools"<?php if($table=='schools') echo ' selected'; ?>>Schools</option>
		                                        <option value="subjects"<?php if($table=='subjects') echo ' selected'; ?>>Subjects</option>
		                                        <option value="targets"<?php if($table=='targets') echo ' selected'; ?>>Target Types</option>
		                                    </select>
		                            <div class="col-sm-1<?php if ($table=='') echo ' hidden'?>">
		                                <button id="addItem" class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="button" onclick="newData()"><i class="fa fa-plus" aria-hidden="true"></i> Add Item</button>
		                        		</div>
		                    </form>
		                </div>
										<br /><br />
										<hr>
		                <div >
		                    <table style="width:30%">
		                        <thead>
		                            <tr>
		                                <th style="width:150px;">Action</th>
		                                <th>Value</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                           <?php
		                                foreach($items as $data) {
		                                    echo '<tr><td><button class="btn btn-info btn-sm" type="button" id="item' . $data[$dataId] . '" onclick="editData(' . $data[$dataId] . ',\'' . $data[$dataValue] . '\')" value="' . $data[$dataId] . '">';
		                                    echo '<i class="fa fa-pencil" aria-hidden="true"></i> Edit</button><td>' . $data[$dataValue] . '</td></tr>';
		                                }
		                            ?>
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </fieldset>
		        <div class="well module-header hidden" id="newItem" style="width:800px;">
		            <fieldset>
		                <legend>New <?php echo $table; ?> Data Item</legend>
		                <form id="dataNew" method="post" role="form" class="form-horizontal" action=".">
		                    <input type="hidden" name="action" id="action" value="lookUpNew">
		                    <input type="hidden" name="table" id="table" value="<?php echo $table; ?>">
		                    <div class="row-fluid">
		                        <div class="col-sm-12">
		                            <div id="grpNewItem" class="form-group has-feedback">
		                                <label for="newDataValue" class="col-sm-12 text-left">New Value</label>
		                                <div class="col-sm-12">
		                                    <input type="text" id="newDataValue" name="newDataValue" class="form-control" value="">
		                                    <span id="errNewDataValue" class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row-fluid">
		                        <div class="col-sm-12">
		                            <div class="input-group col-sm-12">
		                                    <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="button" id="Save" name="Save" onclick="saveNewItem()"><img src='images/save.png' alt=""> Save</button>
		                                    <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="button" id="Cancel" name="Cancel" onclick="cancelNewItem()"><img src='images/list.png' alt=""> Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
		            </fieldset>
		        </div>
						<section>
		        <div class="well module-header hidden" id="editItem" style="width:800px;">
		            <fieldset>
		                <legend>Edit <?php echo $table; ?> Data Item</legend>
		                <form id="dataEdit" method="post" role="form" class="form-horizontal" action=".">
		                    <input type="hidden" name="action" id="action" value="lookUpEdit">
		                    <input type="hidden" name="table" id="table" value="<?php echo $table; ?>">
		                    <input type="hidden" name="dataId" id="dataId" value="">
		                    <div class="row-fluid">
		                        <div class="col-sm-12">
		                            <div id="grpOrigItem" class="form-group has-feedback">
		                                <label for="origDataValue" class="col-sm-12 text-left">Original Value</label>
		                                <div class="col-sm-12">
		                                    <input type="text" id="origDataValue" name="origDataValue" class="form-control" value="" disabled>
		                                    <span id="errOrigDataValue" class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row-fluid">
		                        <div class="col-sm-12">
		                            <div id="grpEditItem" class="form-group has-feedback">
		                                <label for="editDataValue" class="col-sm-12 text-left">New Value</label>
		                                <div class="col-sm-12">
		                                    <input type="text" id="editDataValue" name="editDataValue" class="form-control" value="">
		                                    <span id="errEditDataValue" class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row-fluid">
		                        <div class="col-sm-12">
		                            <div class="input-group col-sm-12">
		                                <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="button" id="Save" name="Save" onclick="saveEditItem()"><img src='images/save.png'></i> Save</button>
		                                <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="button" id="Cancel" name="Cancel" onclick="cancelEditItem()"><img src='images/list.png'></i> Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
		            </fieldset>
		        </div>
					</section>
		    </div>
			</section>
</div>

<script type="text/javascript" src="javascript/lookUp.js"></script>

<?php include("views/footer.php") ?>
