<?php include ("views/header.php"); ?>
	<link href="css/form.css" rel="stylesheet" type="text/css">

	<!--this is the side bar-->
	<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
		<img src="images/closedDoor.png">
	</nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->
	<section>
		  <form class ="formStyle" id="frmDetails" method="post" action= "">
		      <input type='hidden' name ='action' value='<?php echo $_REQUEST['action']=='staffAdd'?'staffNew':'staffUpdate'; ?>'>
		      <input type='hidden' name ='staffID' id = 'staffID' value = "<?php echo fieldValue($details, 'staffID'); ?>">
		      <input type='hidden' name='javascriptValidated' id='javascriptValidated' value='false'>
		      <input type='hidden' name='orgFirstName' id='orgStaffFirstName' value='<?php echo fieldValue($details, 'staffFirstName') ?>'>
		      <input type='hidden' name='orgLastName' id='orgStaffLastName' value='<?php echo fieldValue($details, 'staffLastName') ?>'>
		      <input type='hidden' name='dupNameCount' id='dupNameCount' value=0>

			       <div class="large-box">
				        <label for='firstName'>First Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
			          <input class="w3-input w3-border w3-round-large" type='text' name='staffFirstName' id='staffFirstName' size='20' value="<?php echo fieldValue($details, 'staffFirstName'); ?>">
			          <img src="images/error.png" id="errStaffFirstName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffFirstName'); ?>  >
		         </div>

		          <div class="large-box">
		            <label for='lastName'>Last Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
				    		<input class="w3-input w3-border w3-round-large" type='text' name='staffLastName' id='staffLastName' size='20' value="<?php echo fieldValue($details, 'staffLastName'); ?>">
				    		<img src="images/error.png" id="errStaffLastName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffLastName'); ?> >
		          </div>

		          <div class="date-box">
		            <label>Birthday</label>
		            <input class="w3-input w3-border w3-round-large" type='Date' name='staffDob' id='staffDob' value="<?php echo fieldValue($details, 'staffDob'); ?>">
		            <img src="images/error.png" id="errStaffDob" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffDob'); ?> >
		      		</div>

							<hr>
							<!-- PHONE -->
							<?php if(empty($details['staffPhoneNumber']))
											$phone = '';
											else if(!isset($errors['staffPhoneNumber']))
													$phone = formatPhone($details['staffPhoneNumber']); //Only format phone if there's no error
										else
											$phone = $details['staffPhoneNumber'];
							?>
							<div class="tenChar-box">
								<label for='phone'>Phone</label>
								<input class="w3-input w3-border w3-round-large" type='text' name='staffPhoneNumber' id='staffPhoneNumber' size='20' value='<?php echo $phone; ?>'>
								<img src="images/error.png" id="errStaffPhoneNumber" alt="Error icon" <?php echo errorStyle($errors, 'staffPhoneNumber'); ?> >
							</div>
							<!-- PHONE -->

							<div class="large-box">
								<label>Email</label>
								<input class="w3-input w3-border w3-round-large" type='text' name='staffEmail' id='staffEmail' size='20' value="<?php echo fieldValue($details, 'staffEmail'); ?>">
								<img src="images/error.png" id="errStaffEmail" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffEmail'); ?> >
							</div>

							<hr>
							<div class="large-box">
		            <label>Address</label>
		            <input class="w3-input w3-border w3-round-large" type='text' name='staffStreetAddress' id='staffStreetAddress' size='20' value="<?php echo fieldValue($details, 'staffStreetAddress'); ?>">
				    		<img src="images/error.png" id="errstaffStreetAddress" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffStreetAddress'); ?> >
		  				</div>

		  				<div class="large-box">
		            <label>City</label>
		            <input class="w3-input w3-border w3-round-large" type='text' name='staffCity' id='staffCity' size='20' value="<?php echo fieldValue($details, 'staffCity'); ?>">
				    		<img src="images/error.png" id="errStaffCity" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffCity'); ?> >
		  				</div>


		    			<div class="twoChar-box">
		            <label>State</label>
		            <input class="w3-input w3-border w3-round-large" type='text' name='staffState' id='staffState' size='2' maxLength="2" value="<?php echo fieldValue($details, 'staffState'); ?>">
				    		<img src="images/error.png" id="errStaffState" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffState'); ?> >
		  				</div>


		  			 	<div class="sixChar-box">
		            <label>Zip</label>
		            <input class="w3-input w3-border w3-round-large" type='text' name='staffZipCode' id='staffZipCode' size='5' maxLength="5" value="<?php echo fieldValue($details, 'staffZipCode'); ?>">
				    		<img src="images/error.png" id="errStaffZipCode" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'staffZipCode'); ?> >
							</div>

							<hr>
							<div class="date-box">
								<label>Start Date&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
								<input class="w3-input w3-border w3-round-large" type='Date' name='staffStartDate' id='staffStartDate' value="<?php echo fieldValue($details, 'staffStartDate'); ?>">
								<img src="images/error.png" id="errStaffStartDate" width="14" height="14" alt="Error icon"
								<?php echo errorStyle($errors, 'staffStartDate'); ?> >
							</div>

							<div class="date-box">
								<label>End Date</label>
								<input class="w3-input w3-border w3-round-large" type='Date' name='staffEndDate' id='staffEndDate' value="<?php echo fieldValue($details, 'staffEndDate'); ?>">
								<img src="images/error.png" id="errStaffEndDate" width="14" height="14" alt="Error icon"
								<?php echo errorStyle($errors, 'staffEndDate'); ?> >
							</div>

							<hr>
							<div class="large-box">
			          <label>Notes</label>
			          <textarea name="staffNote" id="staffNote" cols=40 rows=3><?php echo fieldValue($details, 'staffNote'); ?></textarea>
							</div>
						  <br />
							<br />

							<section>
								<label>&nbsp;</label>
									<button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type='submit' id='btnSave' name='btnSave'><img src='images/save.png'> Save</button>
									<button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
									<button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type='submit' id='btnCancel' name='btnCancel'><img src='images/list.png'> Staff List</button>
							</section>
				</form>
   </section>
</div>

<script src="javascript/areyousure.js"></script>
<script src="javascript/NumericKeyPress.js"></script>
<script src="javascript/FormatNumber.js"></script>
<script src="javascript/isDate.js"></script>
<script src="javascript/DateFormat.js"></script>
<script src="javascript/toTitleCase.js"></script>
<script src="javascript/formatPhone.js"></script>
<script src="javascript/staffDetails.js"></script>

<?php include ("views/footer.php"); ?>
