<section>
    <a href="?action=phoneNew&contactID=<?php echo ($_REQUEST['contactID']); ?><?php echo($_REQUEST['action'] == 'clientEdit')? '&sender=client':'&sender=contact'; ?>">
      <i class="fa fa-phone-square" aria-hidden="true"></i> New Phone</a>

    <table style="width:40%">
        <thead>
            <tr>
                <th>Phone Number</th>
                <th class="center">Type</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($phones as $phone):?>
            <tr>
                <td><?php echo displayPhoneNumber($phone['phoneNumber']); ?></td>
                <td class="center"><?php echo $phone['phoneNumberType'];?></td>
                <td class="center">
                    <a href="?action=phoneEdit&phoneNumberID=<?php echo $phone['phoneNumberID']; ?>&contactID=<?php echo ($_REQUEST['contactID']); ?><?php if ($_REQUEST['action'] == 'clientEdit')
                    { ?><?php echo '&sender=client'; ?><?php } else { ?><?php echo '&sender=contact'; ?><?php } ?>"><i class="fa fa-pencil" aria-hidden="true"></i><?php echo" Edit";?></a>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php if (count($phones) == 0) : ?>
            <tr>
                <td>(None saved)</td>
                <td></td>
                <td></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</section>
