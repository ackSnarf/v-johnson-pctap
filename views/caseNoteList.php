<?php include ("views/header.php"); ?>
	  <link href="css/table.css" rel="stylesheet" type="text/css">

	<nav>
		<ul>
			<li><a href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
			<li><a href="?action=contactList">Contact</a></li>
			<li><a class="active" href="?action=caseNoteList">Case Notes</a></li>
			<li><a href="?action=hearingList">Hearings</a></li>
			<li><a href="#commServicePage">Community Service</a></li>
      <li><a href="?action=homeworkCenterList">Homework Center</a></li>
			<li><a href="?action=gradeList">Grades</a></li>
		</ul>
	</nav>

<div id="pageDiv" class="clearfix">

			<section>
				   <div class="large-box">
						   	<label>Client Name</label>
						    <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
					 </div>

					 <div class="large-box">
							 <label for='caseNumber'>Court Case Number</label>
							 <input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
					 </div>
					 <br /><br />
					 <hr>
		  </section>

			<section>
					<form  method="post" action="">
			        <input type="hidden" name="action" value="caseNoteNew">
			        <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Note</button>
					</form>
				<br />
			</section>

		<section>
			<table style="width:80%">
					<thead>
							<tr>
									<th>Details</th>
									<th>Note Type</th>
									<th>Target</th>
									<th class="center">Target Contacted</th>
									<th>Note Date</th>
									<th>Note Time</th>
									<th>Staff Name</th>
									<th>Note...</th>
							</tr>
					</thead>
					 <tbody>
							<?php foreach($caseNotes as $caseNote): ?>
						    <tr>
									<td><a href="?action=caseNoteDetails&caseNoteID=<?php echo $caseNote['caseNoteID']; ?>"><?php echo $caseNote['caseNoteID']; ?></a></td>
						      <td><?php echo $caseNote['noteTypeDescription']?></td>
						      <td><?php echo $caseNote['targetType']?></td>
									<td class="center"><?php if($caseNote['success']==1) echo '<img src="images/checkMark.png" alt="">' ?></td>
						      <td><?php echo date_format(new DateTime($caseNote['caseNoteDate']), 'm/d/Y');?></td>
									<td><?php echo $caseNote['caseNoteTime']?></td>
									<td><?php echo $caseNote['staffFirstName']?></td>
						      <td class="caseNote"><?php echo $caseNote['note']?></td>
						    </tr>
							<?php endforeach; ?>
				 </tbody>
			</table>
		</section>

</div>
<?php include("views/footer.php") ?>
