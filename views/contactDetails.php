<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">
<link href="css/form.css" rel="stylesheet" type="text/css">

<nav>
    <ul>
        <li><a href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
        <li><a class="active" href="?action=contactList">Contact</a></li>
        <li><a href="?action=caseNoteList">Case Notes</a></li>
        <li><a href="?action=hearingList">Hearings</a></li>
        <li><a href="#commServicePage">Community Service</a></li>
        <li><a href="?action=homeworkCenterList">Homework Center</a></li>
        <li><a href="?action=gradeList">Grades</a></li>
    </ul>
</nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

    <section>
        <div class="large-box">
    	     <label>Client Name</label>
           <input class="w3-input w3-border w3-round-large"  type='text' name='clientName' id='clientName' size='20' value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
        </div>

        <div class="large-box">
            <label for='caseNumber'>Court Case Number</label>
            <input  class="w3-input w3-border w3-round-large"  type='text' name='caseNumber' id='caseNumber' size='20' value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
        </div>
        <br /><br />
        <hr>
     </section>

    <section>
          <form class="formStyle" id="contactDetails" method="post" action=".">

                  <!--HIDDEN FIELDS-->
                  <input type="hidden" name="action" value="<?php echo ($new)?'contactNew':'contactEdit' ?>">
                  <input type="hidden" name="contactID" id="contactID" value="<?php echo fieldValue($details, 'contactID'); ?>">
                  <input type="hidden" name="javascriptValidated" id="javascriptValidated" value="false">
                  <input type="hidden" name="orgContactFirstName" id="orgContactFirstName" value="<?php echo fieldValue($details, 'contactFirstName'); ?>">
                  <input type="hidden" name="orgContactLastName" id="orgContactLastName" value="<?php echo fieldValue($details, 'contactLastName'); ?>">

              <br />
              <!--CHECKBOX: EMERGENCY CONTACT-->
              <div class="extraLong-box">
                <?php $emergencyContact= fieldValue($details, 'emergencyContact', false); ?>
                <input type="checkbox" id="chkEmergencyContact" name="emergencyContact" value="yes"<?php echo ($emergencyContact)?' checked':''; ?>>
                <label for="emergencyContact" class="flowLabel">Emergency&nbspContact?</label>
              </div>
              <br />
                  <!--FIRST NAME-->
          		<div class="large-box">
          			<label for="txtContactFirstName">First Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactFirstName" name="contactFirstName" value="<?php echo fieldValue($details, 'contactFirstName'); ?>" <?php echo $CONTACTFIRSTNAME_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactFirstName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactFirstName'); ?> >
          		</div>


                  <!--LAST NAME-->
          		<div class="large-box">
          			<label for="txtContactLastName">Last Name&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactLastName" name="contactLastName" value="<?php echo fieldValue($details, 'contactLastName'); ?>" <?php echo $CONTACTLASTNAME_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactLastName" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactLastName'); ?> >
          		</div>

                  <!--CONTACT TYPES TABLE INPUT (RELATIONSHIP)-->
          		<div class="target-box">
                  <?php $contactTypeID=fieldValue($details, 'contactTypeID'); ?>
                      <label for="cmbContactTypeID">Relationship to Client</label>
                        <select class="w3-input w3-border w3-round-large" id="cmbContactTypeID" name="contactTypeID">
          				          <?php foreach ($relationships as $r) : ?>
                              <option value="<?php echo $r['contactTypeID']; ?>" <?php if($r[ 'contactTypeID']==$contactTypeID) echo " selected"; ?> ><?php echo $r[ 'contactTitle']; ?></option>
                            <?php endforeach; ?>
                        </select>
                      <img src="images/error.png" id="errContactTypeID" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactTypeID'); ?> >
              </div>


              <hr>
              <!--STREET ADDRESS-->
              <div class="large-box">
          			<label for="txtContactStreetAddress">Address</label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactStreetAddress" name="contactStreetAddress" value="<?php echo fieldValue($details, 'contactStreetAddress'); ?>" <?php echo $CONTACTSTREETADDRESS_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactStreetAddress" width="25" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactStreetAddress'); ?> >
          		</div>

                  <!--CITY-->
          		<div class="large-box">
                <label for="txtContactCity">City</label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactCity" name="contactCity" value="<?php echo fieldValue($details, 'contactCity'); ?>" <?php echo $CONTACTCITY_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactCity" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactCity'); ?> >
          		</div>

                  <!--STATE-->
          		<div class="twoChar-box">
                <label for="txtContactState">State</label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactState" name="contactState" value="<?php echo fieldValue($details, 'contactState'); ?>" <?php echo $CONTACTSTATE_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactState" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactState'); ?> >
          		</div>

                  <!--ZIP CODE-->
          		<div class="sixChar-box">
                <label for="numContactZipCode">Zip Code</label>
                <input class="w3-input w3-border w3-round-large" type="number" id="numContactZipCode" name="contactZipCode" value="<?php echo fieldValue($details, 'contactZipCode'); ?>" <?php echo $CONTACTZIPCODE_VALIDATION ?> size="20">
                <img src="images/error.png" id="errContactZipCode" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactZipCode'); ?> >
          		 </div>

               <hr>
                  <!--EMAIL-->
               <div class="extraLong-box">
          			<label for="txtContactEmail">Email</label>
                <input class="w3-input w3-border w3-round-large" type="text" id="txtContactEmail" name="contactEmail" value="<?php echo fieldValue($details, 'contactEmail'); ?>" size="20">
                <img src="images/error.png" id="errContactEmail" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'contactEmail'); ?> >
          		</div>



                <!-- New Contact only -->
                <?php if ($new) { ?>
                    <?php include("views/phoneDetails.php"); ?>
                <!-- Edit Contact only -->
                <?php } else { ?>
                    <section>
                      <!--BUTTONS -->
                      <label>&nbsp;</label>
                      <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button>
                      <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
                      <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="submit" id="btnCancel" name="btnCancel"><img src="images/list.png" alt=""> Contact List</button>
                      <button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red" type="submit" <?php echo strpos($action, 'New')!==false ? " hidden" : " "; ?> id="btnDelete" name="btnDelete"><img src="images/delete.png" alt=""> Delete</button>
                    </section>
                <?php include("views/phoneList.php"); ?>
               <?php } ?> <!--end edit contact only-->
          </form>
    </section>
</div> <!-- end the formatting div -->

<!--JAVASCRIPT-->
<?php if ($new) { ?>
    <?php echo '<script type="text/javascript" src="javascript/contactsNew.js"></script><script type="text/javascript" src="javascript/formatPhone.js"></script>'; ?>
<?php } else { ?>
    <?php echo '<script type="text/javascript" src="javascript/contacts.js"></script>'; ?>
 <?php } ?>
<!--end if -->
<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/numericKeyPress.js"></script>
<script type="text/javascript" src="javascript/toTitleCase.js"></script>

<?php include("views/footer.php") ?>
