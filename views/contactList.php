<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">

    <nav>
        <ul>
            <li><a href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
            <li><a class="active" href="?action=contactList">Contact</a></li>
            <li><a href="?action=caseNoteList">Case Notes</a></li>
            <li><a href="?action=hearingList">Hearings</a></li>
            <li><a href="#commServicePage">Community Service</a></li>
            <li><a href="?action=homeworkCenterList">Homework Center</a></li>
            <li><a href="?action=gradeList">Grades</a></li>
        </ul>
    </nav>

    <div id="pageDiv" class="clearfix">  <!-- contents wrapped in a div-->

        <!--CLIENT NAME-->
        <section>
            <div class="large-box">
              <label>Client Name</label>
              <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20' value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
            </div>

            <div class="large-box">
              <label for='caseNumber'>Court Case Number</label>
          		<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20' value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
            </div>
            <br /><br />
          	<hr>
        </section>

        <!--CONTACT LIST TABLE-->
        <section> <!--for table -->
            <form  method="post" action="">
                <input type="hidden" name="action" value="contactNew">
                <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Contact</button>
            </form>
            <br />


          	<table style="width:35%">
          		<thead>
                    <tr>
                        <th>Name</th>
                        <th>Relationship</th>
                        <th>Emergency Contact</th>
                    </tr>
              </thead>
              <tbody>
                  <?php foreach($details as $contact):?>
                    <tr>
                      <td><a href="?action=contactEdit&contactID=<?php echo $contact['contactID']; ?>" >
                            <?php echo $contact['contactFirstName'] . " " . $contact['contactLastName']; ?></a></td>
                      <td><?php echo $contact['contactTitle']; ?></td>
                      <td class="center"><?php if($contact['emergencyContact']==1) echo '<img src="images/checkMark.png" alt="">' ?></td>
                    </tr>
                  <?php endforeach; ?>
              </tbody>
          </table>
    </section>
</div>
<?php include ("views/footer.php"); ?>
