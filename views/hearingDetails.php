<?php include('views/header.php'); ?>
<link href="css/form.css" rel="stylesheet" type="text/css">

<div id="pageDiv" class="clearfix">

  <nav style ="border: 3px solid #f8f8ff; background-color = #0687bf">
      <img src="images/closedDoor.png">
  </nav>


    <section>
        <div class="large-box">
            <label>Client Name</label>
            <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
        </div>
        <div class="caseNum-box">
            <label for='caseNumber'>Court Case Number</label>
            <input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
        </div>
        <br /><br />
        <hr>
    </section>

    <section>
      <form  method="post" action="">
        <input type="hidden" name="action" value="caseNoteNew">
        <button  class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Note</button>
      </form>
    </section>

    <hr>
    <section >
          <form class="formStyle" id="frmDetails" method="post" action= ".">
            <input type='hidden' name='action' value='<?php echo (strpos($action, 'New') > 0) ? 'hearingSaveNew' : 'hearingUpdate'; ?>'>
            <input type='hidden' name='hearingID' value='<?php echo fieldValue($details, 'hearingID'); ?>'>
            <input type='hidden' name='courtOrderID' id='courtOrderID' value='<?php echo fieldValue($details, 'courtOrderID'); ?>'>
            <input type='hidden' name='origHearingDate' id='origHearingDate' value='<?php echo fieldValue($details, 'hearingDate') ?>'>
            <input type='hidden' name='javaScriptValidated' id='javaScriptValidated' value='false'>

            <div id="parent">
                <div id="one">
                    <div class="tenChar-box">
                      <?php $clientPresent = fieldValue($details, 'clientPresent', false); ?>
                      <label>&nbsp;</label>
                      <input type='checkbox' id='clientPresent' name='clientPresent' value='ClientPresent' <?php echo ($clientPresent)?'checked':''; ?> />
                      <label class='flowLabel' for='clientPresent'>Client&nbspPresent</label>
                      <br>
                    </div>

                    <div class="tenChar-box">
                      <?php $tapMonitor = fieldValue($details, 'tapMonitor', false); ?>
                      <label>&nbsp;</label>
                      <input type='checkbox' id='tapMonitor' name='tapMonitor' value='Monitor' <?php echo ($tapMonitor)?'checked':''; ?> />
                      <label class='flowLabel' for='tapMonitor'>TAP&nbspMonitor</label>
                    </div>

                    <div class="tenChar-box">
                      <?php $aodaScreen = fieldValue($details, 'aodaScreen', false); ?>
                      <label>&nbsp;</label>
                      <input type='checkbox' id='aodaScreen' name='aodaScreen' value='AODA' <?php echo ($aodaScreen)?'checked':''; ?> />
                      <label class='flowLabel' for='aodaScreen'>AODA&nbspScreen</label>
                      <br>
                    </div>

                    <div class="tenChar-box">
                      <?php $jipsReferral = fieldValue($details, 'jipsReferral', false); ?>
                      <label>&nbsp;</label>
                      <input type='checkbox' id='jipsReferral' name='jipsReferral' value='JIPS' <?php echo ($jipsReferral)?'checked':''; ?> />
                      <label class='flowLabel' for='jipsReferral'>JIPS&nbspReferral</label>
                      <br>
                    </div>

                    <div class="tenChar-box">
                      <?php $wrap = fieldValue($details, 'wrap', false); ?>
                      <label>&nbsp;</label>
                      <input type='checkbox' id='wrap' name='wrap' value='WRAP' <?php echo ($wrap)?'checked':''; ?> />
                      <label class='flowLabel' for='wrap'>WRAP</label>
                      <br>
                    </div>
              </div>
              <div id="two">
                    <div class="date-box">
                      <label for ='hearingDate'>Hearing&nbspDate&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                      <input class="w3-input w3-border w3-round-large" type='date' name='hearingDate' id='hearingDate' value='<?php echo fieldValue($details, 'hearingDate', date("Y-m-d")); ?>'>
                      <img src="images/error.png" id="errHearingDate" width="14" height="14" alt="Error icon"
                      <?php echo errorStyle($errors, 'hearingDate'); ?>>
                      <br>
                    </div>


                    <div class="date-box">
                      <label for ='nextHearingDate'>Next&nbspHearing&nbspDate</label>
                      <input class="w3-input w3-border w3-round-large" type='date' name='nextHearingDate' id='nextHearingDate' value='<?php echo fieldValue($details, 'nextHearingDate'); ?>'>
                      <img src="images/error.png" id="errNextHearingDate" width="14" height="14" alt="Error icon"
                      <?php echo errorStyle($errors, 'nextHearingDate'); ?>>
                   </div>

                   <div class="large-box">
                     <?php $orderEndsID=fieldValue($details, 'orderEndsID');?>
                     <label for="orderEndsID">Order&nbspEnds</label>
                     <select class="w3-input w3-border w3-round-large" name="orderEndsID" id="orderEndsID" >
                       <?php foreach ($orderEnds as $oe) : ?>
                         <option value="<?php echo $oe['orderEndsID']; ?>" <?php if($oe['orderEndsID']==$orderEndsID) echo ' selected'; ?> >
                           <?php echo $oe['orderEndsDescription']; ?>
                         </option>
                       <?php endforeach; ?>
                     </select>
                     <img src="images/error.png" id="errOrderEndsID" width="14" height="14" alt="Error icon"
                     <?php echo errorStyle($errors, 'orderEndsID'); ?>>
                     <br>
                   </div>

                   <div class="sixChar-box">
                     <?php $yearOrderEnds=fieldValue($details, 'yearOrderEnds');?>
                     <label for="yearOrderEnds">Year</label>
                     <select class="w3-input w3-border w3-round-large" name="yearOrderEnds" id="yearOrderEnds" >
                       <?php if($details=="") :?>
                         <option value="<?php echo date('Y'); ?>">&nbsp;</option>
                       <?php endif; ?>
                       <?php foreach (range(date('Y', strtotime('+1 year')), 2012) as $y) : ?>
                         <option value="<?php echo $y; ?>" <?php if($y==$yearOrderEnds) echo ' selected'; ?> >
                           <?php echo $y; ?>
                         </option>
                       <?php endforeach; ?>
                     </select>
                     <img src="images/error.png" id="errYearOrderEnds" width="14" height="14" alt="Error icon"
                     <?php echo errorStyle($errors, 'yearOrderEnds'); ?>>
                     <br>
                   </div>
              </div><!--end of wide section -->

                <div id="three">
                      <div class="large-box">
                        <?php $dmvSuspension = fieldValue($details, 'dmvSuspension', false); ?>
                        <label>&nbsp;</label>
                        <input type='checkbox' id='dmvSuspension' name='dmvSuspension' value='DMV' <?php echo ($dmvSuspension)?'checked':''; ?> />
                        <label class='flowLabel' for='dmvSuspension'>DMV&nbspSuspension</label>
                        <img src="images/error.png" id="errDmvSuspension" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'dmvSuspension'); ?>>
                      </div>

                      <div class="large-box">
                        <?php $dmvSuspensionStayed = fieldValue($details, 'dmvSuspensionStayed', false); ?>
                        <label>&nbsp;</label>
                        <input type='checkbox' id='dmvSuspensionStayed' name='dmvSuspensionStayed' value='DMVStayed' <?php echo ($dmvSuspensionStayed)?'checked':''; ?> />
                        <label class='flowLabel' for='dmvSuspensionStayed'>DMV&nbspStayed</label>
                      </div>

                      <div class="large-box">
                        <?php $dnrSuspension = fieldValue($details, 'dnrSuspension', false); ?>
                        <label>&nbsp;</label>
                        <input type='checkbox' id='dnrSuspension' name='dnrSuspension' value='DNR' <?php echo ($dnrSuspension)?'checked':''; ?> />
                        <label class='flowLabel' for='dnrSuspension'>DNR&nbspSuspension</label>
                        <img src="images/error.png" id="errDnrSuspension" width="14" height="14" alt="Error icon"
                        <?php echo errorStyle($errors, 'dnrSuspension'); ?>>
                      </div>

                      <div class="large-box">
                        <?php $dnrSuspensionStayed = fieldValue($details, 'dnrSuspensionStayed', false); ?>
                        <label>&nbsp;</label>
                        <input type='checkbox' id='dnrSuspensionStayed' name='dnrSuspensionStayed' value='DNRStayed' <?php echo ($dnrSuspensionStayed)?'checked':''; ?> />
                        <label class='flowLabel' for='dnrSuspensionStayed'>DNR&nbspStayed</label>
                        <br>
                      </div>
                </div><!-- second section-->
                <div id="wide">
                </div><!-- second wide section-->
            </div><!--end parent-->
          <hr>
            <div id="parent">
                <div id="one">
                </div>
              <div id="one">
                    <div class="dynamic-box">
                      <?php $tapAfterSchool = fieldValue($details, 'tapAfterSchool', false); ?>

                      <input type='checkbox' id='tapAfterSchool' name='tapAfterSchool' value='Afterschool' <?php echo ($tapAfterSchool)?'checked':''; ?> />
                      <label class='flowLabel' for='tapAfterSchool'>TAP&nbspAfterschool</label>
                      <img src="images/error.png" id="errTapAfterSchool" width="14" height="14" alt="Error icon"
                      <?php echo errorStyle($errors, 'tapAfterSchool'); ?>>
                      <br>

                      <div id='tapAfterSchoolFields'>

                        <?php $hcMonday = fieldValue($details, 'hcMonday', false); ?>

                        <input type='checkbox' id='hcMonday' name='hcMonday' value='Monday' <?php echo ($hcMonday)?'checked':''; ?> />
                        <label class='flowLabel' for='hcMonday'>Monday</label>
                        <br>

                        <?php $hcTuesday = fieldValue($details, 'hcTuesday', false); ?>

                        <input type='checkbox' id='hcTuesday' name='hcTuesday' value='Tuesday' <?php echo ($hcTuesday)?'checked':''; ?> />
                        <label class='flowLabel' for='hcTuesday'>Tuesday</label>
                        <br>

                        <?php $hcWednesday = fieldValue($details, 'hcWednesday', false); ?>

                        <input type='checkbox' id='hcWednesday' name='hcWednesday' value='Wednesday' <?php echo ($hcWednesday)?'checked':''; ?> />
                        <label class='flowLabel' for='hcWednesday'>Wednesday</label>
                        <br>

                        <?php $hcThursday = fieldValue($details, 'hcThursday', false); ?>

                        <input type='checkbox' id='hcThursday' name='hcThursday' value='Thursday' <?php echo ($hcThursday)?'checked':''; ?> />
                        <label class='flowLabel' for='hcThursday'>Thursday</label>
                        <br>

                      </div>
                    </div>
              </div><!-- second row wide box 2-->
              <div id="one">
                    <div class="dynamic-box">
                      <?php $csw = fieldValue($details, 'csw', false); ?>
                      <input type='checkbox' id='csw' name='csw' value='CSW' <?php echo ($csw)?'checked':''; ?> />
                      <label class='flowLabel' for='csw'>Community&nbspService</label>
                      <img src="images/error.png" id="errCsw" width="14" height="14" alt="Error icon"
                      <?php echo errorStyle($errors, 'csw'); ?>>
                      <br>

                      <div id='cswFields'>
                        <label for='csHoursOrdered'>CS Hours Ordered</label>
                        <input type='number' name='csHoursOrdered' id='csHoursOrdered' step='1' value='<?php echo fieldValue($details, 'csHoursOrdered'); ?>'>
                        <img src="images/error.png" id="errCsHoursOrdered" width="14" height="14" alt="Error icon"
                        <?php echo errorStyle($errors, 'csHoursOrdered'); ?>>
                        <br>

                        <label for='csHoursStayed'>CS Hours Stayed</label>
                        <input type='number' name='csHoursStayed' id='csHoursStayed' step='1' value='<?php echo fieldValue($details, 'csHoursStayed'); ?>'>
                        <img src="images/error.png" id="errCsHoursStayed" width="14" height="14" alt="Error icon"
                        <?php echo errorStyle($errors, 'csHoursStayed'); ?>>
                        <br>

                        <label for='csHoursDueByReview'>CS Hours Due by Review</label>
                        <input type='number' name='csHoursDueByReview' id='csHoursDueByReview' step='1' value='<?php echo fieldValue($details, 'csHoursDueByReview'); ?>'>
                        <img src="images/error.png" id="errCsHoursDueByReview" width="14" height="14" alt="Error icon"
                        <?php echo errorStyle($errors, 'csHoursDueByReview'); ?>>
                        <br>
                      </div>
                    </div>
              </div><!-- end section-->

              <div id="one">
                    <div class="dynamic-box">
                      <?php $forfeiture = fieldValue($details, 'forfeiture', false); ?>

                      <input type='checkbox' id='forfeiture' name='forfeiture' value='Forfeiture' <?php echo ($forfeiture)?'checked':''; ?> />
                      <label class='flowLabel' for='forfeiture'>Forfeiture</label>
                      <img src="images/error.png" id="errForfeiture" width="14" height="14" alt="Error icon"
                      <?php echo errorStyle($errors, 'forfeiture'); ?>>
                      <br>

                      <div id='forfeitureFields'>
                        <label for='forfeitureAmount'>Forfeiture&nbspAmount</label>
                        $<input type='number' name='forfeitureAmount' id='forfeitureAmount' step='0.01' value='<?php echo fieldValue($details, 'forfeitureAmount'); ?>'>
                        <img src="images/error.png" id="errForfeitureAmount" width="14" height="14" alt="Error icon"
                        <?php echo errorStyle($errors, 'forfeitureAmount'); ?>>
                        <br>
                        <?php $forfeitureStayed = fieldValue($details, 'forfeitureStayed', false); ?>
                        <label>&nbsp;</label>
                        <input type='checkbox' id='forfeitureStayed' name='forfeitureStayed' value='ForfeitureStayed' <?php echo ($forfeitureStayed)?'checked':''; ?> />
                        <label class='flowLabel' for='forfeitureStayed'>Forfeiture&nbspStayed</label>
                        <br>
                      </div>
                   </div>
                 </div><!-- end section-->
                 <div id="wide">
                 </div><!-- second wide section-->
              </div><!-- end parent -->
              <br />

              <section>
                <label>&nbsp;</label>
            		<button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type='submit' id='btnSave' name='btnSave'><img src='images/save.png'> Save</button>
                <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
                <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type='submit' id='btnCancel' name='btnCancel'><img src='images/list.png'> Hearing List</button>
                <button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red" type='submit' id='btnDelete' name='btnDelete'><img src='images/delete.png'> Delete</button>
              </section>
          </form>
        </section>
        <br /><br /><br />
</div> <!--formatting div -->


<script src="javascript/areyousure.js"></script>
<script src="javascript/NumberKeyPress.js"></script>
<script src="javascript/FormatNumber.js"></script>
<script src="javascript/isDate.js"></script>
<script src="javascript/DateFormat.js"></script>
<script src="javascript/hearings.js"></script>

<?php include("views/footer.php"); ?>
