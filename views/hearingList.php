<?php include ("views/header.php"); ?>
  <link href="css/hearingList.css" rel="stylesheet" type="text/css">

    <nav>
    	<ul>
    		<li><a href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
    		<li><a href="?action=contactList">Contact</a></li>
    		<li><a href="?action=caseNoteList">Case Notes</a></li>
    		<li><a class="active" href="?action=hearingList">Hearings</a></li>
    		<li><a href="#commServicePage">Community Service</a></li>
        <li><a href="?action=homeworkCenterList">Homework Center</a></li>
    		<li><a href="?action=gradeList">Grades</a></li>
    	</ul>
    </nav>

    <div id="pageDiv" class="clearfix">

  <!--CLIENT NAME-->
  <section>
    <div class="large-box">
      <label>Client&nbspName</label>
      <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
    </div>

    <div class="caseNum-box">
      <label for='caseNumber'>Court&nbspCase&nbspNumber</label>
			<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
    </div>
    <br /><br />
    <hr>
  </section>

  <section>
    <form class="formStyle" method="post" action="">
      <input type="hidden" name="action" value="hearingNew">
      <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Hearing</button>
    </form>
    <br />
  </section>

  <section style="overflow-x:auto;">
  	<table>
  		<thead>
  				<tr>
  					<th>Hearing Date</th>
						<th>Client Present</th>
						<th>TAP Monitor</th>
						<th>WRAP</th>
						<th>Community Service</th>
						<th>CS Hours Ordered</th>
						<th>CS Hours Stayed</th>
						<th>CS Hours Due by Review</th>
						<th>TAP Afterschool</th>
						<th>Monday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Forfeiture</th>
						<th>Forfeiture Amount</th>
						<th>Forfeiture Stayed</th>
          </tr>
  		</thead>
  		<tbody>
        <?php foreach($hearings as $hearing) : ?>
  	    <tr>
  				<td>
            <a href="?action=hearingDetails&hearingID=<?php echo $hearing['hearingID']; ?>"> <?php echo date_format(new DateTime($hearing['hearingDate']), 'm/d/Y'); ?></a>
          </td>

					<td><?php if($hearing['tapMonitor']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['tapMonitor']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['wrap']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if( $hearing['csw']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php echo $hearing['csHoursOrdered']; ?></td>
					<td><?php echo $hearing['csHoursStayed']; ?></td>
					<td><?php echo $hearing['csHoursDueByReview']; ?></td>
					<td><?php if($hearing['tapAfterSchool']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['hcMonday']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['hcTuesday']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['hcWednesday']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['hcThursday']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php if($hearing['forfeiture']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
					<td><?php echo $hearing['forfeitureAmount'] == null || $hearing['forfeitureAmount'] == '' || $hearing['forfeitureAmount'] == 0 ? '' : '$ ' . $hearing['forfeitureAmount']; ?></td>
					<td><?php if($hearing['forfeitureStayed']==1) echo '<img src="images/checkMark.png" alt="">'?></td>
  	    </tr>
        <?php endforeach; ?>
  	  </tbody>
  	</table>
  </section>
</div>

<?php include("views/footer.php"); ?>
