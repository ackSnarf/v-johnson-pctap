<?php include('views/header.php'); ?>
<link href="css/form.css" rel="stylesheet" type="text/css">

<!--this is the side bar-->
<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
<img src="images/closedDoor.png">
</nav>

<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

      <section>
           <div class="large-box">
              <label>Client Name</label>
              <input  class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
          </div>

           <div class="caseNum-box">
              <label for='caseNumber'>Court Case Number</label>
          		<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
          </div>
          <br /><br />
      	<hr>
      </section>

      <section>
            <form class="formStyle" id="frmDetails" method="post" action= ".">
              <input type='hidden' name='action' value='<?php echo (strpos($action, 'New') > 0) ? 'gradeSaveNew' : 'gradeUpdate'; ?>'>
              <input type='hidden' name='gradeID' value='<?php echo fieldValue($details, 'gradeID'); ?>'>
              <input type='hidden' name='tapID' value='<?php echo fieldValue($details, 'tapID'); ?>'>
              <input type='hidden' name='origSubjectID' id='origSubjectID' value='<?php echo fieldValue($details, 'subjectID') ?>'>
              <input type='hidden' name='origDateGradeChecked' id='origDateGradeChecked' value='<?php echo fieldValue($details, 'dateGradeChecked') ?>'>
              <input type='hidden' name='javaScriptValidated' id='javaScriptValidated' value='false'>

              <br />
              <div class="tenChar-box">
                  <?php $subjectID=fieldValue($details, 'subjectID');?>
                		<label for="subjectID">Subject&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                  		<select class="w3-input w3-border w3-round-large" name="subjectID" id="subjectID" >
                  			<?php foreach ($subjects as $s) : ?>
                  				<option value="<?php echo $s['subjectID']; ?>" <?php if($s['subjectID']==$subjectID) echo ' selected'; ?> >
                  					<?php echo $s['subjectName']; ?>
                  				</option>
                  			<?php endforeach; ?>
                  		</select>
                		<img src="images/error.png" id="errSubjectID" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'subjectID'); ?>>
                </div>

                <div class="threeChar-box">
                  <label for='grade'>Grade&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
              		<input class="w3-input w3-border w3-round-large" type='number' name='grade' id='grade' value='<?php echo fieldValue($details, 'grade'); ?>' <?php echo $GRADE_VALIDATION ?>>
              		<img src="images/error.png" id="errGrade" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'grade'); ?>>
              		</div>

                <div class="date-box">
                  <label for ='dateGradeChecked'>Date Grade&nbspChecked&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
                  <input class="w3-input w3-border w3-round-large" type='date' name='dateGradeChecked' id='dateGradeChecked' value='<?php echo fieldValue($details, 'dateGradeChecked', date("Y-m-d")); ?>'>
                  <img src="images/error.png" id="errDateGradeChecked" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'dateGradeChecked'); ?>>
                </div>

                <section>
                  <label>&nbsp;</label>
                  <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type='submit' id='btnSave' name='btnSave'>  <img src='images/save.png'> Save</button>
                  <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
                  <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type='submit' id='btnCancel' name='btnCancel'><img src='images/list.png'> Grade List</button>
                  <button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red" type='submit' id='btnDelete' name='btnDelete'><img src='images/delete.png'> Delete</button>
                </section>
            </form>
      </section>

</div>

<script src="javascript/areyousure.js"></script>
<script src="javascript/NumberKeyPress.js"></script>
<script src="javascript/FormatNumber.js"></script>
<script src="javascript/isDate.js"></script>
<script src="javascript/DateFormat.js"></script>
<script src="javascript/grades.js"></script>

<?php include('views/footer.php'); ?>
