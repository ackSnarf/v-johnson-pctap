    <!--PHONE NUMBER-->
    <div class="large-box">
        <label for="phoneNumber">Phone&nbspNumber</label>
        <input class="w3-input w3-border w3-round-large" type="number" id="phoneNumber" name="phoneNumber" value="<?php echo fieldValue($details, 'phoneNumber'); ?>" <?php echo $PHONENUMBER_VALIDATION ?> size="20">
        <img src="images/error.png" id="errPhoneNumber" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'phoneNumber'); ?> >
    </div>

    <!--PHONE TYPE-->
    <div class="sixChar-box">
        <label for="phoneNumberTypeID">Type</label>
        <select class="w3-input w3-border w3-round-large" id="phoneNumberTypeID" name="phoneNumberTypeID">
            <?php $phoneNumberTypeID=fieldValue($details, 'phoneNumberTypeID'); ?>
            <?php foreach($types as $t): ?>
            <option value="<?php echo $t['phoneNumberTypeID']; ?>" <?php if($t[ 'phoneNumberTypeID']==$phoneNumberTypeID) echo ' selected'; ?>><?php echo $t[ 'phoneNumberType'];?>
            </option>
            <?php endforeach; ?>
        </select>
        <img src="images/error.png" id="errPhoneNumberType" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'phoneNumberType'); ?> >
    </div>

    <br />
    <br />

    <!--BUTTONS -->
    <section>
        <label>&nbsp;</label>

        <?php if(!isset($_REQUEST['sender'])) {
            $btnCancelText = " Back to List";
        } elseif ($_REQUEST['sender'] == "contact") {
            $btnCancelText = " Contact Details";
        } else {
            $btnCancelText = " Client Details";
        } ?>

        <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button>
        <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
        <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="submit" id="btnCancel" name="btnCancel"><img src="images/list.png" alt=""><?php echo $btnCancelText; ?></button>
        <button <?php echo strpos($action, 'New')!==false ? " hidden" : 'class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red"'; ?> type="submit"  id="btnDelete" name="btnDelete"><img src="images/delete.png" alt=""> Delete</button>
    </section>
