<?php include ("views/header.php"); ?>
		<link href="css/form.css" rel="stylesheet" type="text/css">

			<!-- New Court Order -->
		<?php if ($sideNav) { ?>
			<nav>
				<ul>
					<li><a class="active" href="#noAction">Client</a></li>
					<li><a  class="inactive" href="#noAction">Contact</a></li>
					<li><a  class="inactive" href="#noAction">Case Notes</a></li>
					<li><a  class="inactive" href="#noAction">Hearings</a></li>
					<li><a  class="inactive" href="#noAction">Community Service</a></li>
					<li><a  class="inactive" href="#noAction">Homework Center</a></li>
					<li><a  class="inactive" href="#noAction">Grades</a></li>
				</ul>
			</nav>
			<?php } else { ?>
				<nav>
					<ul>
						<li><a class="active" href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
						<li><a href="?action=contactList">Contact</a></li>
						<li><a href="?action=caseNoteList">Case Notes</a></li>
						<li><a href="?action=hearingList">Hearings</a></li>
						<li><a href="#commServicePage">Community Service</a></li>
            <li><a href="?action=homeworkCenterList">Homework Center</a></li>
						<li><a href="?action=gradeList">Grades</a></li>
					</ul>
				</nav>

 			<?php } ?> <!--end if -->


		<div id="pageDiv" class="clearfix">  <!-- content wrapped in a div-->

			<section>
					  <div class="large-box">
							<label for='contactFirstName'>Client Name</label>
							<input class="w3-input w3-border w3-round-large" type='text' name='contactFirstName' id='firstName' size='20' value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
				  </div>

					<div class="caseNum-box">
							<label for='caseNumber'>Court Case Number</label>
							<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber'  size='20' value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
			   </div>
				 <br /><br />
				 <hr />
			</section>

			<section>

<form id="courtOrderDetails" method="post" action=".">
	<!-- HIDDEN -->
	<input type="hidden" name="action"  value='<?php echo (strpos($action, 'New')>0)?'courtOrderSaveNew':'courtOrderUpdate'?>'>
	<input type="hidden" name="javascriptValidated" id="javascriptValidated" value = "true">

	<!--REFERRAL DATE-->
	<div class="date-box">
		<label for="referralDate">Referral Date&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
		<input class="w3-input w3-border w3-round-large" type="date" name="referralDate" id="referralDate" value="<?php echo fieldValue($courtOrderDetails, 'referralDate'); ?>" >
		<img src="images/error.png" id="errReferralDate" width="14" height="14" alt="error icon" <?php echo errorStyle($errors, 'referralDate');?> >
	</div>

	<!--CASE NUMBER-->
	<div class="sixChar-box">
		<label for='caseNumber'>Case Number&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
		<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20' value="<?php echo fieldValue($courtOrderDetails, 'caseNumber'); ?>" <?php echo $CASENUMBER_VALIDATION ?> >
		<img src="images/error.png" id="errCaseNumber" width="14" height="14" alt="error icon" <?php echo errorStyle($errors, 'caseNumber'); ?> >
	</div>

	<br />

<!--GRADE LEVEL-->
<div class="sixChar-box">
<?php $gradeLevelID=fieldValue($courtOrderDetails, 'gradeLevelID');?>
		<label for="gradeLevelID">Grade level</label><br />
		<select class="w3-input w3-border w3-round-large" name="gradeLevelID" id="gradeLevelID" >
			<?php foreach ($gradeLevels as $g) : ?>
				<option value="<?php echo $g['gradeLevelID']; ?>" <?php if($g['gradeLevelID']==$gradeLevelID) echo 'selected'; ?> >
					<?php echo $g['gradeLevel']; ?>
				</option>
			<?php endforeach; ?>
		</select>
		</div>

<!--SCHOOL ONE-->
<div class="sixChar-box">
<?php $schoolIDa=fieldValue($courtOrderDetails, 'schoolIDa');?>
		<label for="schoolIDa">School One</label><br />
		<select class="w3-input w3-border w3-round-large" name="schoolIDa" id="schoolIDa" >
			<?php foreach ($schools as $s) : ?>
				<option value="<?php echo $s['schoolID']; ?>" <?php if($s['schoolID']==$schoolIDa) echo 'selected'; ?> >
					<?php echo $s['schoolName']; ?>
				</option>
			<?php endforeach; ?>
		</select>
		</div>

<!--SCHOOL TWO-->
<div class="sixChar-box">
	<?php $schoolIDb=fieldValue($courtOrderDetails, 'schoolIDb');?>
	<label for="schoolIDb">School Two</label><br />
		<select class="w3-input w3-border w3-round-large" name="schoolIDb" id="schoolIDb" >
			<?php foreach ($schools as $sc) : ?>
				<option value="<?php echo $sc['schoolID']; ?>" <?php if($sc['schoolID']==$schoolIDb) echo 'selected'; ?> >
					<?php echo $sc['schoolName']; ?>
				</option>
			<?php endforeach; ?>
		</select>
</div>

<hr>
<!--DISCHARGE DATE-->
<div class="date-box">
	<label for="dischargeDate">Discharge Date</label>
		<input class="w3-input w3-border w3-round-large" type="date" name="dischargeDate" id="dischargeDate" value="<?php echo fieldValue($courtOrderDetails, 'dischargeDate'); ?>" />
   <img src="images/error.png" id="errDischargeDate" width="14" height="14" alt="error icon" <?php echo errorStyle($errors, 'dischargeDate'); ?> >
</div>

<!--DISCHARGE REASON-->
<div class="large-box">
	<?php $dischargeReasonID=fieldValue($courtOrderDetails, 'dischargeReasonID');?>
	<label for="dischargeReasonID">Discharge Reason</label>
	<select class="w3-input w3-border w3-round-large" name="dischargeReasonID" id="dischargeReasonID" >
		<?php foreach ($dischargeReasons as $d) : ?>
			<option value="<?php echo $d['dischargeReasonID']; ?>" <?php if($d['dischargeReasonID']==$dischargeReasonID) echo 'selected'; ?> >
				<?php echo $d['dischargeReason']; ?>
			</option>
		<?php endforeach; ?>
	</select>
	<br />
</div>

	<section>
		<label>&nbsp;</label>
		<a><button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button></a>
		<a><button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button></a>
		<a><button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="submit" id="btnCancel" name="btnCancel"><img src="images/list.png" alt=""> Client Details</button></a>
	</section>

</form>
</section>


</div> <!-- end the formatting div -->

<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/courtOrders.js"></script>
<script type="text/javascript" src="javascript/isDate.js"></script>
<script type="text/javascript" src="javascript/DateFormat.js"></script>
<?php include("views/footer.php") ?>
