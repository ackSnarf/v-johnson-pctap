<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">


<nav>
	<ul>
		<li><a href="?action=clientEdit&contactID=<?php echo $_SESSION['contactID'];?>">Client</a></li>
		<li><a href="?action=contactList">Contact</a></li>
		<li><a href="?action=caseNoteList">Case Notes</a></li>
		<li><a href="?action=hearingList">Hearings</a></li>
		<li><a href="#commServicePage">Community Service</a></li>
    <li><a href="?action=homeworkCenterList">Homework Center</a></li>
		<li><a class="active" href="?action=gradeList">Grades</a></li>
	</ul>
</nav>

<div id="pageDiv" class="clearfix">  <!-- contents wrapped in a div-->

		  <!--CLIENT NAME-->
		  <section>
		  	<div class="large-box">
		      <label>Client Name</label>
		      <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
				</div>

				<div class="large-box">
					<label for='caseNumber'>Court Case Number</label>
				  <input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
				</div>
				<br /></br />
				<hr>
		  </section>

		  <section>
		    <form class="formStyle" method="post" action="">
		      <input class="w3-input w3-border w3-round-large" type="hidden" name="action" value="gradeNew">
		      <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Grade</button>
		    </form>
				<br />
		  </section>

		  <section>
		  	<table style="width:30%">
		  		<thead>
		  				<tr>
		  						<th>Subject</th>
		              <th class="center">Grade</th>
		              <th>Date Checked</th>
		          </tr>
		  		</thead>
		  		<tbody>
		        <?php foreach($grades as $grade) : ?>
		  	    <tr>
		  	      <td>
								<a href="?action=gradeDetails&gradeID=<?php echo $grade['gradeID']; ?>">
									<?php echo $grade['subjectName']; ?></a>
							</td>
		          <td class="center"><?php echo $grade['grade']; ?></td>
		          <td><?php echo date_format(new DateTime($grade['dateGradeChecked']), 'm/d/Y'); ?></td>
		  	    </tr>
		        <?php endforeach; ?>
		  	  </tbody>
		  	</table>
		  </section>
</div>

<?php include("views/footer.php"); ?>
