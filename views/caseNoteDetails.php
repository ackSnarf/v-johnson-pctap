<?php include ("views/header.php"); ?>
		<link href="css/form.css" rel="stylesheet" type="text/css">

		<nav style ="border: 3px solid #f8f8ff; background-color = #0687bf">
				<img src="images/closedDoor.png">
		</nav>

		<div id="pageDiv" class="clearfix">

		<section>
			 <div class="large-box">
				 <label>Client Name</label>
		     <input class="w3-input w3-border w3-round-large" type='text' name='clientName' id='clientName' size='20'value="<?php echo fieldValue($_SESSION, 'clientName'); ?>" readonly>
			 </div>

			<div class="caseNum-box">
				<label for='caseNumber'>Court Case Number</label>
				<input class="w3-input w3-border w3-round-large" type='text' name='caseNumber' id='caseNumber' size='20'value="<?php echo fieldValue($_SESSION, 'caseNumber'); ?>" readonly>
			</div>
			<br /><br />
			<hr>
		</section>

		<section>
				<form class="formStyle" id="caseNoteDetails" method="post" action=".">
				  <input type="hidden" name="action" value='<?php echo (strpos($action, 'New')>0)?'caseNoteSaveNew':'caseNoteUpdate'?>'>
					<input type="hidden" name="javascriptValidated" id="javascriptValidated" value = "false">
					<input type='hidden' name='caseNoteID' id='caseNoteID' size='10'	value="<?php echo fieldValue($details, 'caseNoteID'); ?>"/>

				  <!--DATE -->
					<div class="date-box">
						<a onclick=" dayNow(dtpCaseNoteDate)" href="#">Set Date&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></a>
					  <input class="w3-input w3-border w3-round-large" type="date" name="caseNoteDate" id="dtpCaseNoteDate" value="<?php echo fieldValue($details, 'caseNoteDate'); ?>" autofocus/>
						<img src="images/error.png" id="errCaseNoteDate" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'caseNoteDate'); ?> >
					</div>

				    <!--TIME -->
				  <div class="date-box">
					<a onclick="timeNow(caseNoteTime)" href="#">Set Time&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></a>
					<input class="w3-input w3-border w3-round-large" type="time" name="caseNoteTime" id="caseNoteTime" value="<?php echo fieldValue($details, 'caseNoteTime'); ?>" />
					<img src="images/error.png" id="errCaseNoteTime" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'caseNoteTime'); ?> >
				</div>
				<hr>
				  <!--STAFF TABLE INPUT -->
					<div class="large-box">
					<?php $staffID=fieldValue($details, 'staffID');?>
							<label for="staffID">Staff Name</label>
							<select class="w3-input w3-border w3-round-large" name="staffID" id="staffID" >
								<?php foreach ($staff as $s) : ?>
									<option value="<?php echo $s['staffID']; ?>" <?php if($s['staffID']==$staffID) echo 'selected'; ?> >
										<?php echo $s['staffFirstName']; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>


				  <!--TARGETS TABLE INPUT -->
					<div class="target-box">
					<?php $targetTypeID=fieldValue($details, 'targetTypeID');?>
							<label for="targetTypeID">Target Type</label>
							<select class="w3-input w3-border w3-round-large" name="targetTypeID" id="targetTypeID" >
								<?php foreach ($targetTypes as $t) : ?>
									<option value="<?php echo $t['targetTypeID']; ?>" <?php if($t['targetTypeID']==$targetTypeID) echo ' selected'; ?> >
										<?php echo $t['targetType']; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>


				  <!--nNOTE TYPES TABLE INPUT -->
					<div class="tenChar-box">
					<?php $noteTypeID=fieldValue($details, 'noteTypeID');?>
							<label for="noteTypeID">Note Type</label>
							<select class="w3-input w3-border w3-round-large" name="noteTypeID" id="noteTypeID" >
								<?php foreach ($noteTypes as $nt) : ?>
									<option value="<?php echo $nt['noteTypeID']; ?>" <?php if($nt['noteTypeID']==$noteTypeID) echo ' selected'; ?> >
										<?php echo $nt['noteTypeDescription']; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>



				  <!--CHECKBOX -->
					<div class="check-box">
					<?php $success=fieldValue($details, 'success', false);?>
				  <input type="checkbox" id="success" name="success" value="yes"<?php echo ($success)? 'checked' : ''; ?>>
				  <label for="success" class="flowLabel">Target&nbspContacted?</label>
				</div>



				  <!--nNOTE FIELD -->
					<hr>
					<div class="large-box">
					<?php $note=(isset($details['note']))?$details['note']:"";?>
				  <label for="note">Note&nbsp<i class="fa fa-asterisk" aria-hidden="true"></i></label>
				  <textarea name="note" id="txtNote" cols=40 rows=3><?php echo fieldValue($details, 'note'); ?></textarea>
					<img src="images/error.png" id="errNote" width="14" height="14" alt="Error icon"  <?php echo errorStyle($errors, 'note'); ?> >
				</div>
				  <br />


					<section> <!--BUTTONS -->
						<label>&nbsp;</label>
						<button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button>
						<button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
						<button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="submit" id="btnCancel" name="btnCancel"><img src="images/list.png" alt=""> Notes List</button></a>
						<button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge w3-hover-red" type="submit" id="btnDelete" name="btnDelete"><img src="images/delete.png" alt=""> Delete</button>
					</section>
				</form>
		</section>
</div> <!-- end the formatting div -->

<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/caseNotes.js"></script>
<script type="text/javascript" src="javascript/isDate.js"></script>
<script type="text/javascript" src="javascript/DateFormat.js"></script>

<?php include("views/footer.php") ?>
