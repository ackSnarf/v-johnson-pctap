<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">
<link href="css/form.css" rel="stylesheet" type="text/css">

<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
  <img src="images/closedDoor.png">
</nav>

<div id="pageDiv" class="clearfix">
    <section>
        <form class ="formStyle" id="hcAttendance" method="post" action= ".">
            <input type='hidden' name="action" value="<?php echo $_REQUEST['action']=='hcAttendanceNew'?'hcAttendanceSaveNew':'hcAttendanceUpdate'; ?>">
            <input type="hidden" name="javascriptValidated" id="javascriptValidated" value = "false">
            <!--<input type='hidden' name='day' id='day' value=<!?php echo date('l');?>>-->

            <h3> Homework Center Attendance </h3>
            <!--<!?php echo printArray($details);?>-->
            <hr>
            <section>
              	<div class="sixCharRight-box">
                    <h5><?php echo date('l');?></h5>
                    <br />
                </div>

            	  <div class="date-box">
                    <input class="w3-input w3-border w3-round-large" type="Date" id="hcattendDate" name="hcattendDate" value="<?php echo date("Y-m-d"); ?>" readonly>
                    <img src="images/error.png" id="errhcattendDate" width="14" height="14" alt="Error icon" <?php echo errorStyle($errors, 'hcattendDate'); ?> >
               </div>
            </section>

            <table style="width:70%">
                <tr>
            			<th>Student</th>
            			<th>Status</th>
            			<th>Attendance Points</th>
            			<th>Homework Points</th>
            		</tr>
                		<?php foreach ($details as $d) : ?>
                		<tr>
                  			<td>
                            <input  class="w3-input w3-border w3-round" type="text" id="client[]" name="client[]" value="<?php echo $d['client']; ?>" readonly>
                            <input  type="hidden" id="tapID[]" name="tapID[]" value="<?php echo $d['tapID']; ?>">
                  			</td>
        			          <td>
                            <select  class="w3-input w3-border w3-round" id="hcattendStatusID[]" name="hcattendStatusID[]">
            				              <?php foreach ($status as $s) : ?>
                                      <option value="<?php echo $s['hcattendStatusID']; ?>"><?php echo $s['hcattendStatus']; ?></option>
                                  <?php endforeach; ?>
            	              </select>
                       </td>
                        <td>
                            <input  class="w3-input w3-border w3-round" type="number" id="hcattendPoints[]" name="hcattendPoints[]" value="0" <?php echo $HCATTENDPOINTS_VALIDATION ?>>
                        </td>
                  			<td>
                            <input  class="w3-input w3-border w3-round" type="number" id="homeworkPoints[]" name="homeworkPoints[]" value="0" <?php echo $HOMEWORKPOINTS_VALIDATION ?>>
                        </td>
                    </tr>
        		        <?php endforeach; ?>
            </table>

            <br />
             <!--BUTTONS -->
            <section>
                <label>&nbsp;</label>
                <button class="w3-btn w3-white w3-border w3-border-purple w3-round-xlarge w3-hover-purple" type="submit" id="btnSave" name="btnSave"><img src="images/save.png" alt=""> Save</button>
                <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-grey" type="reset" id="btnReset" name="btnReset"><img src="images/reset.png" alt=""> Reset Form</button>
                <button class="w3-btn w3-white w3-border w3-border-yellow w3-round-xlarge w3-hover-yellow" type="submit" id="btnCancel" name="btnCancel"><img src="images/list.png" alt=""> Client List</button>
            </section>
        </form>
    </section>
</div>
<script type="text/javascript" src="javascript/areyousure.js"></script>
<script type="text/javascript" src="javascript/DateFormat.js"></script>
<script type="text/javascript" src="javascript/isDate.js"></script>
<script type="text/javascript" src="javascript/numericKeyPress.js"></script>
<script type="text/javascript" src="javascript/hcAttendance.js"></script>

<?php include ("views/footer.php"); ?>
