<?php include ("views/header.php"); ?>
<link href="css/table.css" rel="stylesheet" type="text/css">


	<!--this is the side bar-->
	<nav style = "border: 3px solid #f8f8ff; background-color = #0687bf">
		<img src="images/closedDoor.png">
	</nav>

	<div id="pageDiv" class="clearfix">

	<!-- SEARCH ALL STAFF -->
		<section>
				<form method="post" action="">
					<div class="large-box">
						<h5>Search All Staff</h5>
					    <input class="w3-input w3-border w3-round-large" type="text" list="allStaff" id="txtStaff"/>
							    <datalist id="allStaff">
							      <?php foreach ($allStaff as $s): ?>
							        <option value="<?php echo $s['staffName']; ?>" id="<?php echo $s['staffID']; ?>" >
							        <?php endforeach; ?>
							    </datalist>
					    <input type="hidden" name="staffID" id="staffID" value="null">
					</div>

				    <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-blue"
						type="submit" onClick="selectedStaff()" id="btnSearch" name="action" value="staffDetails">Go to Details</button>
						<br /><br />
					<hr>
				</form>
		 </section>

	  <!--NEW STAFF-->
	  <section>
		    <form  method="post" action="">
		        <input type="hidden" name="action" value="staffAdd">
		        <button class="w3-btn w3-white w3-border w3-border-blue w3-round-xlarge w3-hover-light-blue" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> New Staff</button>
		    </form>
	    <hr>
	  </section>

	  <!--CURRENT STAFF LIST -->
	  <section>
	    <table style="width:20%">
	      <thead>
	        <th style="background-color: white"> Current Staff </th>
	      </thead>
		      <tbody>
		         <?php foreach($staff as $s): ?>
		            <tr>
		               <td style="border: none" ><a href="?action=staffDetails&staffID=<?php echo $s['staffID']; ?>">
		                <?php echo $s['staffName']; ?></a>
		               </td>
		            </tr>
		         <?php endforeach; ?>
		     </tbody>
	  	</table>
	  </section>
 </div>

<script src="javascript/search.js"></script>
<?php include ("views/footer.php"); ?>
